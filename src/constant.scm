; math constants

(define dinf 900000000) ; 9e8
(define inf +inf.0)
(define e 2.718281828459045) ; (exp 1.0)
(define pi 3.141592653589793) ; (* 4.0 (atan 1.0))
(define twoPi 6.283185307179586) ; (* 2 pi)
(define halfPi 1.5707963267948966) ; (* 0.5 pi)

; enumeration constants

(define addToHead 0)
(define addToTail 1)
(define addBefore 2)
(define addAfter 3)
(define addReplace 4)

(define genNormalize 1)
(define genWavetable 2)
(define genClear 4)

(define doNothing 0)
(define pauseSynth 1)
(define removeSynth 2)
(define removeGroup 14)

(define noLoop 0)
(define loop 1)

(define linear 0)
(define exponential 1)
