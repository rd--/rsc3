; t|[t] -> [t]
(define asArray
  (lambda (value)
    (if
     (list? value)
     value
     (if (vector? value)
         value
         (list value)))))

; [t] -> int -> t
(define at
  (lambda (sequence index)
    (seq-ref sequence (- index 1))))

; [t] -> (t -> u) -> [u]
(define collect
  (lambda (c f)
    (if (list? c)
        (map f c)
        (error "collect" "not a list?"))))

(define concatenation concat)

; [t] -> int -> int -> [t]
(define copyFrom:to
  (lambda (c i j)
    (if (string? c)
        (substring c (- i 1) j)
        (error "copyFrom:to" "not a string?"))))

(define ifTrue
  (lambda (p q)
      (if p (q))))

(define maxItem maximum)

(define nth at)

(define reciprocal
  (lambda (n)
    (Fdiv 1 n)))

(define rounded
  (lambda (number)
    (RoundTo number 1)))

(define size length)

(define timesRepeat
  (lambda (anInteger aFunction)
    (if (> anInteger 0) (begin (aFunction) (timesRepeat (- anInteger 1) aFunction)))))

; (equal? (upToBy 1 9 1) (list 1 2 3 4 5 6 7 8 9))
; (equal? (upToBy 1 9 2) (list 1 3 5 7 9))
(define upToBy
  (lambda (start stop step)
    (if (> start stop)
	'()
	(cons start (upToBy (+ start step) stop step)))))

; (equal? (downToBy 9 1 1) (list 9 8 7 6 5 4 3 2 1))
; (equal? (downToBy 9 1 2) (list 9 7 5 3 1))
(define downToBy
  (lambda (start stop step)
    (if (< start stop)
	'()
	(cons start (downToBy (- start step) stop step)))))

; (equal? (upOrDownToBy 1 9 2) (list 1 3 5 7 9))
; (equal? (upOrDownToBy 9 1 2) (list 9 7 5 3 1))
(define upOrDownToBy
  (lambda (start stop step)
    (if (> stop start)
	(upToBy start stop step)
	(downToBy start stop step))))

; (equal? (upOrDownTo 1 9) (list 1 2 3 4 5 6 7 8 9))
; (equal? (upOrDownTo 9 1) (list 9 8 7 6 5 4 3 2 1))
(define upOrDownTo
  (lambda (start stop)
    (upOrDownToBy start stop 1)))

; int -> int -> [int] ; supercollider (n .. m) i.e. upOrDownTo
(define to upOrDownTo)

(define toBy upOrDownToBy)

(define ! dup)

(define !+
  (lambda (f k)
    (Mix (! f k))))

(define !^
  (lambda (f k)
    (Splay2 (! f k))))
