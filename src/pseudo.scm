; [float] -> ugen
(define asLocalBuf
  (lambda (l)
    (let* ((b (LocalBuf 1 (seq-length l)))
           (s (asSetBuf b 0 l)))
      (Mrg2 b s))))

; int -> [float] -> ugen
(define asLocalBufInterleaved
  (lambda (nc l)
    (let* ((nf (div (seq-length l) nc))
           (b (LocalBuf nc nf))
           (s (asSetBuf b 0 l)))
      (Mrg2 b s))))

; [[float]] -> ugen
(define asLocalBufMultiChannel
  (lambda (a)
    (asLocalBufInterleaved (seq-length a) (concat (transpose (seq-list a))))))

; the cardinality input is derived from the values input...
(define asSetBuf
  (lambda (buf offset values)
    (SetBuf buf offset (seq-length values) (seq-list values))))

(define Asr
  (lambda (gate attackTime releaseTime curve)
    (EnvGen (EnvAsr attackTime 1 releaseTime curve) gate 1 0 1 0)))

(define ControlIn
  (lambda (nc bus)
    (construct-ugen "In" control-rate (list bus) nil nc nil)))

(define ControlOut
  (lambda (bus channelsArray)
    (if
     (eq? (rateOf channelsArray) control-rate)
     (Out bus channelsArray)
     (error "ControlOut" "signal not control rate"))))

; ugen -> ugen -> ugen
(define Dcons
  (lambda (x xs)
    (let ((i (Dseq 1 (Mce2 0 1)))
          (a (Dseq 1 (Mce2 x xs))))
      (Dswitch i a))))

; ugen -> ugen -> ugen -> ugen -> ugen -> ugen
(define DynKlankBuilder
  (lambda (i fs fo ds l)
    (if (null? l)
        0
        (let ((f (list-ref l 0))
              (a (list-ref l 1))
              (d (list-ref l 2)))
          (Add
           (Mul (Ringz i (MulAdd f fs fo) (Mul d ds)) a)
           (DynKlankBuilder i fs fo ds (drop 3 l)))))))

(define DynKlank
  (lambda (i fs fo ds s)
    (DynKlankBuilder i fs fo ds (mceChannels s))))

(define FftDefaults
  (lambda (buf in) (FFT buf in 0.5 0 1 0)))

; ugen -> ugen -> ugen -> ugen
(define FreqShift*
  (lambda (i f p)
    (let ((o (SinOsc f (Mce2 (Add p (* 0.5 pi)) p)))
          (h (Hilbert i)))
      (Mix (Mul h o)))))

(define IfftDefaults
  (lambda (buf) (IFFT buf 0 0)))

; [ugen] -> [ugen] -> [ugen] -> ugen
;
; (klangData (list 220 330 440) 1 (list))
(define klangData
  (lambda (freqs amps phases)
    (let* ((m (length freqs))
           (f (lambda (l) (extend l m)))
           (amps* (f (if (null? amps) (list 1) amps)))
           (phases* (f (if (null? phases) (list 0) phases))))
      (concat
       (zipWith3
        (lambda (i j k) (list i j k))
        freqs amps* phases*)))))

; ugen -> ugen -> ugen -> ugen
(define klangDataMce
  (lambda (freqs amps phases)
    (klangData (mceChannels freqs) (mceChannels amps) (mceChannels phases))))

; [ugen] -> [ugen] -> [ugen] -> ugen
(define klankData
  (lambda (freqs amps times)
    (klangData freqs amps (if (null? times) 1 times))))

; ugen -> ugen -> ugen -> ugen
(define klankDataMce
  (lambda (f a p)
    (klankData (mceChannels f) (mceChannels a) (mceChannels p))))

; [ugen] -> ugen
(define ListChoose
  (lambda (l)
    (Select (IRand 0 (length l)) l)))

; ugen:mce -> ugen
(define mceChoose
  (lambda (u)
    (ListChoose (mceChannels u))))

(define LinLin
  (lambda (in srclo srchi dstlo dsthi)
    (let* ((scale (Fdiv (Sub dsthi dstlo) (Sub srchi srclo)))
           (offset (Sub dstlo (Mul scale srclo))))
      (MulAdd in scale offset))))

(define ExpRange (lambda (i l r) (LinExp i -1 1 l r)))
(define LinRange (lambda (i l r) (LinLin i -1 1 l r)))

(define toExpRange (lambda (i r) (LinExp i -1 1 (fst r) (snd r))))
(define toLinRange (lambda (i r) (LinLin i -1 1 (fst r) (snd r))))

(define Mce vector)
(define Mce2 (lambda (a b) (vector a b)))
(define Mce3 (lambda (a b c) (vector a b c)))
(define Mce4 (lambda (a b c d) (vector a b c d)))
(define Mce5 (lambda (a b c d e) (vector a b c d e)))
(define Mce6 (lambda (a b c d e f) (vector a b c d e f)))
(define Mce7 (lambda (a b c d e f g) (vector a b c d e f g)))
(define Mce8 (lambda (a b c d e f g h) (vector a b c d e f g h)))
(define Mce9 (lambda (a b c d e f g h i) (vector a b c d e f g h i)))
(define Mce10 (lambda (a b c d e f g h i j) (vector a b c d e f g h i j)))

; mce -> int -> ugen ; zero-indexed
(define mceChannel
  (lambda (u n)
    (list-ref (mceChannels u) n)))

; ([ugen] -> [ugen]) -> (mce -> mce)
(define mceEdit
  (lambda (f)
    (lambda (u)
      (f (mceChannels u)))))

(define mceProduct
  (mceEdit (lambda (l) (list (foldl1 Mul l)))))

(define mceSum
  (mceEdit (lambda (l) (list (foldl1 Add l)))))

; int -> (int -> t) -> [t]
(define listFill
  (lambda (n f)
    (map f (enumFromTo 0 (- n 1)))))

(define listFillOneIndexed
  (lambda (n f)
    (map f (enumFromTo 1 n))))

; int -> (int -> ugen) -> mce
(define mceFill
  (lambda (n f)
    (listFill n f)))

(define mceFillOneIndexed
  (lambda (n f)
    (listFillOneIndexed n f)))

; (ugen -> ugen) -> mce -> mce
(define mceMap
  (lambda (f u)
    (map f (mceChannels u))))

; mce -> mce
(define mceReverse
  (mceEdit reverse))

; mce -> mce
(define mceTranspose
  (lambda (u)
    (transpose (map mceChannels (mceChannels u)))))

; int -> ugen -> ugen ; (mceRotate 3 (list 1 2 3 4 5 6))
(define mceRotate
  (lambda (k u)
    (let ((rotateRight (lambda (n p)
                         (let ((spl (splitAt (- (length p) n) p)))
                           (append (snd spl) (fst spl))))))
      (rotateRight k (mceChannels u)))))

; num -> num -> mce ; mce [i .. j]
(define mceFromTo
  (lambda (i j)
    (enumFromTo i j)))

; num -> num -> num -> mce ; mce [i,j .. k]
(define mceFromThenTo
  (lambda (i j k)
    (enumFromThenTo i j k)))

(define mixList
  (lambda (l)
    (foldl1 Add l)))

; ugen|mce -> ugen
(define Mix
  (lambda (u)
    (mixList (mceChannels u))))

; int -> (int -> ugen) -> ugen
(define mixFill
  (lambda (n f)
    (Mix (mceFill n f))))

(define mixFillOneIndexed
  (lambda (n f)
    (Mix (mceFillOneIndexed n f))))

; ugen -> ugen -> warp -> ugen -> ugen
(define MouseRand
  (lambda (l r ty tm)
    (let ((f (if (= ty 0) LinLin LinExp)))
      (Lag (f (LFNoise1 1) -1 1 l r) tm))))

(define MouseXRand
  MouseRand)

(define MouseYRand
  MouseRand)

(define MouseButtonRand
  (lambda (l r tm)
    (let ((o (LFClipNoise 1)))
      (kr: (Lag (LinLin o -1 1 l r) tm)))))

(define Mrg2 make-mrg)
(define Mrg3 (lambda (a b c) (Mrg2 a (Mrg2 b c))))
(define Mrg4 (lambda (a b c d) (Mrg2 a (Mrg3 b c d))))
(define Mrg5 (lambda (a b c d e) (Mrg2 a (Mrg4 b c d e))))

(define Mul3 (lambda (a b c) (Mul (Mul a b) c)))
(define Mul4 (lambda (a b c d) (Mul (Mul (Mul a b) c) d)))
(define Mul5 (lambda (a b c d e) (Mul (Mul (Mul (Mul a b) c) d) e)))

(define Add3 (lambda (a b c) (Add (Add a b) c)))
(define Add4 (lambda (a b c d) (Add (Add (Add a b) c) d)))
(define Add5 (lambda (a b c d e) (Add (Add (Add (Add a b) c) d) e)))

(define Div Fdiv)
(define Div3 (lambda (a b c) (Fdiv (Fdiv a b) c)))

(define AddDiv (lambda (i j k) (Fdiv (Add i j) k)))
(define AddMul (lambda (i j k) (Mul (Add i j) k)))
(define DivAdd (lambda (i j k) (Add (Fdiv i j) k)))
(define MulDiv (lambda (i j k) (Fdiv (Mul i j) k)))

; [m] -> [p] -> [#, m, p...]
(define packfft-data
  (lambda (m p)
    (cons (* 2 (length m))
          (concat (zipWith list m p)))))

; [[m, p]] -> [#, m, p...]
(define packfft-data*
  (lambda (mp)
    (cons (* 2 (length mp))
          (concat mp))))

; ugen -> ugen -> ugen -> ugen ; default arguments
(define pitchDefaults
  (lambda (in median ampThreshold)
    (Pitch in 444.0 60.0 4000.0 100.0 16 median ampThreshold 0.5 1 0)))

; ugen -> ugen -> ugen -> ugen -> ugen
(define PMOsc
  (lambda (cf mf pm mp)
    (SinOsc cf (Mul (SinOsc mf mp) pm))))

(define PmOsc
  PMOsc)

(define pvCollect
  (lambda (c nf f from to z?)
    (let* ((m (UnpackFFT c nf from to 0))
           (p (UnpackFFT c nf from to 1))
           (i (enumFromTo from to))
           (e (zipWith3 f m p i)))
      (PackFFT c nf from to z? (packfft-data* e)))))

; ugen -> ugen
(define SoundIn
  (lambda (n)
    (if (mce? n)
        (let ((l (mceChannels n)))
          (if (consecutive? l)
              (In (length l) (Add (NumOutputBuses) (head l)))
              (In 1 (Add (NumOutputBuses) n))))
        (In 1 (Add (NumOutputBuses) n)))))

; int -> ugen -> ugen -> ugen:ar
(define Tap
  (lambda (nc buf delay-time)
    (let ((n (Mul delay-time (Neg (SampleRate)))))
      (PlayBuf nc buf 1 0 n loop doNothing))))

; ugen -> ugen -> ugen
(define TChoose
  (lambda (trig array)
    (Select (TIRand 0 (length (mceChannels array)) trig) array)))

; ugen -> ugen -> ugen -> ugen -> ugen
(define TWChoose
  (lambda (trig array weights normalize)
    (Select (TWindex trig normalize weights) array)))

(define UnpackFFT
  (lambda (c nf from to mp?)
    (map (lambda (i)
            (Unpack1FFT c nf i mp?))
         (enumFromTo from to))))

; Pan a set of channels across the stereo field.
; input, spread:1, level:1, center:0, levelComp:true
; Ugen -> Ugen -> Ugen -> Ugen -> Bool -> Ugen
(define Splay
  (lambda (i s l c lc)
    (let* ((n (if (mce? i) (max 2 (mceDegree i)) 2))
           (m (- n 1))
           (p (map (lambda (x) (- (* (/ 2.0 m) x) 1)) (enumFromTo 0 m)))
           (a (if lc (sqrt (/ 1.0 n)) 1)))
      (Mix (Pan2 i (map (lambda (x) (MulAdd s x c)) p) (Mul l a))))))

(define Splay2
  (lambda (i)
    (Splay i 1 1 0 #t)))

; Select q or r by p, ie. if p == 1 then q else if p == 0 then r@.
(define Select2
  (lambda (p q r)
    (Add (Mul p q) (Mul (Sub 1 p) r))))

(define UgenIf
  Select2)

; Ugen -> Ugen -> Ugen ; Triggers when a value changes
(define Changed
  (lambda (input threshold)
    (Gt
     (Abs (HPZ1 input))
     threshold)))

; VarLag in terms of EnvGen.  Note: in Sc3 curvature and warp are separate arguments.
(define VarLagEnv
  (lambda (in time warp start)
    (let* ((start* (if start start in))
           (e (Env (list start* in) (list time) (list warp) -1 -1))
           (time_ch (if (equal? (rateOf time) scalar-rate) 0 (Changed time 0)))
           (tr (Add3 (Changed in 0) time_ch (Impulse 0 0))))
      (EnvGen tr 1 0 1 doNothing e))))

; int -> (t -> t) -> t -> t ; iterate function n times
(define iter (lambda (n f i) (if (= n 0) i (f (iter (- n 1) f i)))))

(define TLine
  (lambda (start end dur trig)
    (let ((d (Env (list start start end) (list 0 dur) (list 'linear 'linear) -1 -1)))
      (EnvGen trig 1 0 1 0 d))))

(define TXLine
  (lambda (start end dur trig)
    (let ((d (Env (list start start end) (list 0 dur) (list 'exponential 'exponential) -1 -1)))
      (EnvGen trig 1 0 1 0 d))))

(define TRand2
  (lambda (x t)
    (TRand (- x) x t)))

; Mix one output from many sources
(define SelectX
  (lambda (ix xs)
    (let ((s0 (Select (RoundTo ix 2) xs))
          (s1 (Select (Add (Trunc ix 2) 1) xs)))
      (XFade2 s0 s1 (Fold2 (MulAdd ix 2 -1) 1) 1))))

(define Ln
  (lambda (start end dur)
    (Line start end dur doNothing)))

(define XLn
  (lambda (start end dur)
    (XLine start end dur doNothing)))

(define Seq
  Dseq)

(define DmdOn
  Demand)

(define Shuf
  Dshuf)

(define Choose
  Drand)

(define Sequencer
  (lambda (valueArray trig)
    (DmdOn trig 0 (Seq dinf valueArray))))

(define ImpulseSequencer
  (lambda (valueArray trig)
    (Mul (Sequencer valueArray trig) trig)))

(define Bpf BPF)
(define Bpz2 BPZ2)
(define Dc DC)
(define DiBrown Dibrown)
(define Fft FFT)
(define Fos FOS)
(define Hpf HPF)
(define FbSineL FBSineL)
(define FbSineN FBSineN)
(define FbSineC FBSineC)
(define Ifft IFFT)
(define LeakDc LeakDC)
(define LfClipNoise LFClipNoise)
(define LfCub LFCub)
(define LfdClipNoise LFDClipNoise)
(define LfdNoise0 LFDNoise0)
(define LfdNoise1 LFDNoise1)
(define LfdNoise3 LFDNoise3)
(define LfNoise0 LFNoise0)
(define LfNoise1 LFNoise1)
(define LfNoise2 LFNoise2)
(define LfPar LFPar)
(define LfPulse LFPulse)
(define LfSaw LFSaw)
(define LfTri LFTri)
(define Lpf LPF)
(define Lpz1 LPZ2)
(define Lpz2 LPZ2)
(define MoogFf MoogFF)
(define Rcd RCD)
(define Rhpf RHPF)
(define Rlpf RLPF)
(define SetResetFf SetResetFF)
(define SinOscFb SinOscFB)
(define Sos SOS)
(define TiRand TIRand)
(define ToggleFf ToggleFF)
(define TwIndex TWindex)

(define RingzBank
  (lambda (input freq amp time)
    (Klank input 1 0 1 (klankData freq amp time))))

(define SinOscBank
  (lambda (freq amp phase)
    (Klang 1 0 (klangData freq amp phase))))

(define Product
  (lambda (l)
    (foldl1 Mul l)))

(define List list)
(define Reverse reverse)
(define Transpose transpose)
(define Channels mceChannels)

(define Dup dup)

(define Negated Neg)

(define rand0 Rand_)
(define linRand0 LinRand_)

(define Sine
  (lambda (trig dur)
    (let ((e (Env (list 0 0 1 0) (list 0 (Fdiv dur 2) (Fdiv dur 2)) '(sine sine sine) -1 1)))
      (EnvGen trig 1 0 1 0 e))))

(define BufAlloc
  (lambda (numChannels numFrames)
    (LocalBuf numFrames numChannels)))

(define Perc
  (lambda (trig attackTime releaseTime curve)
    (EnvGen trig 1 0 1 0 (EnvPerc attackTime releaseTime 1 curve))))

(define mean
  (lambda (input)
    (Fdiv (Mix input) (size input))))

(define DustN
  (lambda (n density)
    (! (lambda () (Dust density)) n)))

(define WhiteNoiseN
  (lambda (n)
    (! (lambda () (WhiteNoise)) n)))

(define PinkNoiseN
  (lambda (n)
    (! (lambda () (PinkNoise)) n)))

(define BrownNoiseN
  (lambda (n)
    (! (lambda () (BrownNoise)) n)))

(define LfNoise1N
  (lambda (n freq)
    (! (lambda () (LfNoise1 freq)) n)))

(define LfNoise2N
  (lambda (n freq)
    (! (lambda () (LfNoise2 freq)) n)))

(define EqPan2
  (lambda (in pos)
    (Pan2 in pos 1)))
