; type CcEvent (w/gate,x,y,z/force,orientation,radius-x,radius-y,pitch,pitch-x,pitch-y)

; ugen -> ugen -> ccevent
(define CcEventAddr
  (lambda (k0 c)
    (apply values (mceChannels (ControlIn 10 (Add k0 (Mul c 10)))))))

; ugen -> ugen -> int -> (ccevent -> ugen) -> ugen
;
; (CcEventVoicerAddr 13000 0 1 (lambda (g x y z o rx ry p) (list g x y z o rx ry p)))
(define CcEventVoicerAddr
  (lambda (k0 c0 n f)
    (map
     (lambda (c) (call-with-values (lambda () (CcEventAddr k0 (Add c0 c))) f))
     (enumFromTo 0 (- n 1)))))

; int -> (ccevent -> ugen) -> ugen
(define Voicer
  (lambda (n f)
    (CcEventVoicerAddr (ctl "eventAddr" 13000) (ctl "eventZero" 0) n f)))

(define UnitCps
  (lambda (n)
    (MidiCps (Mul n 127))))
