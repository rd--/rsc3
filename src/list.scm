; [a] -> int -> [a]
;
; (== (extend 1 6) (list 1 1 1 1 1 1))
; (== (extend (list 1 2 3) 6) (list 1 2 3 1 2 3))
; (== (extend '(x y z) 7) '(x y z x y z x))
(define extend
  (lambda (l n)
    (if
     (list? l)
     (let ((z (length l)))
       (cond ((= z n) l)
             ((> z n) (take n l))
             (else (extend (append l l) n))))
     (replicate n l))))

; [t]|any -> int
;
; (equal? (map lengthOrOne (list (list 1 2 3) 4)) (list 3 1))
(define lengthOrOne
  (lambda (l)
    (if (list? l) (length l) 1)))

; [a] -> int -> [a]
(define take-cycle
  (lambda (l n)
    (if (null? l)
        nil
        (cons (head l)
              (take-cycle (drop n l) n)))))

; (a -> a -> a) -> ([a] -> [a])
(define differentiate-with
  (lambda (f)
    (lambda (l)
      (zipWith f l (cons 0 l)))))

; num a => [a] -> [a]
(define differentiate
  (differentiate-with -))

; (a -> a -> a) -> ([a] -> [a])
(define integrate-with
  (lambda (f)
    (lambda (l)
      (let ((x (car l))
            (xs (cdr l))
            (g (lambda (a x) (let ((y (f a x))) (twoTuple y y)))))
        (cons x (snd (mapAccumL g x xs)))))))

; num a => [a] -> [a]
(define integrate
  (integrate-with +))

; (n -> n -> n) -> [n] -> [n]
(define d->dx-by
  (lambda (f l)
    (zipWith f (drop 1 l) l)))

; num n => [n] -> [n]
(define d->dx
  (lambda (l)
    (zipWith - (drop 1 l) l)))

; int -> [any] -> [any]
(define without
  (lambda (n l)
    (append (take n l) (drop (+ n 1) l))))

; [int] -> bool
(define consecutive?
  (lambda (l)
    (let ((x (head l))
          (xs (tail l)))
      (or (null? xs)
          (and (= (+ x 1) (head xs))
               (consecutive? xs))))))
