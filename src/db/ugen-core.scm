(define BinaryOpUGen
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 nil)))

(define UnaryOpUGen
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 nil)))

(define A2K
  (lambda (input)
    (construct-ugen "A2K" control-rate (list input) nil 1 nil)))

(define APF
  (lambda (input freq radius)
    (construct-ugen "APF" (list 0) (list input freq radius) nil 1 nil)))

(define AllpassC
  (lambda (input maxdelaytime delaytime decaytime)
    (construct-ugen "AllpassC" (list 0) (list input maxdelaytime delaytime decaytime) nil 1 nil)))

(define AllpassL
  (lambda (input maxdelaytime delaytime decaytime)
    (construct-ugen "AllpassL" (list 0) (list input maxdelaytime delaytime decaytime) nil 1 nil)))

(define AllpassN
  (lambda (input maxdelaytime delaytime decaytime)
    (construct-ugen "AllpassN" (list 0) (list input maxdelaytime delaytime decaytime) nil 1 nil)))

(define AmpComp
  (lambda (freq root exp_)
    (construct-ugen "AmpComp" audio-rate (list freq root exp_) nil 1 nil)))

(define AmpCompA
  (lambda (freq root minAmp rootAmp)
    (construct-ugen "AmpCompA" audio-rate (list freq root minAmp rootAmp) nil 1 nil)))

(define Amplitude
  (lambda (input attackTime releaseTime)
    (construct-ugen "Amplitude" audio-rate (list input attackTime releaseTime) nil 1 nil)))

(define BAllPass
  (lambda (input freq rq)
    (construct-ugen "BAllPass" (list 0) (list input freq rq) nil 1 nil)))

(define BBandPass
  (lambda (input freq bw)
    (construct-ugen "BBandPass" (list 0) (list input freq bw) nil 1 nil)))

(define BBandStop
  (lambda (input freq bw)
    (construct-ugen "BBandStop" (list 0) (list input freq bw) nil 1 nil)))

(define BHiPass
  (lambda (input freq rq)
    (construct-ugen "BHiPass" (list 0) (list input freq rq) nil 1 nil)))

(define BHiShelf
  (lambda (input freq rs db)
    (construct-ugen "BHiShelf" (list 0) (list input freq rs db) nil 1 nil)))

(define BLowPass
  (lambda (input freq rq)
    (construct-ugen "BLowPass" (list 0) (list input freq rq) nil 1 nil)))

(define BLowShelf
  (lambda (input freq rs db)
    (construct-ugen "BLowShelf" (list 0) (list input freq rs db) nil 1 nil)))

(define BPF
  (lambda (input freq rq)
    (construct-ugen "BPF" (list 0) (list input freq rq) nil 1 nil)))

(define BPZ2
  (lambda (input)
    (construct-ugen "BPZ2" (list 0) (list input) nil 1 nil)))

(define BPeakEQ
  (lambda (input freq rq db)
    (construct-ugen "BPeakEQ" (list 0) (list input freq rq db) nil 1 nil)))

(define BRF
  (lambda (input freq rq)
    (construct-ugen "BRF" (list 0) (list input freq rq) nil 1 nil)))

(define BRZ2
  (lambda (input)
    (construct-ugen "BRZ2" (list 0) (list input) nil 1 nil)))

(define Balance2
  (lambda (left right pos level)
    (construct-ugen "Balance2" (list 0 1) (list left right pos level) nil 2 nil)))

(define Ball
  (lambda (input g damp friction)
    (construct-ugen "Ball" audio-rate (list input g damp friction) nil 1 nil)))

(define BeatTrack
  (lambda (chain lock)
    (construct-ugen "BeatTrack" control-rate (list chain lock) nil 4 nil)))

(define BeatTrack2
  (lambda (busindex numfeatures windowsize phaseaccuracy lock weightingscheme)
    (construct-ugen "BeatTrack2" control-rate (list busindex numfeatures windowsize phaseaccuracy lock weightingscheme) nil 6 nil)))

(define BiPanB2
  (lambda (inA inB azimuth gain)
    (construct-ugen "BiPanB2" audio-rate (list inA inB azimuth gain) nil 3 nil)))

(define Blip
  (lambda (freq numharm)
    (construct-ugen "Blip" audio-rate (list freq numharm) nil 1 nil)))

(define BlockSize
  (lambda ()
    (construct-ugen "BlockSize" scalar-rate nil nil 1 nil)))

(define BrownNoise
  (lambda ()
    (construct-ugen "BrownNoise" audio-rate nil nil 1 nil)))

(define BufAllpassC
  (lambda (buf input delaytime decaytime)
    (construct-ugen "BufAllpassC" (list 1) (list buf input delaytime decaytime) nil 1 nil)))

(define BufAllpassL
  (lambda (buf input delaytime decaytime)
    (construct-ugen "BufAllpassL" (list 1) (list buf input delaytime decaytime) nil 1 nil)))

(define BufAllpassN
  (lambda (buf input delaytime decaytime)
    (construct-ugen "BufAllpassN" (list 1) (list buf input delaytime decaytime) nil 1 nil)))

(define BufChannels
  (lambda (bufnum)
    (construct-ugen "BufChannels" control-rate (list bufnum) nil 1 nil)))

(define BufCombC
  (lambda (buf input delaytime decaytime)
    (construct-ugen "BufCombC" (list 1) (list buf input delaytime decaytime) nil 1 nil)))

(define BufCombL
  (lambda (buf input delaytime decaytime)
    (construct-ugen "BufCombL" (list 1) (list buf input delaytime decaytime) nil 1 nil)))

(define BufCombN
  (lambda (buf input delaytime decaytime)
    (construct-ugen "BufCombN" (list 1) (list buf input delaytime decaytime) nil 1 nil)))

(define BufDelayC
  (lambda (buf input delaytime)
    (construct-ugen "BufDelayC" (list 1) (list buf input delaytime) nil 1 nil)))

(define BufDelayL
  (lambda (buf input delaytime)
    (construct-ugen "BufDelayL" (list 1) (list buf input delaytime) nil 1 nil)))

(define BufDelayN
  (lambda (buf input delaytime)
    (construct-ugen "BufDelayN" (list 1) (list buf input delaytime) nil 1 nil)))

(define BufDur
  (lambda (bufnum)
    (construct-ugen "BufDur" control-rate (list bufnum) nil 1 nil)))

(define BufFrames
  (lambda (bufnum)
    (construct-ugen "BufFrames" control-rate (list bufnum) nil 1 nil)))

(define BufRateScale
  (lambda (bufnum)
    (construct-ugen "BufRateScale" control-rate (list bufnum) nil 1 nil)))

(define BufRd
  (lambda (nc bufnum phase loop interpolation)
    (construct-ugen "BufRd" audio-rate (list bufnum phase loop interpolation) nil nc nil)))

(define BufSampleRate
  (lambda (bufnum)
    (construct-ugen "BufSampleRate" control-rate (list bufnum) nil 1 nil)))

(define BufSamples
  (lambda (bufnum)
    (construct-ugen "BufSamples" control-rate (list bufnum) nil 1 nil)))

(define BufWr
  (lambda (bufnum phase loop inputArray)
    (construct-ugen "BufWr" (list 3) (list bufnum phase loop) inputArray 1 nil)))

(define COsc
  (lambda (bufnum freq beats)
    (construct-ugen "COsc" audio-rate (list bufnum freq beats) nil 1 nil)))

(define CheckBadValues
  (lambda (input id_ post)
    (construct-ugen "CheckBadValues" (list 0) (list input id_ post) nil 1 nil)))

;(define ClearBuf
;  (lambda (buf)
;    (construct-ugen "ClearBuf" scalar-rate (list buf) nil 1 nil)))

(define Clip
  (lambda (input lo hi)
    (construct-ugen "Clip" (list 0) (list input lo hi) nil 1 nil)))

(define ClipNoise
  (lambda ()
    (construct-ugen "ClipNoise" audio-rate nil nil 1 nil)))

(define CoinGate
  (lambda (prob input)
    (construct-ugen "CoinGate" (list 1) (list prob input) nil 1 nil)))

(define CombC
  (lambda (input maxdelaytime delaytime decaytime)
    (construct-ugen "CombC" (list 0) (list input maxdelaytime delaytime decaytime) nil 1 nil)))

(define CombL
  (lambda (input maxdelaytime delaytime decaytime)
    (construct-ugen "CombL" (list 0) (list input maxdelaytime delaytime decaytime) nil 1 nil)))

(define CombN
  (lambda (input maxdelaytime delaytime decaytime)
    (construct-ugen "CombN" (list 0) (list input maxdelaytime delaytime decaytime) nil 1 nil)))

(define Compander
  (lambda (input control_ thresh slopeBelow slopeAbove clampTime relaxTime)
    (construct-ugen "Compander" (list 0) (list input control_ thresh slopeBelow slopeAbove clampTime relaxTime) nil 1 nil)))

(define CompanderD
  (lambda (input thresh slopeBelow slopeAbove clampTime relaxTime)
    (construct-ugen "CompanderD" audio-rate (list input thresh slopeBelow slopeAbove clampTime relaxTime) nil 1 nil)))

(define ControlDur
  (lambda ()
    (construct-ugen "ControlDur" scalar-rate nil nil 1 nil)))

(define ControlRate
  (lambda ()
    (construct-ugen "ControlRate" scalar-rate nil nil 1 nil)))

(define Convolution
  (lambda (input kernel framesize)
    (construct-ugen "Convolution" audio-rate (list input kernel framesize) nil 1 nil)))

(define Convolution2
  (lambda (input kernel trigger framesize)
    (construct-ugen "Convolution2" audio-rate (list input kernel trigger framesize) nil 1 nil)))

(define Convolution2L
  (lambda (input kernel trigger framesize crossfade)
    (construct-ugen "Convolution2L" audio-rate (list input kernel trigger framesize crossfade) nil 1 nil)))

(define Convolution3
  (lambda (input kernel trigger framesize)
    (construct-ugen "Convolution3" audio-rate (list input kernel trigger framesize) nil 1 nil)))

(define Crackle
  (lambda (chaosParam)
    (construct-ugen "Crackle" audio-rate (list chaosParam) nil 1 nil)))

(define CuspL
  (lambda (freq a b xi)
    (construct-ugen "CuspL" audio-rate (list freq a b xi) nil 1 nil)))

(define CuspN
  (lambda (freq a b xi)
    (construct-ugen "CuspN" audio-rate (list freq a b xi) nil 1 nil)))

(define DC
  (lambda (input)
    (construct-ugen "DC" audio-rate (list input) nil 1 nil)))

(define Dbrown
  (lambda (length_ lo hi step)
    (construct-ugen "Dbrown" demand-rate (list length_ lo hi step) nil 1 nil)))

(define Dbufrd
  (lambda (bufnum phase loop)
    (construct-ugen "Dbufrd" demand-rate (list bufnum phase loop) nil 1 nil)))

(define Dbufwr
  (lambda (bufnum phase input loop)
    (construct-ugen "Dbufwr" demand-rate (list bufnum phase input loop) nil 1 nil)))

(define Dconst
  (lambda (sum_ input tolerance)
    (construct-ugen "Dconst" demand-rate (list sum_ input tolerance) nil 1 nil)))

(define Decay
  (lambda (input decayTime)
    (construct-ugen "Decay" (list 0) (list input decayTime) nil 1 nil)))

(define Decay2
  (lambda (input attackTime decayTime)
    (construct-ugen "Decay2" (list 0) (list input attackTime decayTime) nil 1 nil)))

(define DecodeB2
  (lambda (nc w x y orientation)
    (construct-ugen "DecodeB2" (list 0 1 2) (list w x y orientation) nil nc nil)))

(define DegreeToKey
  (lambda (bufnum input octave)
    (construct-ugen "DegreeToKey" (list 1) (list bufnum input octave) nil 1 nil)))

(define DelTapRd
  (lambda (buffer phase delTime interp)
    (construct-ugen "DelTapRd" (list 1) (list buffer phase delTime interp) nil 1 nil)))

(define DelTapWr
  (lambda (buffer input)
    (construct-ugen "DelTapWr" (list 1) (list buffer input) nil 1 nil)))

(define Delay1
  (lambda (input)
    (construct-ugen "Delay1" (list 0) (list input) nil 1 nil)))

(define Delay2
  (lambda (input)
    (construct-ugen "Delay2" (list 0) (list input) nil 1 nil)))

(define DelayC
  (lambda (input maxdelaytime delaytime)
    (construct-ugen "DelayC" (list 0) (list input maxdelaytime delaytime) nil 1 nil)))

(define DelayL
  (lambda (input maxdelaytime delaytime)
    (construct-ugen "DelayL" (list 0) (list input maxdelaytime delaytime) nil 1 nil)))

(define DelayN
  (lambda (input maxdelaytime delaytime)
    (construct-ugen "DelayN" (list 0) (list input maxdelaytime delaytime) nil 1 nil)))

(define Demand
  (lambda (trig_ reset demandUGens)
    (construct-ugen "Demand" (list 0) (list trig_ reset) demandUGens (length (mceChannels demandUGens)) nil)))

(define DemandEnvGen
  (lambda (level dur shape curve gate_ reset levelScale levelBias timeScale doneAction)
    (construct-ugen "DemandEnvGen" audio-rate (list level dur shape curve gate_ reset levelScale levelBias timeScale doneAction) nil 1 nil)))

(define DetectIndex
  (lambda (bufnum input)
    (construct-ugen "DetectIndex" (list 1) (list bufnum input) nil 1 nil)))

(define DetectSilence
  (lambda (input amp time doneAction)
    (construct-ugen "DetectSilence" (list 0) (list input amp time doneAction) nil 1 nil)))

(define Dgeom
  (lambda (length_ start grow)
    (construct-ugen "Dgeom" demand-rate (list length_ start grow) nil 1 nil)))

(define Dibrown
  (lambda (length_ lo hi step)
    (construct-ugen "Dibrown" demand-rate (list length_ lo hi step) nil 1 nil)))

(define DiskIn
  (lambda (nc bufnum loop)
    (construct-ugen "DiskIn" audio-rate (list bufnum loop) nil nc nil)))

(define DiskOut
  (lambda (bufnum input)
    (construct-ugen "DiskOut" audio-rate (list bufnum) input 1 nil)))

(define Diwhite
  (lambda (length_ lo hi)
    (construct-ugen "Diwhite" demand-rate (list length_ lo hi) nil 1 nil)))

(define Done
  (lambda (src)
    (construct-ugen "Done" control-rate (list src) nil 1 nil)))

(define Dpoll
  (lambda (input label_ run trigid)
    (construct-ugen "Dpoll" demand-rate (list input label_ run trigid) nil 1 nil)))

(define Drand
  (lambda (repeats list_)
    (construct-ugen "Drand" demand-rate (list repeats) list_ 1 nil)))

(define Dreset
  (lambda (input reset)
    (construct-ugen "Dreset" demand-rate (list input reset) nil 1 nil)))

(define Dseq
  (lambda (repeats list_)
    (construct-ugen "Dseq" demand-rate (list repeats) list_ 1 nil)))

(define Dser
  (lambda (repeats list_)
    (construct-ugen "Dser" demand-rate (list repeats) list_ 1 nil)))

(define Dseries
  (lambda (length_ start step)
    (construct-ugen "Dseries" demand-rate (list length_ start step) nil 1 nil)))

(define Dshuf
  (lambda (repeats list_)
    (construct-ugen "Dshuf" demand-rate (list repeats) list_ 1 nil)))

(define Dstutter
  (lambda (n input)
    (construct-ugen "Dstutter" demand-rate (list n input) nil 1 nil)))

(define Dswitch
  (lambda (index list_)
    (construct-ugen "Dswitch" demand-rate (list index) list_ 1 nil)))

(define Dswitch1
  (lambda (index list_)
    (construct-ugen "Dswitch1" demand-rate (list index) list_ 1 nil)))

(define Dunique
  (lambda (source maxBufferSize protected)
    (construct-ugen "Dunique" demand-rate (list source maxBufferSize protected) nil 1 nil)))

(define Dust
  (lambda (density)
    (construct-ugen "Dust" audio-rate (list density) nil 1 nil)))

(define Dust2
  (lambda (density)
    (construct-ugen "Dust2" audio-rate (list density) nil 1 nil)))

(define Duty
  (lambda (dur reset doneAction level)
    (construct-ugen "Duty" audio-rate (list dur reset doneAction level) nil 1 nil)))

(define Dwhite
  (lambda (length_ lo hi)
    (construct-ugen "Dwhite" demand-rate (list length_ lo hi) nil 1 nil)))

(define Dwrand
  (lambda (repeats weights list_)
    (construct-ugen "Dwrand" demand-rate (list repeats weights) list_ 1 nil)))

(define Dxrand
  (lambda (repeats list_)
    (construct-ugen "Dxrand" demand-rate (list repeats) list_ 1 nil)))

(define EnvGen
  (lambda (gate_ levelScale levelBias timeScale doneAction envelope_)
    (construct-ugen "EnvGen" audio-rate (list gate_ levelScale levelBias timeScale doneAction) envelope_ 1 nil)))

(define ExpRand
  (lambda (lo hi)
    (construct-ugen "ExpRand" scalar-rate (list lo hi) nil 1 nil)))

(define FBSineC
  (lambda (freq im fb a c xi yi)
    (construct-ugen "FBSineC" audio-rate (list freq im fb a c xi yi) nil 1 nil)))

(define FBSineL
  (lambda (freq im fb a c xi yi)
    (construct-ugen "FBSineL" audio-rate (list freq im fb a c xi yi) nil 1 nil)))

(define FBSineN
  (lambda (freq im fb a c xi yi)
    (construct-ugen "FBSineN" audio-rate (list freq im fb a c xi yi) nil 1 nil)))

(define FFT
  (lambda (buffer input hop wintype active winsize)
    (construct-ugen "FFT" control-rate (list buffer input hop wintype active winsize) nil 1 nil)))

(define FOS
  (lambda (input a0 a1 b1)
    (construct-ugen "FOS" (list 0) (list input a0 a1 b1) nil 1 nil)))

(define FSinOsc
  (lambda (freq iphase)
    (construct-ugen "FSinOsc" audio-rate (list freq iphase) nil 1 nil)))

(define Fold
  (lambda (input lo hi)
    (construct-ugen "Fold" (list 0) (list input lo hi) nil 1 nil)))

(define Formant
  (lambda (fundfreq formfreq bwfreq)
    (construct-ugen "Formant" audio-rate (list fundfreq formfreq bwfreq) nil 1 nil)))

(define Formlet
  (lambda (input freq attacktime decaytime)
    (construct-ugen "Formlet" (list 0) (list input freq attacktime decaytime) nil 1 nil)))

(define Free
  (lambda (trig_ id_)
    (construct-ugen "Free" (list 0) (list trig_ id_) nil 1 nil)))

(define FreeSelf
  (lambda (input)
    (construct-ugen "FreeSelf" control-rate (list input) nil 1 nil)))

(define FreeSelfWhenDone
  (lambda (src)
    (construct-ugen "FreeSelfWhenDone" control-rate (list src) nil 1 nil)))

(define FreeVerb
  (lambda (input mix room damp)
    (construct-ugen "FreeVerb" (list 0) (list input mix room damp) nil 1 nil)))

(define FreeVerb2
  (lambda (input in2 mix room damp)
    (construct-ugen "FreeVerb2" (list 0) (list input in2 mix room damp) nil 2 nil)))

(define FreqShift
  (lambda (input freq phase)
    (construct-ugen "FreqShift" audio-rate (list input freq phase) nil 1 nil)))

(define GVerb
  (lambda (input roomsize revtime damping inputbw spread drylevel earlyreflevel taillevel maxroomsize)
    (construct-ugen "GVerb" (list 0) (list input roomsize revtime damping inputbw spread drylevel earlyreflevel taillevel maxroomsize) nil 2 nil)))

(define Gate
  (lambda (input trig_)
    (construct-ugen "Gate" (list 0) (list input trig_) nil 1 nil)))

(define GbmanL
  (lambda (freq xi yi)
    (construct-ugen "GbmanL" audio-rate (list freq xi yi) nil 1 nil)))

(define GbmanN
  (lambda (freq xi yi)
    (construct-ugen "GbmanN" audio-rate (list freq xi yi) nil 1 nil)))

(define Gendy1
  (lambda (ampdist durdist adparam ddparam minfreq maxfreq ampscale durscale initCPs knum)
    (construct-ugen "Gendy1" audio-rate (list ampdist durdist adparam ddparam minfreq maxfreq ampscale durscale initCPs knum) nil 1 nil)))

(define Gendy2
  (lambda (ampdist durdist adparam ddparam minfreq maxfreq ampscale durscale initCPs knum a c)
    (construct-ugen "Gendy2" audio-rate (list ampdist durdist adparam ddparam minfreq maxfreq ampscale durscale initCPs knum a c) nil 1 nil)))

(define Gendy3
  (lambda (ampdist durdist adparam ddparam freq ampscale durscale initCPs knum)
    (construct-ugen "Gendy3" audio-rate (list ampdist durdist adparam ddparam freq ampscale durscale initCPs knum) nil 1 nil)))

(define GrainBuf
  (lambda (nc trigger dur sndbuf rate_ pos interp pan envbufnum maxGrains)
    (construct-ugen "GrainBuf" audio-rate (list trigger dur sndbuf rate_ pos interp pan envbufnum maxGrains) nil nc nil)))

(define GrainFM
  (lambda (nc trigger dur carfreq modfreq index pan envbufnum maxGrains)
    (construct-ugen "GrainFM" audio-rate (list trigger dur carfreq modfreq index pan envbufnum maxGrains) nil nc nil)))

(define GrainIn
  (lambda (nc trigger dur input pan envbufnum maxGrains)
    (construct-ugen "GrainIn" audio-rate (list trigger dur input pan envbufnum maxGrains) nil nc nil)))

(define GrainSin
  (lambda (nc trigger dur freq pan envbufnum maxGrains)
    (construct-ugen "GrainSin" audio-rate (list trigger dur freq pan envbufnum maxGrains) nil nc nil)))

(define GrayNoise
  (lambda ()
    (construct-ugen "GrayNoise" audio-rate nil nil 1 nil)))

(define HPF
  (lambda (input freq)
    (construct-ugen "HPF" (list 0) (list input freq) nil 1 nil)))

(define HPZ1
  (lambda (input)
    (construct-ugen "HPZ1" (list 0) (list input) nil 1 nil)))

(define HPZ2
  (lambda (input)
    (construct-ugen "HPZ2" (list 0) (list input) nil 1 nil)))

(define Hasher
  (lambda (input)
    (construct-ugen "Hasher" (list 0) (list input) nil 1 nil)))

(define HenonC
  (lambda (freq a b x0 x1)
    (construct-ugen "HenonC" audio-rate (list freq a b x0 x1) nil 1 nil)))

(define HenonL
  (lambda (freq a b x0 x1)
    (construct-ugen "HenonL" audio-rate (list freq a b x0 x1) nil 1 nil)))

(define HenonN
  (lambda (freq a b x0 x1)
    (construct-ugen "HenonN" audio-rate (list freq a b x0 x1) nil 1 nil)))

(define Hilbert
  (lambda (input)
    (construct-ugen "Hilbert" (list 0) (list input) nil 2 nil)))

(define IEnvGen
  (lambda (index envelope_)
    (construct-ugen "IEnvGen" audio-rate (list index) envelope_ 1 nil)))

(define IFFT
  (lambda (buffer wintype winsize)
    (construct-ugen "IFFT" audio-rate (list buffer wintype winsize) nil 1 nil)))

(define IRand
  (lambda (lo hi)
    (construct-ugen "IRand" scalar-rate (list lo hi) nil 1 nil)))

(define Impulse
  (lambda (freq phase)
    (construct-ugen "Impulse" audio-rate (list freq phase) nil 1 nil)))

(define In
  (lambda (nc bus)
    (construct-ugen "In" audio-rate (list bus) nil nc nil)))

(define InFeedback
  (lambda (nc bus)
    (construct-ugen "InFeedback" audio-rate (list bus) nil nc nil)))

(define InRange
  (lambda (input lo hi)
    (construct-ugen "InRange" (list 0) (list input lo hi) nil 1 nil)))

(define InRect
  (lambda (x y rect)
    (construct-ugen "InRect" audio-rate (list x y rect) nil 1 nil)))

(define InTrig
  (lambda (nc bus)
    (construct-ugen "InTrig" control-rate (list bus) nil nc nil)))

(define Index
  (lambda (bufnum input)
    (construct-ugen "Index" (list 1) (list bufnum input) nil 1 nil)))

(define IndexInBetween
  (lambda (bufnum input)
    (construct-ugen "IndexInBetween" (list 1) (list bufnum input) nil 1 nil)))

(define IndexL
  (lambda (bufnum input)
    (construct-ugen "IndexL" (list 1) (list bufnum input) nil 1 nil)))

(define Integrator
  (lambda (input coef)
    (construct-ugen "Integrator" (list 0) (list input coef) nil 1 nil)))

(define K2A
  (lambda (input)
    (construct-ugen "K2A" audio-rate (list input) nil 1 nil)))

(define KeyState
  (lambda (keycode minval maxval lag)
    (construct-ugen "KeyState" control-rate (list keycode minval maxval lag) nil 1 nil)))

(define KeyTrack
  (lambda (chain keydecay chromaleak)
    (construct-ugen "KeyTrack" control-rate (list chain keydecay chromaleak) nil 1 nil)))

(define Klang
  (lambda (freqscale freqoffset specificationsArrayRef)
    (construct-ugen "Klang" audio-rate (list freqscale freqoffset) specificationsArrayRef 1 nil)))

(define Klank
  (lambda (input freqscale freqoffset decayscale specificationsArrayRef)
    (construct-ugen "Klank" (list 0) (list input freqscale freqoffset decayscale) specificationsArrayRef 1 nil)))

(define LFClipNoise
  (lambda (freq)
    (construct-ugen "LFClipNoise" audio-rate (list freq) nil 1 nil)))

(define LFCub
  (lambda (freq iphase)
    (construct-ugen "LFCub" audio-rate (list freq iphase) nil 1 nil)))

(define LFDClipNoise
  (lambda (freq)
    (construct-ugen "LFDClipNoise" audio-rate (list freq) nil 1 nil)))

(define LFDNoise0
  (lambda (freq)
    (construct-ugen "LFDNoise0" audio-rate (list freq) nil 1 nil)))

(define LFDNoise1
  (lambda (freq)
    (construct-ugen "LFDNoise1" audio-rate (list freq) nil 1 nil)))

(define LFDNoise3
  (lambda (freq)
    (construct-ugen "LFDNoise3" audio-rate (list freq) nil 1 nil)))

(define LFGauss
  (lambda (duration width iphase loop doneAction)
    (construct-ugen "LFGauss" audio-rate (list duration width iphase loop doneAction) nil 1 nil)))

(define LFNoise0
  (lambda (freq)
    (construct-ugen "LFNoise0" audio-rate (list freq) nil 1 nil)))

(define LFNoise1
  (lambda (freq)
    (construct-ugen "LFNoise1" audio-rate (list freq) nil 1 nil)))

(define LFNoise2
  (lambda (freq)
    (construct-ugen "LFNoise2" audio-rate (list freq) nil 1 nil)))

(define LFPar
  (lambda (freq iphase)
    (construct-ugen "LFPar" audio-rate (list freq iphase) nil 1 nil)))

(define LFPulse
  (lambda (freq iphase width)
    (construct-ugen "LFPulse" audio-rate (list freq iphase width) nil 1 nil)))

(define LFSaw
  (lambda (freq iphase)
    (construct-ugen "LFSaw" audio-rate (list freq iphase) nil 1 nil)))

(define LFTri
  (lambda (freq iphase)
    (construct-ugen "LFTri" audio-rate (list freq iphase) nil 1 nil)))

(define LPF
  (lambda (input freq)
    (construct-ugen "LPF" (list 0) (list input freq) nil 1 nil)))

(define LPZ1
  (lambda (input)
    (construct-ugen "LPZ1" (list 0) (list input) nil 1 nil)))

(define LPZ2
  (lambda (input)
    (construct-ugen "LPZ2" (list 0) (list input) nil 1 nil)))

(define Lag
  (lambda (input lagTime)
    (construct-ugen "Lag" (list 0) (list input lagTime) nil 1 nil)))

(define Lag2
  (lambda (input lagTime)
    (construct-ugen "Lag2" (list 0) (list input lagTime) nil 1 nil)))

(define Lag2UD
  (lambda (input lagTimeU lagTimeD)
    (construct-ugen "Lag2UD" (list 0) (list input lagTimeU lagTimeD) nil 1 nil)))

(define Lag3
  (lambda (input lagTime)
    (construct-ugen "Lag3" (list 0) (list input lagTime) nil 1 nil)))

(define Lag3UD
  (lambda (input lagTimeU lagTimeD)
    (construct-ugen "Lag3UD" (list 0) (list input lagTimeU lagTimeD) nil 1 nil)))

(define LagIn
  (lambda (nc bus lag)
    (construct-ugen "LagIn" control-rate (list bus lag) nil nc nil)))

(define LagUD
  (lambda (input lagTimeU lagTimeD)
    (construct-ugen "LagUD" (list 0) (list input lagTimeU lagTimeD) nil 1 nil)))

(define LastValue
  (lambda (input diff)
    (construct-ugen "LastValue" (list 0) (list input diff) nil 1 nil)))

(define Latch
  (lambda (input trig_)
    (construct-ugen "Latch" (list 0) (list input trig_) nil 1 nil)))

(define LatoocarfianC
  (lambda (freq a b c d xi yi)
    (construct-ugen "LatoocarfianC" audio-rate (list freq a b c d xi yi) nil 1 nil)))

(define LatoocarfianL
  (lambda (freq a b c d xi yi)
    (construct-ugen "LatoocarfianL" audio-rate (list freq a b c d xi yi) nil 1 nil)))

(define LatoocarfianN
  (lambda (freq a b c d xi yi)
    (construct-ugen "LatoocarfianN" audio-rate (list freq a b c d xi yi) nil 1 nil)))

(define LeakDC
  (lambda (input coef)
    (construct-ugen "LeakDC" (list 0) (list input coef) nil 1 nil)))

(define LeastChange
  (lambda (a b)
    (construct-ugen "LeastChange" audio-rate (list a b) nil 1 nil)))

(define Limiter
  (lambda (input level dur)
    (construct-ugen "Limiter" (list 0) (list input level dur) nil 1 nil)))

(define LinCongC
  (lambda (freq a c m xi)
    (construct-ugen "LinCongC" audio-rate (list freq a c m xi) nil 1 nil)))

(define LinCongL
  (lambda (freq a c m xi)
    (construct-ugen "LinCongL" audio-rate (list freq a c m xi) nil 1 nil)))

(define LinCongN
  (lambda (freq a c m xi)
    (construct-ugen "LinCongN" audio-rate (list freq a c m xi) nil 1 nil)))

(define LinExp
  (lambda (input srclo srchi dstlo dsthi)
    (construct-ugen "LinExp" (list 0) (list input srclo srchi dstlo dsthi) nil 1 nil)))

(define LinPan2
  (lambda (input pos level)
    (construct-ugen "LinPan2" (list 0) (list input pos level) nil 2 nil)))

(define LinRand
  (lambda (lo hi minmax)
    (construct-ugen "LinRand" scalar-rate (list lo hi minmax) nil 1 nil)))

(define LinXFade2
  (lambda (inA inB pan)
    (construct-ugen "LinXFade2" (list 0 1) (list inA inB pan) nil 1 nil)))

(define Line
  (lambda (start end dur doneAction)
    (construct-ugen "Line" audio-rate (list start end dur doneAction) nil 1 nil)))

(define Linen
  (lambda (gate_ attackTime susLevel releaseTime doneAction)
    (construct-ugen "Linen" control-rate (list gate_ attackTime susLevel releaseTime doneAction) nil 1 nil)))

(define LocalBuf
  (lambda (numChannels numFrames)
    (construct-ugen "LocalBuf" scalar-rate (list numChannels numFrames) nil 1 nil)))

(define LocalIn
  (lambda (nc default_)
    (construct-ugen "LocalIn" audio-rate nil default_ nc nil)))

(define LocalOut
  (lambda (input)
    (construct-ugen "LocalOut" (list 0) nil input 0 nil)))

(define Logistic
  (lambda (chaosParam freq init_)
    (construct-ugen "Logistic" audio-rate (list chaosParam freq init_) nil 1 nil)))

(define LorenzL
  (lambda (freq s r b h xi yi zi)
    (construct-ugen "LorenzL" audio-rate (list freq s r b h xi yi zi) nil 1 nil)))

(define Loudness
  (lambda (chain smask tmask)
    (construct-ugen "Loudness" control-rate (list chain smask tmask) nil 1 nil)))

(define MFCC
  (lambda (chain numcoeff)
    (construct-ugen "MFCC" control-rate (list chain numcoeff) nil 13 nil)))

(define MantissaMask
  (lambda (input bits)
    (construct-ugen "MantissaMask" (list 0) (list input bits) nil 1 nil)))

(define Median
  (lambda (length_ input)
    (construct-ugen "Median" (list 1) (list length_ input) nil 1 nil)))

(define MidEQ
  (lambda (input freq rq db)
    (construct-ugen "MidEQ" (list 0) (list input freq rq db) nil 1 nil)))

(define ModDif
  (lambda (x y mod_)
    (construct-ugen "ModDif" (list 0) (list x y mod_) nil 1 nil)))

(define MoogFF
  (lambda (input freq gain reset)
    (construct-ugen "MoogFF" (list 0) (list input freq gain reset) nil 1 nil)))

(define MostChange
  (lambda (a b)
    (construct-ugen "MostChange" (list 0 1) (list a b) nil 1 nil)))

(define MouseButton
  (lambda (minval maxval lag)
    (construct-ugen "MouseButton" control-rate (list minval maxval lag) nil 1 nil)))

(define MouseX
  (lambda (minval maxval warp lag)
    (construct-ugen "MouseX" control-rate (list minval maxval warp lag) nil 1 nil)))

(define MouseY
  (lambda (minval maxval warp lag)
    (construct-ugen "MouseY" control-rate (list minval maxval warp lag) nil 1 nil)))

(define NRand
  (lambda (lo hi n)
    (construct-ugen "NRand" scalar-rate (list lo hi n) nil 1 nil)))

(define NodeID
  (lambda ()
    (construct-ugen "NodeID" scalar-rate nil nil 1 nil)))

(define Normalizer
  (lambda (input level dur)
    (construct-ugen "Normalizer" (list 0) (list input level dur) nil 1 nil)))

(define NumAudioBuses
  (lambda ()
    (construct-ugen "NumAudioBuses" scalar-rate nil nil 1 nil)))

(define NumBuffers
  (lambda ()
    (construct-ugen "NumBuffers" scalar-rate nil nil 1 nil)))

(define NumControlBuses
  (lambda ()
    (construct-ugen "NumControlBuses" scalar-rate nil nil 1 nil)))

(define NumInputBuses
  (lambda ()
    (construct-ugen "NumInputBuses" scalar-rate nil nil 1 nil)))

(define NumOutputBuses
  (lambda ()
    (construct-ugen "NumOutputBuses" scalar-rate nil nil 1 nil)))

(define NumRunningSynths
  (lambda ()
    (construct-ugen "NumRunningSynths" scalar-rate nil nil 1 nil)))

(define OffsetOut
  (lambda (bus input)
    (construct-ugen "OffsetOut" (list 1) (list bus) input 0 nil)))

(define OnePole
  (lambda (input coef)
    (construct-ugen "OnePole" (list 0) (list input coef) nil 1 nil)))

(define OneZero
  (lambda (input coef)
    (construct-ugen "OneZero" (list 0) (list input coef) nil 1 nil)))

(define Onsets
  (lambda (chain threshold odftype relaxtime floor_ mingap medianspan whtype rawodf)
    (construct-ugen "Onsets" control-rate (list chain threshold odftype relaxtime floor_ mingap medianspan whtype rawodf) nil 1 nil)))

(define Osc
  (lambda (bufnum freq phase)
    (construct-ugen "Osc" audio-rate (list bufnum freq phase) nil 1 nil)))

(define OscN
  (lambda (bufnum freq phase)
    (construct-ugen "OscN" audio-rate (list bufnum freq phase) nil 1 nil)))

(define Out
  (lambda (bus input)
    (construct-ugen "Out" (list 1) (list bus) input 0 nil)))

(define PSinGrain
  (lambda (freq dur amp)
    (construct-ugen "PSinGrain" audio-rate (list freq dur amp) nil 1 nil)))

(define PV_Add
  (lambda (bufferA bufferB)
    (construct-ugen "PV_Add" control-rate (list bufferA bufferB) nil 1 nil)))

(define PV_BinScramble
  (lambda (buffer wipe width trig_)
    (construct-ugen "PV_BinScramble" control-rate (list buffer wipe width trig_) nil 1 nil)))

(define PV_BinShift
  (lambda (buffer stretch shift interp)
    (construct-ugen "PV_BinShift" control-rate (list buffer stretch shift interp) nil 1 nil)))

(define PV_BinWipe
  (lambda (bufferA bufferB wipe)
    (construct-ugen "PV_BinWipe" control-rate (list bufferA bufferB wipe) nil 1 nil)))

(define PV_BrickWall
  (lambda (buffer wipe)
    (construct-ugen "PV_BrickWall" control-rate (list buffer wipe) nil 1 nil)))

(define PV_ConformalMap
  (lambda (buffer areal aimag)
    (construct-ugen "PV_ConformalMap" control-rate (list buffer areal aimag) nil 1 nil)))

(define PV_Conj
  (lambda (buffer)
    (construct-ugen "PV_Conj" control-rate (list buffer) nil 1 nil)))

(define PV_Copy
  (lambda (bufferA bufferB)
    (construct-ugen "PV_Copy" control-rate (list bufferA bufferB) nil 1 nil)))

(define PV_CopyPhase
  (lambda (bufferA bufferB)
    (construct-ugen "PV_CopyPhase" control-rate (list bufferA bufferB) nil 1 nil)))

(define PV_Diffuser
  (lambda (buffer trig_)
    (construct-ugen "PV_Diffuser" control-rate (list buffer trig_) nil 1 nil)))

(define PV_Div
  (lambda (bufferA bufferB)
    (construct-ugen "PV_Div" control-rate (list bufferA bufferB) nil 1 nil)))

;(define PV_HainsworthFoote
;  (lambda (maxSize)
;    (construct-ugen "PV_HainsworthFoote" control-rate (list maxSize) nil 1 nil)))

;(define PV_JensenAndersen
;  (lambda (maxSize)
;    (construct-ugen "PV_JensenAndersen" control-rate (list maxSize) nil 1 nil)))

(define PV_LocalMax
  (lambda (buffer threshold)
    (construct-ugen "PV_LocalMax" control-rate (list buffer threshold) nil 1 nil)))

(define PV_MagAbove
  (lambda (buffer threshold)
    (construct-ugen "PV_MagAbove" control-rate (list buffer threshold) nil 1 nil)))

(define PV_MagBelow
  (lambda (buffer threshold)
    (construct-ugen "PV_MagBelow" control-rate (list buffer threshold) nil 1 nil)))

(define PV_MagClip
  (lambda (buffer threshold)
    (construct-ugen "PV_MagClip" control-rate (list buffer threshold) nil 1 nil)))

(define PV_MagDiv
  (lambda (bufferA bufferB zeroed)
    (construct-ugen "PV_MagDiv" control-rate (list bufferA bufferB zeroed) nil 1 nil)))

(define PV_MagFreeze
  (lambda (buffer freeze)
    (construct-ugen "PV_MagFreeze" control-rate (list buffer freeze) nil 1 nil)))

(define PV_MagMul
  (lambda (bufferA bufferB)
    (construct-ugen "PV_MagMul" control-rate (list bufferA bufferB) nil 1 nil)))

(define PV_MagNoise
  (lambda (buffer)
    (construct-ugen "PV_MagNoise" control-rate (list buffer) nil 1 nil)))

(define PV_MagShift
  (lambda (buffer stretch shift)
    (construct-ugen "PV_MagShift" control-rate (list buffer stretch shift) nil 1 nil)))

(define PV_MagSmear
  (lambda (buffer bins)
    (construct-ugen "PV_MagSmear" control-rate (list buffer bins) nil 1 nil)))

(define PV_MagSquared
  (lambda (buffer)
    (construct-ugen "PV_MagSquared" control-rate (list buffer) nil 1 nil)))

(define PV_Max
  (lambda (bufferA bufferB)
    (construct-ugen "PV_Max" control-rate (list bufferA bufferB) nil 1 nil)))

(define PV_Min
  (lambda (bufferA bufferB)
    (construct-ugen "PV_Min" control-rate (list bufferA bufferB) nil 1 nil)))

(define PV_Mul
  (lambda (bufferA bufferB)
    (construct-ugen "PV_Mul" control-rate (list bufferA bufferB) nil 1 nil)))

(define PV_PhaseShift
  (lambda (buffer shift integrate)
    (construct-ugen "PV_PhaseShift" control-rate (list buffer shift integrate) nil 1 nil)))

(define PV_PhaseShift270
  (lambda (buffer)
    (construct-ugen "PV_PhaseShift270" control-rate (list buffer) nil 1 nil)))

(define PV_PhaseShift90
  (lambda (buffer)
    (construct-ugen "PV_PhaseShift90" control-rate (list buffer) nil 1 nil)))

(define PV_RandComb
  (lambda (buffer wipe trig_)
    (construct-ugen "PV_RandComb" control-rate (list buffer wipe trig_) nil 1 nil)))

(define PV_RandWipe
  (lambda (bufferA bufferB wipe trig_)
    (construct-ugen "PV_RandWipe" control-rate (list bufferA bufferB wipe trig_) nil 1 nil)))

(define PV_RectComb
  (lambda (buffer numTeeth phase width)
    (construct-ugen "PV_RectComb" control-rate (list buffer numTeeth phase width) nil 1 nil)))

(define PV_RectComb2
  (lambda (bufferA bufferB numTeeth phase width)
    (construct-ugen "PV_RectComb2" control-rate (list bufferA bufferB numTeeth phase width) nil 1 nil)))

(define Pan2
  (lambda (input pos level)
    (construct-ugen "Pan2" (list 0) (list input pos level) nil 2 nil)))

(define Pan4
  (lambda (input xpos ypos level)
    (construct-ugen "Pan4" audio-rate (list input xpos ypos level) nil 4 nil)))

(define PanAz
  (lambda (nc input pos level width orientation)
    (construct-ugen "PanAz" (list 0) (list input pos level width orientation) nil nc nil)))

(define PanB
  (lambda (input azimuth elevation gain)
    (construct-ugen "PanB" audio-rate (list input azimuth elevation gain) nil 4 nil)))

(define PanB2
  (lambda (input azimuth gain)
    (construct-ugen "PanB2" (list 0) (list input azimuth gain) nil 3 nil)))

(define PartConv
  (lambda (input fftsize irbufnum)
    (construct-ugen "PartConv" audio-rate (list input fftsize irbufnum) nil 1 nil)))

(define Pause
  (lambda (gate_ id_)
    (construct-ugen "Pause" control-rate (list gate_ id_) nil 1 nil)))

(define PauseSelf
  (lambda (input)
    (construct-ugen "PauseSelf" control-rate (list input) nil 1 nil)))

(define PauseSelfWhenDone
  (lambda (src)
    (construct-ugen "PauseSelfWhenDone" control-rate (list src) nil 1 nil)))

(define Peak
  (lambda (input trig_)
    (construct-ugen "Peak" (list 0) (list input trig_) nil 1 nil)))

(define PeakFollower
  (lambda (input decay_)
    (construct-ugen "PeakFollower" (list 0) (list input decay_) nil 1 nil)))

(define Phasor
  (lambda (trig_ rate_ start end resetPos)
    (construct-ugen "Phasor" audio-rate (list trig_ rate_ start end resetPos) nil 1 nil)))

(define PinkNoise
  (lambda ()
    (construct-ugen "PinkNoise" audio-rate nil nil 1 nil)))

(define Pitch
  (lambda (input initFreq minFreq maxFreq execFreq maxBinsPerOctave median ampThreshold peakThreshold downSample clar)
    (construct-ugen "Pitch" control-rate (list input initFreq minFreq maxFreq execFreq maxBinsPerOctave median ampThreshold peakThreshold downSample clar) nil 2 nil)))

(define PitchShift
  (lambda (input windowSize pitchRatio pitchDispersion timeDispersion)
    (construct-ugen "PitchShift" (list 0) (list input windowSize pitchRatio pitchDispersion timeDispersion) nil 1 nil)))

(define PlayBuf
  (lambda (nc bufnum rate_ trigger startPos loop doneAction)
    (construct-ugen "PlayBuf" audio-rate (list bufnum rate_ trigger startPos loop doneAction) nil nc nil)))

(define Pluck
  (lambda (input trig_ maxdelaytime delaytime decaytime coef)
    (construct-ugen "Pluck" (list 0) (list input trig_ maxdelaytime delaytime decaytime coef) nil 1 nil)))

;(define Poll
;  (lambda (trig_ input trigid label_)
;    (construct-ugen "Poll" (list 1) (list trig_ input trigid label_) nil 1 nil)))

(define Pulse
  (lambda (freq width)
    (construct-ugen "Pulse" audio-rate (list freq width) nil 1 nil)))

(define PulseCount
  (lambda (trig_ reset)
    (construct-ugen "PulseCount" (list 0) (list trig_ reset) nil 1 nil)))

(define PulseDivider
  (lambda (trig_ div_ start)
    (construct-ugen "PulseDivider" (list 0) (list trig_ div_ start) nil 1 nil)))

(define QuadC
  (lambda (freq a b c xi)
    (construct-ugen "QuadC" audio-rate (list freq a b c xi) nil 1 nil)))

(define QuadL
  (lambda (freq a b c xi)
    (construct-ugen "QuadL" audio-rate (list freq a b c xi) nil 1 nil)))

(define QuadN
  (lambda (freq a b c xi)
    (construct-ugen "QuadN" audio-rate (list freq a b c xi) nil 1 nil)))

(define RHPF
  (lambda (input freq rq)
    (construct-ugen "RHPF" (list 0) (list input freq rq) nil 1 nil)))

(define RLPF
  (lambda (input freq rq)
    (construct-ugen "RLPF" (list 0) (list input freq rq) nil 1 nil)))

(define RadiansPerSample
  (lambda ()
    (construct-ugen "RadiansPerSample" scalar-rate nil nil 1 nil)))

(define Ramp
  (lambda (input lagTime)
    (construct-ugen "Ramp" (list 0) (list input lagTime) nil 1 nil)))

(define Rand
  (lambda (lo hi)
    (construct-ugen "Rand" scalar-rate (list lo hi) nil 1 nil)))

(define RandID
  (lambda (id_)
    (construct-ugen "RandID" control-rate (list id_) nil 0 nil)))

(define RandSeed
  (lambda (trig_ seed)
    (construct-ugen "RandSeed" audio-rate (list trig_ seed) nil 0 nil)))

(define RecordBuf
  (lambda (bufnum offset recLevel preLevel run loop trigger doneAction inputArray)
    (construct-ugen "RecordBuf" audio-rate (list bufnum offset recLevel preLevel run loop trigger doneAction) inputArray 1 nil)))

(define ReplaceOut
  (lambda (bus input)
    (construct-ugen "ReplaceOut" (list 1) (list bus) input 0 nil)))

(define Resonz
  (lambda (input freq bwr)
    (construct-ugen "Resonz" (list 0) (list input freq bwr) nil 1 nil)))

(define Ringz
  (lambda (input freq decaytime)
    (construct-ugen "Ringz" (list 0) (list input freq decaytime) nil 1 nil)))

(define Rotate2
  (lambda (x y pos)
    (construct-ugen "Rotate2" (list 0 1) (list x y pos) nil 2 nil)))

(define RunningMax
  (lambda (input trig_)
    (construct-ugen "RunningMax" (list 0) (list input trig_) nil 1 nil)))

(define RunningMin
  (lambda (input trig_)
    (construct-ugen "RunningMin" (list 0) (list input trig_) nil 1 nil)))

(define RunningSum
  (lambda (input numsamp)
    (construct-ugen "RunningSum" (list 0) (list input numsamp) nil 1 nil)))

(define SOS
  (lambda (input a0 a1 a2 b1 b2)
    (construct-ugen "SOS" (list 0) (list input a0 a1 a2 b1 b2) nil 1 nil)))

(define SampleDur
  (lambda ()
    (construct-ugen "SampleDur" scalar-rate nil nil 1 nil)))

(define SampleRate
  (lambda ()
    (construct-ugen "SampleRate" scalar-rate nil nil 1 nil)))

(define Sanitize
  (lambda (input replace)
    (construct-ugen "Sanitize" (list 0) (list input replace) nil 1 nil)))

(define Saw
  (lambda (freq)
    (construct-ugen "Saw" audio-rate (list freq) nil 1 nil)))

(define Schmidt
  (lambda (input lo hi)
    (construct-ugen "Schmidt" (list 0) (list input lo hi) nil 1 nil)))

(define Select
  (lambda (which array)
    (construct-ugen "Select" (list 0 1) (list which) array 1 nil)))

(define SendTrig
  (lambda (input id_ value)
    (construct-ugen "SendTrig" (list 0) (list input id_ value) nil 0 nil)))

(define SetResetFF
  (lambda (trig_ reset)
    (construct-ugen "SetResetFF" (list 0 1) (list trig_ reset) nil 1 nil)))

(define Shaper
  (lambda (bufnum input)
    (construct-ugen "Shaper" (list 1) (list bufnum input) nil 1 nil)))

(define SinOsc
  (lambda (freq phase)
    (construct-ugen "SinOsc" audio-rate (list freq phase) nil 1 nil)))

(define SinOscFB
  (lambda (freq feedback)
    (construct-ugen "SinOscFB" audio-rate (list freq feedback) nil 1 nil)))

(define Slew
  (lambda (input up dn)
    (construct-ugen "Slew" (list 0) (list input up dn) nil 1 nil)))

(define Slope
  (lambda (input)
    (construct-ugen "Slope" (list 0) (list input) nil 1 nil)))

(define SpecCentroid
  (lambda (buffer)
    (construct-ugen "SpecCentroid" control-rate (list buffer) nil 1 nil)))

(define SpecFlatness
  (lambda (buffer)
    (construct-ugen "SpecFlatness" control-rate (list buffer) nil 1 nil)))

(define SpecPcile
  (lambda (buffer fraction interpolate)
    (construct-ugen "SpecPcile" control-rate (list buffer fraction interpolate) nil 1 nil)))

(define Spring
  (lambda (input spring damp)
    (construct-ugen "Spring" audio-rate (list input spring damp) nil 1 nil)))

(define StandardL
  (lambda (freq k xi yi)
    (construct-ugen "StandardL" audio-rate (list freq k xi yi) nil 1 nil)))

(define StandardN
  (lambda (freq k xi yi)
    (construct-ugen "StandardN" audio-rate (list freq k xi yi) nil 1 nil)))

(define Stepper
  (lambda (trig_ reset min_ max_ step resetval)
    (construct-ugen "Stepper" (list 0) (list trig_ reset min_ max_ step resetval) nil 1 nil)))

(define StereoConvolution2L
  (lambda (input kernelL kernelR trigger framesize crossfade)
    (construct-ugen "StereoConvolution2L" audio-rate (list input kernelL kernelR trigger framesize crossfade) nil 2 nil)))

(define SubsampleOffset
  (lambda ()
    (construct-ugen "SubsampleOffset" scalar-rate nil nil 1 nil)))

(define Sum3
  (lambda (in0 in1 in2)
    (construct-ugen "Sum3" (list 0 1 2) (list in0 in1 in2) nil 1 nil)))

(define Sum4
  (lambda (in0 in1 in2 in3)
    (construct-ugen "Sum4" (list 0 1 2 3) (list in0 in1 in2 in3) nil 1 nil)))

(define Sweep
  (lambda (trig_ rate_)
    (construct-ugen "Sweep" (list 0) (list trig_ rate_) nil 1 nil)))

(define SyncSaw
  (lambda (syncFreq sawFreq)
    (construct-ugen "SyncSaw" audio-rate (list syncFreq sawFreq) nil 1 nil)))

(define T2A
  (lambda (input offset)
    (construct-ugen "T2A" audio-rate (list input offset) nil 1 nil)))

(define T2K
  (lambda (input)
    (construct-ugen "T2K" control-rate (list input) nil 1 nil)))

(define TBall
  (lambda (input g damp friction)
    (construct-ugen "TBall" audio-rate (list input g damp friction) nil 1 nil)))

(define TDelay
  (lambda (input dur)
    (construct-ugen "TDelay" (list 0) (list input dur) nil 1 nil)))

(define TDuty
  (lambda (dur reset doneAction level gapFirst)
    (construct-ugen "TDuty" audio-rate (list dur reset doneAction level gapFirst) nil 1 nil)))

(define TExpRand
  (lambda (lo hi trig_)
    (construct-ugen "TExpRand" (list 2) (list lo hi trig_) nil 1 nil)))

(define TGrains
  (lambda (nc trigger bufnum rate_ centerPos dur pan amp interp)
    (construct-ugen "TGrains" audio-rate (list trigger bufnum rate_ centerPos dur pan amp interp) nil nc nil)))

(define TIRand
  (lambda (lo hi trig_)
    (construct-ugen "TIRand" (list 2) (list lo hi trig_) nil 1 nil)))

(define TRand
  (lambda (lo hi trig_)
    (construct-ugen "TRand" (list 2) (list lo hi trig_) nil 1 nil)))

(define TWindex
  (lambda (input normalize array)
    (construct-ugen "TWindex" (list 0) (list input normalize) array 1 nil)))

(define Timer
  (lambda (trig_)
    (construct-ugen "Timer" (list 0) (list trig_) nil 1 nil)))

(define ToggleFF
  (lambda (trig_)
    (construct-ugen "ToggleFF" (list 0) (list trig_) nil 1 nil)))

(define Trig
  (lambda (input dur)
    (construct-ugen "Trig" (list 0) (list input dur) nil 1 nil)))

(define Trig1
  (lambda (input dur)
    (construct-ugen "Trig1" (list 0) (list input dur) nil 1 nil)))

(define TwoPole
  (lambda (input freq radius)
    (construct-ugen "TwoPole" (list 0) (list input freq radius) nil 1 nil)))

(define TwoZero
  (lambda (input freq radius)
    (construct-ugen "TwoZero" (list 0) (list input freq radius) nil 1 nil)))

(define VDiskIn
  (lambda (nc bufnum rate_ loop sendID)
    (construct-ugen "VDiskIn" audio-rate (list bufnum rate_ loop sendID) nil nc nil)))

(define VOsc
  (lambda (bufpos freq phase)
    (construct-ugen "VOsc" audio-rate (list bufpos freq phase) nil 1 nil)))

(define VOsc3
  (lambda (bufpos freq1 freq2 freq3)
    (construct-ugen "VOsc3" audio-rate (list bufpos freq1 freq2 freq3) nil 1 nil)))

(define VarLag
  (lambda (input time curvature warp start)
    (construct-ugen "VarLag" (list 0) (list input time curvature warp start) nil 1 nil)))

(define VarSaw
  (lambda (freq iphase width)
    (construct-ugen "VarSaw" audio-rate (list freq iphase width) nil 1 nil)))

(define Vibrato
  (lambda (freq rate_ depth delay onset rateVariation depthVariation iphase trig_)
    (construct-ugen "Vibrato" audio-rate (list freq rate_ depth delay onset rateVariation depthVariation iphase trig_) nil 1 nil)))

(define Warp1
  (lambda (nc bufnum pointer freqScale windowSize envbufnum overlaps windowRandRatio interp)
    (construct-ugen "Warp1" audio-rate (list bufnum pointer freqScale windowSize envbufnum overlaps windowRandRatio interp) nil nc nil)))

(define WhiteNoise
  (lambda ()
    (construct-ugen "WhiteNoise" audio-rate nil nil 1 nil)))

(define Wrap
  (lambda (input lo hi)
    (construct-ugen "Wrap" (list 0) (list input lo hi) nil 1 nil)))

(define WrapIndex
  (lambda (bufnum input)
    (construct-ugen "WrapIndex" (list 1) (list bufnum input) nil 1 nil)))

(define XFade2
  (lambda (inA inB pan level)
    (construct-ugen "XFade2" (list 0 1) (list inA inB pan level) nil 1 nil)))

(define XLine
  (lambda (start end dur doneAction)
    (construct-ugen "XLine" audio-rate (list start end dur doneAction) nil 1 nil)))

(define XOut
  (lambda (bus xfade input)
    (construct-ugen "XOut" (list 2) (list bus xfade) input 0 nil)))

(define ZeroCrossing
  (lambda (input)
    (construct-ugen "ZeroCrossing" (list 0) (list input) nil 1 nil)))

(define MaxLocalBufs
  (lambda (count)
    (construct-ugen "MaxLocalBufs" control-rate (list count) nil 1 nil)))

(define MulAdd
  (lambda (input mul add)
    (construct-ugen "MulAdd" (list 0 1 2) (list input mul add) nil 1 nil)))

(define SetBuf
  (lambda (buf offset length_ array)
    (construct-ugen "SetBuf" scalar-rate (list buf offset length_) array 1 nil)))

(define Neg
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 0)))

(define Not
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 1)))

(define IsNil
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 2)))

(define NotNil
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 3)))

(define BitNot
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 4)))

(define Abs
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 5)))

(define AsFloat
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 6)))

(define AsInt
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 7)))

(define Ceil
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 8)))

(define Floor
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 9)))

(define Frac
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 10)))

(define Sign
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 11)))

(define Squared
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 12)))

(define Cubed
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 13)))

(define Sqrt
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 14)))

(define Exp
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 15)))

(define Recip
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 16)))

(define MidiCps
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 17)))

(define CpsMidi
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 18)))

(define MidiRatio
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 19)))

(define RatioMidi
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 20)))

(define DbAmp
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 21)))

(define AmpDb
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 22)))

(define OctCps
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 23)))

(define CpsOct
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 24)))

(define Log
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 25)))

(define Log2
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 26)))

(define Log10
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 27)))

(define Sin
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 28)))

(define Cos
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 29)))

(define Tan
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 30)))

(define ArcSin
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 31)))

(define ArcCos
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 32)))

(define ArcTan
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 33)))

(define Sinh
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 34)))

(define Cosh
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 35)))

(define Tanh
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 36)))

(define Rand_
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 37)))

(define Rand2
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 38)))

(define LinRand_
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 39)))

(define BiLinRand
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 40)))

(define Sum3Rand
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 41)))

(define Distort
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 42)))

(define SoftClip
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 43)))

(define Coin
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 44)))

(define DigitValue
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 45)))

(define Silence
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 46)))

(define Thru
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 47)))

(define RectWindow
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 48)))

(define HanWindow
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 49)))

(define WelchWindow
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 50)))

(define TriWindow
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 51)))

(define Ramp_
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 52)))

(define Scurve
  (lambda (a)
    (construct-ugen "UnaryOpUGen" (list 0) (list a) nil 1 53)))

(define Add
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 0)))

(define Sub
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 1)))

(define Mul
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 2)))

(define Idiv
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 3)))

(define Fdiv
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 4)))

(define Mod
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 5)))

(define Eq
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 6)))

(define Ne
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 7)))

(define Lt
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 8)))

(define Gt
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 9)))

(define Le
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 10)))

(define Ge
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 11)))

(define Min
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 12)))

(define Max
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 13)))

(define BitAnd
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 14)))

(define BitOr
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 15)))

(define BitXor
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 16)))

(define Lcm
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 17)))

(define Gcd
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 18)))

(define RoundTo
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 19)))

(define RoundUp
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 20)))

(define Trunc
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 21)))

(define Atan2
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 22)))

(define Hypot
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 23)))

(define Hypotx
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 24)))

(define Pow
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 25)))

(define ShiftLeft
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 26)))

(define ShiftRight
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 27)))

(define UnsignedShift
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 28)))

(define Fill
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 29)))

(define Ring1
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 30)))

(define Ring2
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 31)))

(define Ring3
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 32)))

(define Ring4
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 33)))

(define DifSqr
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 34)))

(define SumSqr
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 35)))

(define SqrSum
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 36)))

(define SqrDif
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 37)))

(define AbsDif
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 38)))

(define Thresh
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 39)))

(define AmClip
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 40)))

(define ScaleNeg
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 41)))

(define Clip2
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 42)))

(define Excess
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 43)))

(define Fold2
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 44)))

(define Wrap2
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 45)))

(define FirstArg
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 46)))

(define RandRange
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 47)))

(define ExpRandRange
  (lambda (a b)
    (construct-ugen "BinaryOpUGen" (list 0 1) (list a b) nil 1 48)))

