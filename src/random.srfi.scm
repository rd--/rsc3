; (num, num) -> float
(define random-float-uniform
  (lambda (a b)
    (+ (* (srfi:random-real) (- b a)) a)))

; (int, int) -> int
(define random-int-uniform
  (lambda (l r)
    (+ l (srfi:random-integer (- r l)))))
