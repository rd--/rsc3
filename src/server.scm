; (port, message) -> ()
(define async
  (lambda (fd m)
    (sendMessage fd m)
    (waitMessage fd "/done")))

; port -> ()
(define reset
  (lambda (fd)
    (sendMessages
     fd
     (list
      (g_freeAll1 0)
      clearSched
      (g_new1 1 0 0)))))

; (socket -> a) -> a
(define with-udp-sc3
  (lambda (f)
    (let* ((fd (udpOpen "127.0.0.1" 57110))
           (r (f fd)))
      (udpClose fd)
      r)))

(define with-tcp-sc3
  (lambda (f)
    (let* ((fd (tcpOpen "127.0.0.1" 57110))
           (r (f fd)))
      (tcpClose fd)
      r)))

(define with-sc3 with-udp-sc3)
;(define with-sc3 with-tcp-sc3)

(define with-sc3-list
  (lambda (l)
    (with-sc3
     (lambda (fd)
       (map (lambda (f) (f fd)) l)))))

; [string]
(define statusFields
  (list "# Ugens                     "
        "# Synths                    "
        "# Groups                    "
        "# Instruments               "
        "% Cpu (Average)             "
        "% Cpu (Peak)                "
        "Sample Rate (Nominal)       "
        "Sample Rate (Actual)        "))

; osc -> [string]
(define statusFormat
  (lambda (r)
    (cons "***** SuperCollider Server Status *****"
          (zipWith string-append
                   statusFields
                   (map number->string (tail (tail r)))))))

; port -> [string]
(define serverStatus
  (lambda (fd)
    (sendMessage fd status)
    (let ((r (waitMessage fd "/status.reply")))
      (statusFormat r))))

; port -> ()
(define displayServerStatus
  (lambda (fd)
    (newline)
    (for-each display (intersperse "\n" (serverStatus fd)))
    (newline)))

; port -> int -> number
(define serverStatusField
  (lambda (fd n)
    (sendMessage fd status)
    (let ((r (waitMessage fd "/status.reply")))
      (list-ref r n))))

; port -> float
(define serverSampleRateNominal
  (lambda (s)
    (serverStatusField s 8)))

; port -> float
(define serverSampleRateActual
  (lambda (s)
    (serverStatusField s 9)))

; port -> string -> ugen -> ()
(define sendSynth
  (lambda (fd n u)
    (async fd (d_recv (encodeGraphdef (synthdef n u))))))

(define playAt
  (lambda (fd u nid act grp)
    (sendSynth fd "anonymous" u)
    (sendMessage fd (s_new0 "anonymous" nid act grp))))

; port -> ugen -> ()
(define play
  (lambda (fd u)
    (playAt fd u -1 addToTail 1)))

; ((socket -> a) -> a) -> (ugen -> ())
(define auditionUsing
  (lambda (f)
    (lambda (u)
      (f
       (lambda (fd)
         (play fd u))))))

; ugen -> ()
(define audition (auditionUsing with-udp-sc3))
;(define audition (auditionUsing with-tcp-sc3))

; camelCase
(define withSc3 with-sc3)
(define withSc3List with-sc3-list)
