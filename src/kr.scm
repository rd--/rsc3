(define-record-type end-kr
  (fields source))

(define ar:
  (lambda (u)
    (make-end-kr u)))

(define kr:
  (lambda (u)
    (cond
     ((number? u) u)
     ((control? u) u)
     ((ugen? u)
      (if (equal? (ugen-rate u) audio-rate)
          (begin
            (ugen-rate-set! u control-rate)
            (map kr: (ugen-inputs u))
            u)
          u))
     ((proxy? u) (begin (kr: (proxy-ugen u)) u))
     ((mce? u) (begin (map kr: (mce-proxies u)) u))
     ((mrg? u) (begin (kr: (mrg-left u)) u)) ; hmmm
     ((end-kr? u) (end-kr-source u))
     (else (error "kr" "illegal value" u)))))

(define cr kr:)
