; [string]
(define unary-operator-names
  (list
   "Neg"
   "Not"
   "IsNil"
   "NotNil"
   "BitNot"
   "Abs"
   "AsFloat"
   "AsInt"
   "Ceil"
   "Floor"
   "Frac"
   "Sign"
   "Squared"
   "Cubed"
   "Sqrt"
   "Exp"
   "Recip"
   "MidiCps"
   "CpsMidi"
   "MidiRatio"
   "RatioMidi"
   "DbAmp"
   "AmpDb"
   "OctCps"
   "CpsOct"
   "Log"
   "Log2"
   "Log10"
   "Sin"
   "Cos"
   "Tan"
   "ArcSin"
   "ArcCos"
   "ArcTan"
   "SinH"
   "CosH"
   "TanH"
   "Rand_"
   "Rand2"
   "LinRand_"
   "BiLinRand"
   "Sum3Rand"
   "Distort"
   "SoftClip"
   "Coin"
   "DigitValue"
   "Silence"
   "Thru"
   "RectWindow"
   "HanWindow"
   "WelchWindow"
   "TriWindow"
   "Ramp_"
   "SCurve"))

(define unary-operator-symbols
  (list
   (cons "Neg" "-")))

; int -> string
(define unary-operator-name
  (lambda (i)
    (list-ref unary-operator-names i)))

; [string]
(define binary-operator-names
  (list
   "Add"
   "Sub"
   "Mul"
   "Idiv"
   "Fdiv"
   "Mod"
   "Eq"
   "Ne"
   "Lt"
   "Gt"
   "Le"
   "Ge"
   "Min"
   "Max"
   "BitAnd"
   "BitOr"
   "BitXor"
   "Lcm"
   "Gcd"
   "RoundTo"
   "RoundUp"
   "Trunc"
   "ATan2"
   "Hypot"
   "HypotX"
   "Pow"
   "ShiftLeft"
   "ShiftRight"
   "UnsignedShift"
   "Fill"
   "Ring1"
   "Ring2"
   "Ring3"
   "Ring4"
   "DifSqr"
   "SumSqr"
   "SqrSum"
   "SqrDif"
   "AbsDif"
   "Thresh"
   "AmClip"
   "ScaleNeg"
   "Clip2"
   "Excess"
   "Fold2"
   "Wrap2"
   "FirstArg"
   "RandRange"
   "ExpRandRange"))

(define binary-operator-symbols
  (list
   (cons "Add" "+")
   (cons "Sub" "-")
   (cons "Mul" "*")
   (cons "Fdiv" "/")
   (cons "Mod" "%")
   (cons "Eq" "==")
   (cons "Ne" "!=")
   (cons "Lt" "<")
   (cons "Gt" ">")
   (cons "Le" "<=")
   (cons "Ge" ">=")))

; int -> string
(define binary-operator-name
  (lambda (i)
    (list-ref binary-operator-names i)))

; ugen -> maybe string
;
; (operator-name (Mul (SinOsc 440 0) 0.1))
(define operator-name
  (lambda (u)
    (cond ((string=? (ugen-name u) "BinaryOpUGen")
           (binary-operator-name (ugen-special u)))
          ((string=? (ugen-name u) "UnaryOpUGen")
           (unary-operator-name (ugen-special u)))
          (else
           #f))))
