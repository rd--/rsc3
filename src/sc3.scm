; (num, num) -> num
(define abs-dif
  (lambda (p q)
    (abs (- p q))))

; float -> float
(define amp-db
  (lambda (x)
    (* (log10 x) 20)))

; [t] -> t
(define choose
  (lambda (c)
    (if
     (list? c)
     (list-choose c)
     (if (vector? c)
         (vector-choose c)
         (error "choose" "not a vector?")))))

; (num, num, num) -> num
(define clip
  (lambda (a b n)
    (cond
     ((< n a) a)
     ((> n b) b)
     (else n))))

; (num, num) -> num
(define clip2
  (lambda (a b)
    (clip a (- b) b)))

; float -> boolean
(define coin
  (lambda (n)
    (if (> (rand 1.0) n) #f #t)))

; float -> float
(define cps-midi
  (lambda (freq)
    (+
     (* (log2 (* freq (/ 1.0 440.0))) 12.0)
     69.0)))

; float -> float
(define cps-oct
  (lambda (freq)
    (+
     (log2 (* freq (/ 1.0 440.0)))
     4.75)))

; num -> num
(define cubed
  (lambda (n)
    (* n n n)))

; float -> float
(define db-amp
  (lambda (x)
    (expt 10 (* x 0.05))))

; float -> float
(define db-pow
  (lambda (x)
    (expt 10 (* x 0.1))))

; (float, [float], int) -> float
(define degree-to-key
  (lambda (degree scale steps)
    (let ((scale-n (length scale)))
      (+
       (* steps (div degree scale-n))
       (list-ref scale (exact (mod degree scale-n)))))))

; (float, float) -> float
(define exp-rand
  (lambda (a b)
    (let ((r (/ b a)))
      (* (expt r (rrand 0.0 1.0)) a))))

; (num, num) -> int
(define greater-than
  (lambda (p q)
    (if (> p q) 1 0)))

; (num, num) -> int
(define greater-than-or-equal-to
  (lambda (p q)
    (if (>= p q) 1 0)))

; [t] -> t
(define list-choose
  (lambda (l)
    (list-ref l (rrand 0 (length l)))))

; (num, num) -> num
(define less-than-or-equal-to
  (lambda (p q)
    (if (<= p q) 1 0)))

; float -> float
(define log2
  (lambda (x)
    (/ (log (abs x)) (log 2))))

; float -> float
(define log10
  (lambda (x)
    (/ (log x) (log 10))))

; (num, num) -> int
(define less-than
  (lambda (p q)
    (if (< p q) 1 0)))

; float -> float
(define midi-cps
  (lambda (note)
    (* 440.0 (expt 2.0 (* (- note 69.0) (/ 1.0 12.0))))))

; float -> float
(define midi-ratio
  (lambda (midi)
    (expt 2.0 (* midi (/ 1.0 12.0)))))

; float -> float
(define oct-cps
  (lambda (note)
    (* 440.0 (expt 2.0 (- note 4.75)))))

; float -> float
(define pow-db
  (lambda (x)
    (* (log10 x) 10)))

; num -> num
(define rand
  (lambda (n)
    (rrand (if (exact? n) 0 0.0) n)))

; num -> float
(define randf
  (lambda (p)
    (random-float-uniform 0 p)))

; num -> num
(define rand2
  (lambda (x)
    (rrand (- x) x)))

; num -> float
(define rand2f
  (lambda (p)
    (random-float-uniform (- p) p)))

; float -> float
(define ratio-midi
  (lambda (ratio)
    (* 12.0 (log2 ratio))))

; num -> num
(define recip
  (lambda (n)
    (/ 1 n)))

; (float, float) -> float
(define round-to
  (lambda (p q)
    (* (round (/ p q)) q)))

; (num, num) -> num
(define rrand
  (lambda (p q)
    (if
     (exact? p)
     (random-int-uniform p q)
     (random-float-uniform p q))))

; (num, num) -> float
(define rrandf
  (lambda (p q)
    (random-float-uniform p q)))

; num -> num
(define squared
  (lambda (n)
    (* n n)))

; #[t] -> t
(define vector-choose
  (lambda (l)
    (vector-ref l (rrand 0 (vector-length l)))))

; camelCase
(define absDif abs-dif)
(define ampDb amp-db)
(define cpsMidi cps-midi)
(define cpsOct cps-oct)
(define dbAmp db-amp)
(define dbPow db-pow)
(define degreeToKey degree-to-key)
(define expRand exp-rand)
(define greaterThan greater-than)
(define greaterThanOrEqualTo greater-than-or-equal-to)
(define lessThan less-than)
(define lessThanOrEqualTo less-than-or-equal-to)
(define midiCps midi-cps)
(define midiRatio midi-ratio)
(define octCps oct-cps)
(define powDb pow-db)
(define ratioMidi ratio-midi)
(define roundTo round-to)

; abbrev
(define ge greater-than-or-equal-to)
(define gt greater-than)
(define le less-than-or-equal-to)
(define lt less-than)
