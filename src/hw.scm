; Zero local buffer.
;
; ClearBuf does not copy the buffer number through so this is an MRG node.
;
; ClearBuf :: ugen -> ugen
(define ClearBuf
  (lambda (b)
    (Mrg2 b (construct-ugen "ClearBuf" scalar-rate (list b) nil 1 nil))))

(define PackFFT
  (lambda (b sz fr to z mp)
    (construct-ugen "PackFFT" control-rate (list b sz fr to z) mp 1 nil)))

(define string->ugen
  (lambda (s)
    (cons (string-length s)
          (map char->integer (string->list s)))))

(define Poll
  (lambda (trig input trigid label_)
    (construct-ugen "Poll" (list 1) (list trig input trigid) (string->ugen label_) 1 nil)))

(define PV_HainsworthFoote
  (lambda (buf h f thr wt)
    (construct-ugen "PV_HainsworthFoote" audio-rate (list buf h f thr wt) nil 1 nil)))

(define PV_JensenAndersen
  (lambda (buffer propsc prophfe prophfc propsf threshold waittime)
    (construct-ugen "PV_JensenAndersen" audio-rate (list buffer propsc prophfe prophfc propsf threshold waittime) nil 1 nil)))

(define Unpack1FFT
  (lambda (c b bi wm)
    (construct-ugen "Unpack1FFT" demand-rate (list c b bi wm) nil 1 nil)))
