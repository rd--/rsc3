;; Control

; (rate, string, float) -> control
(define-record-type control
  (fields operating-rate name default index))

; (string, float) -> control
(define ctl
  (lambda (nm df)
    (make-control control-rate nm df nil)))

;; Graphdef

; (string, [float], [float], [control], [ugen]) -> graphdef
(define-record-type graphdef
  (fields name constants defaults controls ugens))

; (graphdef, int) -> ugen
(define graphdef-ugen
  (lambda (g n)
    (list-ref (graphdef-ugens g) n)))

; graphdef -> int -> control
(define graphdef-control
  (lambda (g n)
    (list-ref (graphdef-controls g) n)))

; graphdef -> int -> float
(define graphdef-constant
  (lambda (g n)
    (list-ref (graphdef-constants g) n)))

;; Ugen

; int -> int -> input
(define-record-type input
  (fields ugen port))

; seq t = list t | vector t
(define seq?
  (lambda (seq)
    (or (list? seq)
	(vector? seq))))

; seq t -> int
; (equal? (seq-length (list 1 2 3 4 5)) 5)
; (equal? (seq-length (vector 1 2 3 4 5)) 5)
; (seq-length "12345")
(define seq-length
  (lambda (seq)
    (if (list? seq)
	(length seq)
	(if (vector? seq)
	    (vector-length seq)
	    (error "seq-length" "not a seq?" seq)))))

; seq t -> [t]
; (equal? (seq-list (list 1 2 3 4 5)) (list 1 2 3 4 5))
; (equal? (seq-list (vector 1 2 3 4 5)) (list 1 2 3 4 5))
; (seq-list "12345")
(define seq-list
  (lambda (seq)
    (if (list? seq)
        seq
        (if (vector? seq)
            (vector->list seq)
            (error "seq-list" "not a seq?" seq)))))

; seq t -> int -> t ; zero-indexed sequence element lookup
; (equal? (seq-ref (list 1 2 3 4 5) 2) 3)
; (equal? (seq-ref (vector 1 2 3 4 5) 2) 3)
; (seq-ref "12345" 2)
(define seq-ref
  (lambda (seq index)
    (if
     (list? seq)
     (list-ref seq index)
     (if (vector? seq)
         (vector-ref seq index)
         (error "seq-ref" "not a seq?" seq)))))

(define mce? seq?)

; mce -> [ugen]
(define mce-proxies seq-list)

; ugen -> ugen -> mrg
(define-record-type mrg
  (fields left right))

; rate -> output
(define-record-type output
  (fields rate))

; ugen -> int -> proxy
(define-record-type proxy
  (fields ugen port))

; int -> rate
(define-record-type rate
  (fields from-enum))

; rate
(define scalar-rate (make-rate 0))
(define control-rate (make-rate 1))
(define audio-rate (make-rate 2))
(define demand-rate (make-rate 3))

(define ir scalar-rate)
(define kr control-rate)
(define ar audio-rate)
(define dr demand-rate)

; rate -> symbol
(define rate->abbrev
  (lambda (r)
    (case (rate-from-enum r)
      ((0) 'ir)
      ((1) 'kr)
      ((2) 'ar)
      ((3) 'dr)
      (else (error "rate->abbrev" "unknown rate")))))

; any -> rate
(define rateOf
  (lambda (o)
    (cond ((number? o) scalar-rate)
          ((control? o) control-rate)
          ((ugen? o) (ugen-rate o))
          ((proxy? o) (rateOf (proxy-ugen o)))
          ((mce? o) (rate-select (map rateOf (mce-proxies o))))
          ((mrg? o) (rateOf (mrg-left o)))
          (else (error "rateOf" "illegal value" o)))))

; rate -> rate -> rate
(define rate-max
  (lambda (a b)
    (if (> (rate-from-enum a) (rate-from-enum b)) a b)))

; [rate] -> rate
(define rate-select
  (lambda (l)
    (foldl1 rate-max l)))

; string -> rate -> [ugen] -> [output] -> int -> ugen
(define-record-type ugen
  (fields name (mutable rate) inputs outputs special))

; ugen -> int -> output
(define ugen-output
  (lambda (u n)
    (list-ref (ugen-outputs u) n)))

; ugen -> (name -> rate -> inputs -> outputs -> special -> any) -> any
(define ugen-transform
  (lambda (u f)
    (f
     (ugen-name u)
     (ugen-rate u)
     (ugen-inputs u)
     (ugen-outputs u)
     (ugen-special u))))

; any -> bool
(define allowed-input?
  (lambda (i)
    (or (number? i)
        (control? i)
        (ugen? i)
        (proxy? i)
        (mce? i)
        (mrg? i))))

; ugen -> bool
(define ugen-valid?
  (lambda (u)
    (ugen-transform
     u
     (lambda (n r i o s)
       (and (string? n)
            (rate? r)
            (and (list? i) (all allowed-input? i))
            (and (list? o) (all output? o))
            (integer? s))))))

; int -> (() -> ugen) -> [ugen]
(define clone replicateM)

; (() -> t) | t -> int -> [t]
(define dup
  (lambda (f n)
    (if (procedure? f)
	(replicateM n f)
	(replicate n f))))

; [... -> t, ...] -> t
(define value
  (lambda (f . a)
    (if (procedure? f)
	(apply f a)
	(if (null? a)
	    f
	    (error "value?")))))

; control -> [bytevector]
(define encode-control
  (lambda (c)
    (list (encode-pascal-string (control-name c))
          (encode-i16 (control-index c)))))

; input -> [bytevector]
(define encode-input
  (lambda (i)
    (list (encode-i16 (input-ugen i))
          (encode-i16 (input-port i)))))

; output -> [bytevector]
(define encode-output
  (lambda (o)
    (encode-u8 (rate-from-enum (output-rate o)))))

; [bytevector]
(define scgf
  (map encode-u8 (map char->integer (string->list "SCgf"))))

; ugen -> [bytevector]
(define encode-ugen
  (lambda (u)
    (ugen-transform
     u
     (lambda (n r i o s)
       (list
        (encode-pascal-string n)
        (encode-u8 (rate-from-enum r))
        (encode-i16 (length i))
        (encode-i16 (length o))
        (encode-i16 s)
        (map encode-input i)
        (map encode-output o))))))

; graphdef -> bytevector
(define encodeGraphdef
  (lambda (g)
    (flatten-bytevectors
     (let ((n (graphdef-name g))
           (c (graphdef-constants g))
           (d (graphdef-defaults g))
           (k (graphdef-controls g))
           (u (graphdef-ugens g)))
       (list
        scgf
        (encode-i32 0)
        (encode-i16 1)
        (encode-pascal-string n)
        (encode-i16 (length c))
        (map encode-f32 c)
        (encode-i16 (length d))
        (map encode-f32 d)
        (encode-i16 (length k))
        (map encode-control k)
        (encode-i16 (length u))
        (map encode-ugen u))))))

; node = ugen | proxy | control | float

; node -> [proxy]
(define proxify
  (lambda (u)
    (cond
     ((mce? u) (map proxify (mce-proxies u)))
     ((mrg? u) (make-mrg (proxify (mrg-left u)) (mrg-right u)))
     ((ugen? u) (let* ((o (ugen-outputs u))
                       (n (length o)))
                  (if (< n 2)
                      u
                      (map (lambda (i) (make-proxy u i))
                           (enumFromTo 0 (- n 1))))))
     (else (error "proxify" "illegal ugen" u)))))

(define constant-unary-math
  (lambda (i p1)
    (case i
      ((0) (- p1)) ; Neg
      ((5) (abs p1)) ; Abd
      ((8) (ceiling p1)) ; Ceil
      ((9) (floor p1)) ; Floor
      ((12) (* p1 p1)) ; Squared
      ((13) (* p1 p1 p1)) ; Cubed
      ((14) (sqrt p1)) ; Sqrt
      ((15) (exp p1)) ; Exp
      ((16) (/ 1 p1)) ; Recip
      ((17) (midi-cps p1)) ; MidiCps
      ((18) (cps-midi p1)) ; CpsMidi
      ((19) (midi-ratio p1)) ; MidiRatio
      ((20) (ratio-midi p1)) ; RatioMidi
      ((21) (db-amp p1)) ; DbAmp
      ((22) (amp-db)) ; AmpDb
      ((25) (log p1))
      ((28) (sin p1))
      ((29) (cos p1))
      ((30) (tan p1))
      (else #f))))

(define constant-binary-math
  (lambda (i p1 p2)
    (case i
      ((0) (+ p1 p2)) ; Add
      ((1) (- p1 p2)) ; Sub
      ((2) (* p1 p2)) ; Mul
      ((4) (/ p1 p2)) ; Fdiv
      ((6) (if (= p1 p2) 1 0)) ; Eq
      ((7) (if (= p1 p2) 0 1)) ; Ne
      ((8) (if (< p1 p2) 1 0)) ; Lt
      ((9) (if (> p1 p2) 1 0)) ; Gt
      ((10) (if (<= p1 p2) 1 0)) ; Le
      ((11) (if (>= p1 p2) 1 0)) ; Ge
      ((12) (min p1 p2)) ; Min
      ((13) (max p1 p2)) ; Max
      ((19) (round-to p1 p2)) ; RoundTo
      ((25) (expt p1 p2)) ; Pow
      ((38) (abs-dif p1 p2)) ; AbsDif
      (else #f))))

(define constant-ternary-math
  (lambda (name p1 p2 p3)
    (cond
     ((equal? name "MulAdd") (+ (* p1 p2) p3))
     (else #f))))

(define constant-math
  (lambda (name special inputs)
    (case (length inputs)
      ((1) (constant-unary-math special (list-ref inputs 0)))
      ((2) (constant-binary-math special (list-ref inputs 0) (list-ref inputs 1)))
      ((3) (constant-ternary-math name (list-ref inputs 0) (list-ref inputs 1) (list-ref inputs 2)))
      (else #f))))

(define input-preprocess
  (lambda (x)
    (if (mce? x)
        (map input-preprocess (mce-proxies x))
        x)))

; string -> rate|list -> [node] -> node|nil -> int -> int -> ugen
(define construct-ugen
  (lambda (name rate inputs mce outputs special)
    (or (and (all number? inputs) (constant-math name special inputs))
        (let* ((inputs* (map
                         input-preprocess
                         (if (null? mce)
                             inputs
                             (append inputs (mceChannels (input-preprocess mce))))))
               (rate* (if (rate? rate)
                          rate
                          (rate-select (map (lambda (ix) (rateOf (list-ref inputs* ix))) rate))))
               (special* (if (null? special) 0 special))
               (u (make-ugen
                   name
                   rate*
                   inputs*
                   (replicate outputs (make-output rate*))
                   special*)))
          (proxify (mce-expand u))))))

; ugen -> [node]
(define graph-nodes
  (lambda (u)
    (cond
     ((ugen? u) (cons u (concatMap graph-nodes (ugen-inputs u))))
     ((proxy? u) (cons u (graph-nodes (proxy-ugen u))))
     ((control? u) (list u))
     ((number? u) (list u))
     ((mce? u) (concat (map graph-nodes (mce-proxies u))))
     ((mrg? u) (append (graph-nodes (mrg-left u)) (graph-nodes (mrg-right u))))
     (else (error "graph-nodes" "illegal value" u)))))

; ugen -> [float]
(define graph-constants
  (lambda (u)
    (nub (filter number? (graph-nodes u)))))

; ugen -> [control]
(define graph-controls
  (lambda (u)
    (nub (filter control? (graph-nodes u)))))

; ugen -> [ugen]
(define graph-ugens
  (lambda (u)
    (nub (reverse (filter ugen? (graph-nodes u))))))

; ugen -> [node] -> [control] -> [ugen] -> ugen
(define ugen-close
  (lambda (u nn cc uu)
    (if (not (ugen-valid? u))
        (error "ugen-close" "invalid ugen" u)
        (make-ugen (ugen-name u)
                   (ugen-rate u)
                   (map (lambda (i)
                           (ugen-parameter-to-input i nn cc uu))
                         (ugen-inputs u))
                   (ugen-outputs u)
                   (ugen-special u)))))

; [ugen] -> mrg
(define Mrg
  (lambda (xs)
    (if (null? xs)
        (error "Mrg" "nil input list" xs)
        (if (null? (tail xs))
            (head xs)
            (make-mrg (head xs) (Mrg (tail xs)))))))

; ugen -> ugen
(define prepare-root
  (lambda (u)
    (cond
     ((mce? u) (Mrg (mce-proxies u)))
     ((mrg? u) (make-mrg (prepare-root (mrg-left u)) (prepare-root (mrg-right u))))
     (else u))))

;; Synthdef

; [ugen] -> int
(define count-local-buf
  (lambda (uu)
    (length (filter (lambda (u) (equal? (ugen-name u) "LocalBuf")) uu))))

; ugen -> ugen (required by implicit-max-local-bufs-ugen)
(define max-local-bufs
  (lambda (count)
    (construct-ugen "MaxLocalBufs" scalar-rate (list count) nil 1 nil)))

; [ugen] -> (int,ugen|#f)
(define implicit-max-local-bufs-ugen
  (lambda (uu)
    (let ((n (count-local-buf uu)))
      (cons n (if (> n 0) (max-local-bufs n) #f)))))

; string -> ugen -> graphdef
(define synthdef
  (lambda (name pre-u)
    (let* ((u (prepare-root pre-u))
           (nn (graph-constants u))
           (cc (graph-controls u))
           (uu (graph-ugens u))
           (cx (implicit-control-ugen cc)) ; control must be zero-th indexed ugen if present
           (mx (implicit-max-local-bufs-ugen uu))
           (cons-if-true (lambda (p q) (if p (cons p q) q)))
           (nn* (if (> (car mx) 0) (cons (car mx) nn) nn))
           (uu* (cons-if-true cx (cons-if-true (cdr mx) uu))))
      (make-graphdef
       name
       nn*
       (map control-default cc)
       (map (lambda (c) (control-with-index c cc)) cc)
       (map (lambda (u) (ugen-close u nn* cc uu*)) uu*)))))

; synthdef -> filepath -> ()
(define synthdefWrite
  (lambda (sy fn)
    (let ((fd (open-file-output-port fn (file-options no-fail))))
      (put-bytevector fd (encodeGraphdef sy))
      (close-port fd))))

; [control] -> ugen|#f ; rsc3 only allows control-rate (kr) controls (not scalar or trigger rate)
(define implicit-control-ugen
  (lambda (cc)
    (if (null? cc)
        #f
        (make-ugen
         "Control"
         control-rate
         nil
         (map make-output (replicate (length cc) control-rate))
         0))))

; node -> [node] -> int
(define calculate-index
  (lambda (n nn)
    (let ((i (findIndex (lambda (e) (equal? e n)) nn))) ; is this very slow? optimise?
      (if (not i)
          (error "calculate-index" "not located" n nn)
          i))))

; float -> [node] -> input
(define number-to-input
  (lambda (n nn)
    (make-input -1 (calculate-index n nn))))

; control -> [control] -> control
(define control-with-index
  (lambda (c cc)
    (make-control (control-operating-rate c) (control-name c) (control-default c) (calculate-index c cc))))

; control -> [control] -> input
(define control-to-input
  (lambda (c cc)
    (make-input 0 (calculate-index c cc))))

; ugen -> [ugen] -> input
(define ugen-to-input
  (lambda (u uu)
    (make-input (calculate-index u uu) 0)))

; proxy -> [ugen] -> input
(define proxy-to-input
  (lambda (p uu)
    (make-input (calculate-index (proxy-ugen p) uu)
                (proxy-port p))))

; node -> [node] -> [control] -> [ugen] -> input
(define ugen-parameter-to-input
  (lambda (i nn cc uu)
    (cond
     ((number? i) (number-to-input i nn))
     ((control? i) (control-to-input i cc))
     ((ugen? i) (ugen-to-input i uu))
     ((proxy? i) (proxy-to-input i uu))
     ((mrg? i) (ugen-parameter-to-input (mrg-left i) nn cc uu))
     ((mce? i) (error "ugen-parameter-to-input" "mce?" i))
     (else (error "ugen-parameter-to-input" "illegal input" i)))))

;; Mce

; ugen -> [ugen]
(define mceChannels
  (lambda (u)
    (cond
     ((mce? u) (mce-proxies u))
     ((mrg? u) (let ((rs (mceChannels (mrg-left u))))
                 (cons (make-mrg (head rs) (mrg-right u)) (tail rs))))
     (else (list u)))))

; ugen -> ugen
(define mrg-leftmost
  (lambda (u)
    (if (mrg? u)
        (mrg-leftmost (mrg-left u))
        u)))

; mce|mrg -> int
(define mceDegree
  (lambda (m)
    (let ((l (mrg-leftmost m)))
      (if (mce? l)
          (length (mce-proxies l))
          (error "mceDegree" "illegal input" m)))))

; int -> node -> [node]
(define mce-extend
  (lambda (n i)
    (cond ((mce? i) (extend (mce-proxies i) n))
          ((mrg? i) (let ((rs (mce-extend n (mrg-left i))))
                      (cons (make-mrg (head rs) (mrg-right i)) (tail rs))))
          (else (replicate n i)))))

; ugen -> bool ; is u, or the leftmost mrg node at u, mce?
(define is-mce?
  (lambda (u)
    (mce? (mrg-leftmost u))))

; ugen -> bool
(define mce-required?
  (lambda (u)
    (any is-mce? (ugen-inputs u))))

(define mce-transform
  (lambda (u)
    (ugen-transform
     u
     (lambda (n r i o s)
       (let* ((f (lambda (i*) (make-ugen n r i* o s)))
              (m (maximum (map mceDegree (filter is-mce? i))))
              (e (lambda (i) (mce-extend m i)))
              (i* (transpose (map e i))))
         (map f i*))))))

; node -> node|mce
(define mce-expand
  (lambda (u)
    (cond ((mce? u) (map mce-expand (mce-proxies u)))
          ((mrg? u) (make-mrg (mce-expand (mrg-left u)) (mrg-right u)))
          (else (if (mce-required? u)
                    (mce-expand (mce-transform u)) ; recurse
                    u)))))
