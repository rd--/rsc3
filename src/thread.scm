; requires: sys.*.scm

; () -> float
(define currentTime utcr)

; float -> void
(define pauseThread thread-sleep)

; float -> void
(define pauseThreadUntil
  (lambda (t)
    (let ((c (currentTime)))
      (when (> t c) (pauseThread (- t c))))))

; float -> void
(define wait thread-sleep)
