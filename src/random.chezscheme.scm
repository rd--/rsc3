; (float, float) -> float
;
; (random-float-uniform -1 1)
; (random-float-uniform 2.5 2.75)
(define random-float-uniform
  (lambda (l r)
    (+ (chezscheme:random (- (inexact r) (inexact l))) (inexact l))))

; (int, int) -> int
;
; (random-int-uniform 250 275)
; (random-int-uniform 0.1 10.3)
(define random-int-uniform
  (lambda (l r)
    (+ (chezscheme:random (- (exact (round r)) (exact (round l)))) (exact (round l)))))

; (float, float) -> float
(define random-float-uniform_
  (lambda (l r)
    (+ l (chezscheme:random (- r l)))))

; (int, int) -> int
(define random-int-uniform_
  (lambda (l r)
    (+ l (chezscheme:random (- r l)))))
