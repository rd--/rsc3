; double -> ()
(define thread-sleep
  (lambda (p)
    (let* ((s (exact (floor p)))
           (f (- p s))
           (ns (exact (round (* f 1000000000))))
           (du (chezscheme:make-time 'time-duration ns s)))
      (chezscheme:sleep du))))

; () -> double
(define utcr
  (lambda ()
    (let ((t (chezscheme:current-time 'time-utc)))
      (+ (chezscheme:time-second t)
         (/ (chezscheme:time-nanosecond t) 1e9)))))

; string -> ()
(define system chezscheme:system)

; string -> string | bool
(define getenv chezscheme:getenv)
