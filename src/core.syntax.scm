; syntax for binding control values
(define-syntax letc
  (syntax-rules ()
    ((_ () expr)
     expr)
    ((_ ((name default) ...) expr)
     (let ((name (ctl (symbol->string (quote name)) default))
           ...)
       expr))))
