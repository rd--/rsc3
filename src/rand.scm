; (int, num) -> [num]
(define randn
  (lambda (n r)
    (replicateM n (lambda () (rand r)))))

; (int, num, num) -> [num]
(define rrandn
  (lambda (n l r)
    (replicateM n (lambda () (rrand l r)))))
