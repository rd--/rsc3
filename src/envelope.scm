; Envelope

; symbol|number -> number
(define curveToShape
  (lambda (c)
    (cond
     ((symbol? c)
      (cond ((equal? c 'step) 0.0)
            ((equal? c 'linear) 1.0)
            ((equal? c 'exponential) 2.0)
            ((equal? c 'sine) 3.0)
            ((equal? c 'welch) 4.0)
            ((equal? c 'squared) 6.0)
            ((equal? c 'cubed) 7.0)
            (else (error "curveToShape" "symbol" c))))
     ((number? c)
      5.0)
     ((ugen? c)
      c)
     (else
      (error "curveToShape" "illegal curve" c)))))

; any -> number
(define curveToValue
  (lambda (c)
    (if (number? c) c 0.0)))

; Make a <list> for use with the EnvGen Ugen. `levels' is a <list>
; containing the left to right gain values for the envelope, it has
; one more element than the <list> `times', having the delta times for
; each envelope segment. `curve' is either a string or a number or a
; <list> of such, in either case it is expanded to a list of the same
; length as `times'. `releaseNode' is the index of the 'release'
; stage of the envelope, `loopNode' is the index of the 'loop' stage
; of the envelope. These indices are set as invalid, by convention -1,
; to indicate there is no such node.
(define Env
  (lambda (levels times curves releaseNode loopNode)
    (let ((degree (- (length levels) 1)))
      (append
       (list (head levels) (length times) releaseNode loopNode)
       (concat
        (zipWith3
         (lambda (l t c)
           (list
            l
            t
            (curveToShape c)
            (curveToValue c)))
         (tail levels)
         (extend times degree)
         (extend curves degree)))))))

; [(ugen . ugen)] -> ugen -> ugen -> [ugen] -> ugen
(define EnvCoord
  (lambda (d dur amp curves)
    (Env
     (map (lambda (e) (Mul (cdr e) amp)) d)
     (map (lambda (e) (Mul e dur)) (d->dx-by Sub (map car d)))
     curves
     -1
     -1)))

(define EnvCoordLinear
  (lambda (d dur amp)
    (EnvCoord d dur amp (replicate (- (length d) 1) 1))))

(define breakPointCoord
  (lambda (l)
    (if (null? l)
        '()
        (let ((x (car l))
              (y (cadr l))
              (r (cddr l)))
          (cons (cons x y) (breakPointCoord r))))))

(define EnvBreakPoint
  (lambda (bp d a c) (EnvCoord (breakPointCoord (cons 0 bp)) d a c)))

(define EnvBreakPointLinear
  (lambda (bp d a)
    (EnvCoordLinear (breakPointCoord (cons 0 bp)) d a)))

(define EnvTrapezoidCoord
  (lambda (shape skew)
    (let ((x1 (Mul skew (Sub 1.0 shape))))
      (list (cons 0 (Le skew 0.0))
            (cons x1 1.0)
            (cons (Add shape x1) 1.0)
            (cons 1.0 (Ge skew 1.0))))))

; shape: the sustain time as a proportion of dur, 0=triangular 1=rectangular
; skew: the attack/decay ratio, 0=immediate attack and a slow decay, 1=slow attack, immediate decay
(define EnvTrapezoid
  (lambda (shape skew dur amp)
    (EnvCoord (EnvTrapezoidCoord shape skew) dur amp (replicate 3 'linear))))

(define EnvTriangle
  (lambda (dur level)
    (let ((halfDur (Mul dur 0.5)))
      (Env (list 0.0 level 0.0)
           (list halfDur halfDur)
           (list 'linear 'linear)
           -1
           -1))))

(define EnvSine
  (lambda (dur level)
    (let ((halfDur (Mul dur 0.5)))
      (Env (list 0.0 level 0.0)
           (list halfDur halfDur)
           (list 'sine 'sine)
           -1
           -1))))

(define EnvPerc
  (lambda (attackTime releaseTime level curves)
    (Env (list 0.0 level 0.0)
         (list attackTime releaseTime)
         curves
         -1
         -1)))

(define EnvAdsr
  (lambda (attackTime
           decayTime
           sustainLevel
           releaseTime
           peakLevel
           curves
           bias)
    (Env (map (lambda (e) (Mul e bias))
               (list 0.0 peakLevel (Mul peakLevel sustainLevel) 0.0))
         (list attackTime decayTime releaseTime)
         curves
         2
         -1)))

(define EnvAsr
  (lambda (attackTime sustainLevel releaseTime curves)
    (Env (list 0.0 sustainLevel 0.0)
         (list attackTime releaseTime)
         curves
         1
         -1)))

(define EnvLinen
  (lambda (attackTime sustainTime releaseTime level curves)
    (Env (list 0.0 level level 0.0)
         (list attackTime sustainTime releaseTime)
         (if (null? curves) (list 'linear 'linear 'linear) curves)
         -1
         -1)))

(define asEnvGen
  (lambda (env gt)
    (EnvGen gt 1 0 1 0 env)))
