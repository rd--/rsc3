; double -> void
(define thread-sleep
  (lambda (p)
    (let ((u (exact (round (* p 1000000)))))
      (guile:usleep u))))

; void -> double
(define utcr
  (lambda ()
    (let* ((t (guile:gettimeofday))
           (s (car t))
           (u (cdr t)))
      (+ s (/ u 1000000)))))

; string -> io ()
(define system guile:system)
