; (import (prefix (srfi :19 time) srfi:))

; void -> float
(define utcr
  (lambda ()
    (let ((t (srfi:current-time srfi:time-utc)))
      (+ (srfi:time-second t)
         (/ (srfi:time-nanosecond t) 1e9)))))
