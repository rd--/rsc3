(defvar rsc3-literate-p
  t
  "*Flag to indicate if we are in literate mode (default=t).")

(make-variable-buffer-local 'rsc3-literate-p)

(defun rsc3-unlit (s)
  "Remove bird literate marks"
  (if rsc3-literate-p
      (replace-regexp-in-string "^> " "" s)
    s))

(defun rsc3-uncomment (s)
  "Remove initial comment and Bird-literate markers if present"
   (replace-regexp-in-string "^[; ]*>*" "" s))

;(if rsc3-literate-p
;    (rsc3-unlit s)
;  (rsc3-uncomment s)))

;(defun rsc3-chunk-string (n s)
;  "Split a string into chunks of 'n' characters."
;  (let* ((l (length s))
;         (m (min l n))
;         (c (substring s 0 m)))
;    (if (<= l n)
;        (list c)
;      (cons c (rsc3-chunk-string n (substring s n))))))

;(defun rsc3-send-string (s)
;  (if (comint-check-proc rsc3-buffer)
;      (let ((cs (rsc3-chunk-string 64 (concat s "\n"))))
;        (mapcar
;         (lambda (c) (comint-send-string rsc3-buffer c))
;         cs))
;    (error "no rsc3 process running?")))

(defun rsc3-boot-scsynth ()
  "Start the current scsynth server and establish a connection."
  (interactive)
  (rsc3-evaluate-expression "(boot*)"))

(defun rsc3-quit-scsynth ()
  "Shutdown the current SCSYNTH server."
  (interactive)
  (rsc3-evaluate-expression "(with-sc3 (lambda (fd) (async fd /quit)))"))

(add-to-list 'auto-mode-alist '("\\.ss$" . rsc3-mode))

(define-derived-mode
  literate-rsc3-mode
  rsc3-mode
  "Literate Scheme SuperCollider"
  "Major mode for interacting with an inferior rsc3 process."
  (setq hsc3-literate-p t)
  (rsc3-setup-font-lock)
  (turn-on-font-lock))

(add-to-list 'auto-mode-alist '("\\.lss$" . literate-rsc3-mode))
