;;; scs.el --- scheme supercollider

;;; Commentary:
; A derivative of `sclang-mode' for interacting with `rsc3'.
; .scs files contain scheme/lisp code written in supercollider (.stc) notation.
; This mode runs an external translator to convert a region of .scs to .scm.

(require 'comint)
(require 'thingatpt)
(require 'find-lisp)
(require 'scheme)
(require 'rsc3)
(require 'sclang)
(require 'hsc3)

;;; Code:

(defun scs-process-region ()
  "Translate region from .scs to .scm notation, return result as string."
  (with-output-to-string
    (shell-command-on-region
     (region-beginning)
     (region-end)
     "hsc3-stc-to-scm"
     standard-output)))

(defun scs-translate-region ()
  "Translate region."
  (interactive)
  (scs-process-region))

(defun scs-eval-region ()
  "Evaluate region."
  (interactive)
  (rsc3-send-string (scs-process-region)))

(defun scs-eval-line ()
  "Evaluate line."
  (interactive)
  (rsc3-send-string
   (buffer-substring
    (line-beginning-position)
    (line-end-position))))

(defun scs-play-region ()
  "Play graph."
  (interactive)
  (rsc3-send-string
   (concat "(audition (Out 0" (scs-process-region) "))\n")))

(defun scs-draw-region ()
  "Draw graph."
  (interactive)
  (rsc3-evaluate-expression
   (concat "(draw (Out 0 " (scs-process-region) "))")))

(defvar scs-mode-map nil
  "Keymap for scs mode.")

(defun scs-mode-keybindings (map)
  "Install scs keybindings into MAP."
  (define-key map (kbd "C-c C-e") 'scs-eval-region)
  (define-key map (kbd "C-c C-c") 'scs-eval-line)
  (define-key map (kbd "C-c C-a") 'scs-play-region)
  (define-key map (kbd "C-c C-g") 'scs-draw-region))

(if scs-mode-map
    ()
  (let ((map (make-sparse-keymap "scs")))
    (rsc3-mode-keybindings map)
    (scs-mode-keybindings map) ; overwrite C-cC-a and C-cC-g
    (setq scs-mode-map map)))

(define-derived-mode
  scs-mode
  sclang-mode
  "scs"
  "Major mode for working with .scs files."
  (turn-on-font-lock))

(add-to-list 'auto-mode-alist '("\\.scs$" . scs-mode))

(provide 'scs)

;;; scs.el ends here
