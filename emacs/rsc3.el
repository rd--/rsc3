;;; rsc3.el --- Scheme SuperCollider

;;; Commentary:
; This mode is implemented as a derivation of `scheme' mode.
; Indentation and font locking is courtesy `scheme' mode.
; Inter-process communication is courtesy `comint'.
; Symbol at point acquisition is courtesy `thingatpt'.
; Directory search facilities are courtesy `find-lisp'.

(require 'scheme)
(require 'comint)
(require 'thingatpt)
(require 'find-lisp)

;;; Code:

(defun line-at-point ()
  "Return the line at point as a string."
  (let (beg end)
    (save-excursion
      (beginning-of-line)
      (setq beg (point))
      (end-of-line)
      (setq end (point)))
    (buffer-substring-no-properties beg end)))

(defvar rsc3-buffer
  "*rsc3*"
  "*The name of the rsc3 scheme process buffer.")

(defvar rsc3-interpreter
  (list "rsc3-scheme")
  "*The name of the scheme interpreter to run.")

(defvar rsc3-directory
  nil
  "*The rsc3 directory (default=nil).")

(defvar sc3-help-directory
  nil
  "*The directory containing the SC3 RTF help files (default=nil).")

(defun rsc3-insert-lambda ()
  "Insert lambda."
  (interactive)
  (insert "lambda"))

(defun rsc3-insert-lambda-unicode ()
  "Insert unicode lambda."
  (interactive)
  (insert "λ"))

(defun rsc3-send-string (str)
  "Send STR to rsc3 process, if rsc3 is running."
  (if (comint-check-proc rsc3-buffer)
      (comint-send-string rsc3-buffer str)
    (error "No rsc3 process running?")))

(defun rsc3-load-buffer ()
  "Load the current buffer."
  (interactive)
  (save-buffer)
  (rsc3-see-scheme)
  (rsc3-send-string (format "(load \"%s\")" buffer-file-name)))

(defun rsc3-see-scheme ()
  "Arrange so that the frame has two windows, the current buffer above the `rsc3-buffer'."
  (interactive)
  (if (not (comint-check-proc rsc3-buffer))
      (rsc3-start-scheme)
    (delete-other-windows)
    (split-window-vertically)
    (with-current-buffer rsc3-buffer
      (let ((window (display-buffer (current-buffer))))
        (goto-char (point-max))
        (save-selected-window
          (set-window-point window (point-max)))))))

(defun rsc3-start-scheme ()
  "Start the rsc3 scheme process.
If `rsc3-interpreter' is not already running as a subprocess it is
started and a new window is created to display the results of
evaluating rsc3 expressions.  Input and output is via `rsc3-buffer'."
  (interactive)
  (if (comint-check-proc rsc3-buffer)
      (rsc3-see-scheme)
    (apply
     'make-comint
     "rsc3"
     (car rsc3-interpreter)
     nil
     (cdr rsc3-interpreter))
    (rsc3-see-scheme)))

(defun rsc3-interrupt-scheme ()
  "Interupt scheme process."
  (interactive)
  (interrupt-process rsc3-buffer comint-ptyp))

(defun rsc3-stop ()
  "Interrupt scheme interpreter & reset scsynth."
  (interactive)
  (progn
    (rsc3-interrupt-scheme)
    (sleep-for 0 100)
    (rsc3-reset-scsynth)))

(defun rsc3-quit-scheme ()
  "Quit scheme.  Quit the scheme interpreter and delete the associated buffer."
  (interactive)
  (rsc3-stop)
  (rsc3-evaluate-expression "(exit)")
  (sleep-for 0 100)
  (kill-buffer rsc3-buffer)
  (delete-other-windows))

(defun rsc3-expression-before-point ()
  "Collects the string containing the text from point back to the start of the preceding expression."
  (buffer-substring-no-properties
   (save-excursion (backward-sexp) (point))
   (point)))

(defun rsc3-evaluate-expression (expression)
  "Evaluate an arbitrary EXPRESSION.
Send the string `expression' to the inferior rsc3 process for
evaluation.  If there is not an active sub-process one is started
and the text sent.  The message is terminated with a newline
character."
  (interactive "sString to evaluate: ")
  (if (not (comint-check-proc rsc3-buffer))
      (rsc3-start-scheme))
  (rsc3-send-string expression)
  (rsc3-send-string "\n"))

(defun rsc3-run-line ()
  "Send the current line to the interpreter."
  (interactive)
  (rsc3-evaluate-expression
   (buffer-substring
    (line-beginning-position)
    (line-end-position))))

(defun rsc3-evaluate ()
  "Evaluate the complete s-expression that precedes point."
  (interactive)
  (rsc3-evaluate-expression (rsc3-expression-before-point)))

(defun rsc3-play ()
  "Rewrite and evaluate the s-expression that precedes point."
  (interactive)
  (rsc3-evaluate-expression
   (concat "(audition (Out 0 " (rsc3-expression-before-point) "))")))

(defun rsc3-draw ()
  "Draw the UGen graph at point using draw*."
  (interactive)
  (rsc3-evaluate-expression
   (concat "(draw (Out 0 " (rsc3-expression-before-point) "))")))

(defun rsc3-reset-scsynth ()
  "Free all nodes running at the current SCSYNTH server."
  (interactive)
  (rsc3-evaluate-expression "(withSc3 reset)"))

(defun rsc3-status-scsynth ()
  "Show status at server."
  (interactive)
  (rsc3-evaluate-expression "(withSc3 displayServerStatus)"))

(defun rsc3-find-files (dir rgx)
  "Find files at DIR matching RGX."
  (mapc (lambda (filename)
          (find-file-other-window filename))
        (find-lisp-find-files dir rgx)))

(defun rsc3-help ()
  "Lookup up the name at point in the rsc3 help files."
  (interactive)
  (let ((rgx (concat "^" (thing-at-point 'symbol) "\\.help\\.scm$")))
    (hsc3-find-files (concat rsc3-directory "help/") rgx)))

(defun rsc3-sc3-help ()
  "Lookup up the name at point in the SC3 (RTF) help files."
  (interactive)
  (let ((rgx (concat "^" (thing-at-point 'symbol) "\\\(.help\\\)?.rtf$")))
    (rsc3-find-files sc3-help-directory rgx)))

(defun rsc3-ugen-summary ()
  "Print hsc3 summary of UGen at point."
  (interactive)
  (display-message-or-buffer
   (shell-command-to-string (format "hsc3-help ugen summary %s" (thing-at-point 'symbol)))))

(defvar rsc3-mode-map nil
  "Keymap for rsc3 mode.")

(defun rsc3-mode-keybindings (map)
  "Install rsc3 keybindings into MAP."
  ;; Scheme
  (define-key map (kbd "C-c C-l") 'rsc3-load-buffer)
  (define-key map (kbd "C-c <") 'rsc3-load-buffer)
  (define-key map (kbd "C-c >") 'rsc3-see-scheme)
  (define-key map (kbd "C-c C-c") 'rsc3-run-line)
  (define-key map (kbd "C-c C-q") 'rsc3-quit-scheme)
  (define-key map (kbd "C-\\") 'rsc3-insert-lambda)
  (define-key map (kbd "M-\\") 'rsc3-insert-lambda-unicode)
  (define-key map (kbd "C-c C-i") 'rsc3-interrupt-scheme)
  ;; scsynth
  (define-key map (kbd "C-c C-o") 'rsc3-quit-scsynth)
  (define-key map (kbd "C-c C-k") 'rsc3-reset-scsynth)
  (define-key map (kbd "C-c C-s") 'rsc3-stop)
  (define-key map (kbd "C-c C-p") 'rsc3-status-scsynth)
  (define-key map (kbd "C-c C-b") 'rsc3-boot-scsynth)
  ;; Expression
  (define-key map (kbd "C-c C-e") 'rsc3-evaluate)
  (define-key map (kbd "C-c C-a") 'rsc3-play)
  (define-key map (kbd "C-c C-g") 'rsc3-draw)
  ;; Help
  (define-key map (kbd "C-c C-u") 'rsc3-ugen-summary)
  (define-key map (kbd "C-c C-h") 'rsc3-help))

(defun rsc3-mode-menu (map)
  "Install rsc3 menu into MAP."
  ;; rsc3
  (define-key map [menu-bar rsc3] (cons "Scheme-SuperCollider" (make-sparse-keymap "Scheme-SuperCollider")))
  ;; Help
  (define-key map [menu-bar rsc3 help] (cons "Help" (make-sparse-keymap "Help")))
  (define-key map [menu-bar rsc3 help rsc3] '("Scheme-SuperCollider help" . rsc3-help))
  ;; Expression
  (define-key map [menu-bar rsc3 expression] (cons "Expression" (make-sparse-keymap "Expression")))
  (define-key map [menu-bar rsc3 expression draw] '("Draw" . rsc3-draw))
  (define-key map [menu-bar rsc3 expression play] '("Play" . rsc3-play))
  (define-key map [menu-bar rsc3 expression evaluate] '("Evaluate" . rsc3-evaluate))
  ;; Scsynth
  (define-key map [menu-bar rsc3 scsynth] (cons "SCSynth" (make-sparse-keymap "SCSynth")))
  (define-key map [menu-bar rsc3 scsynth status] '("Status" . rsc3-status-scsynth))
  (define-key map [menu-bar rsc3 scsynth stop] '("Stop" . rsc3-stop))
  (define-key map [menu-bar rsc3 scsynth reset] '("Reset" . rsc3-reset-scsynth))
  ;; Scheme
  (define-key map [menu-bar rsc3 scheme] (cons "Scheme" (make-sparse-keymap "Scheme")))
  (define-key map [menu-bar rsc3 scheme quit-scheme] '("Quit scheme" . rsc3-quit-scheme))
  (define-key map [menu-bar rsc3 scheme see-scheme] '("See scheme" . rsc3-see-scheme)))

;; If there is no existing map create one and install the keybindings and menu.
(if rsc3-mode-map
    ()
  (let ((map (make-sparse-keymap "Scheme-SuperCollider")))
    (rsc3-mode-keybindings map)
    (rsc3-mode-menu map)
    (setq rsc3-mode-map map)))

(defun rsc3-font-lock-special-forms ()
  "Rules to font lock special forms."
  (interactive)
  (font-lock-add-keywords
   'rsc3-mode
   (list
    (list (concat "(\\(define[-a-zA-Z/\*]*\\)\\>"
                  "[ \t]*(?"
                  "\\(\\sw+\\)?")
          '(1 font-lock-keyword-face)
          '(2 (cond ((match-beginning 1) font-lock-function-name-face)
                    ((match-beginning 3) font-lock-variable-name-face)
                    (t font-lock-warning-face))
              nil t))
    (cons "\\<[akid]r\\>" font-lock-builtin-face)
    (cons "\\<[A-Z][-\\*_a-zA-Z0-9]*\\>" font-lock-type-face)
    (cons "\\<\\sw+:\\>" font-lock-builtin-face))))

(defvar rsc3-font-lock-settings
  '((scheme-font-lock-keywords
     scheme-font-lock-keywords-1 scheme-font-lock-keywords-2)
    nil nil (("+-*/.<>=!?$%_&~^:" . "w")) beginning-of-defun
    (font-lock-mark-block-function . mark-defun)))

(defun rsc3-setup-font-lock ()
  "Set up font locking.
This duplicates what scheme.el does, but sets case-fold to nil instead of t.
This is required for the math UGen names, which include Not and Abs."
  (make-local-variable 'font-lock-defaults)
  (setq font-lock-defaults rsc3-font-lock-settings)
  (put 'letc 'scheme-indent-function 'scheme-let-indent)
  (rsc3-font-lock-special-forms)
  (setq-default font-lock-keywords-case-fold-search nil))

(define-derived-mode
  rsc3-mode
  scheme-mode
  "Scheme SuperCollider"
  "Major mode for interacting with an inferior rsc3 process."
  (rsc3-setup-font-lock)
  (turn-on-font-lock))

(add-to-list 'auto-mode-alist '("\\.scm$" . rsc3-mode))

(add-to-list 'interpreter-mode-alist '("rsc3" . rsc3-mode))

(provide 'rsc3)

;;; rsc3.el ends here
