;;; sch.el --- scheme haskell

;;; Commentary:
; A derivative of `haskell-mode' for interacting with `rsc3'.
; .sch files contain scheme/lisp code written in haskell notation.
; This mode runs an external translator to convert a region of .sch to .scm.

(require 'comint)
(require 'thingatpt)
(require 'find-lisp)
(require 'scheme)
(require 'rsc3)
(require 'haskell)
(require 'hsc3)

;;; Code:

(defun sch-set-region-to-paragraph ()
  "Set the mark at the start and point at the end of the current paragraph."
  (interactive)
  (backward-paragraph)
  (push-mark nil t t)
  (forward-paragraph))

(defun sch-process-region ()
  "Translate region from haskell to scheme notation, return result as string."
  (with-output-to-string
    (shell-command-on-region
     (region-beginning)
     (region-end)
     "hsc3-sch-to-scm"
     standard-output)))

(defun sch-translate-region ()
  "Translate region."
  (interactive)
  (sch-process-region))

(defun sch-eval-region ()
  "Evaluate region."
  (interactive)
  (rsc3-evaluate-expression (sch-process-region)))

(defun sch-eval-paragraph ()
  "Evaluate paragraph."
  (interactive)
  (sch-set-region-to-paragraph)
  (sch-eval-region))

(defun sch-play-region ()
  "Play graph."
  (interactive)
  (rsc3-send-string
   (concat "(audition (Out 0 " (sch-process-region) "))\n")))

(defun sch-play-paragraph ()
  "Play paragraph."
  (interactive)
  (sch-set-region-to-paragraph)
  (sch-play-region))

(defun sch-draw-region ()
  "Draw graph."
  (interactive)
  (rsc3-evaluate-expression
   (concat "(draw (Out 0 " (sch-process-region) "))")))

(defun sch-draw-paragraph ()
  "Draw paragraph."
  (interactive)
  (sch-set-region-to-paragraph)
  (sch-draw-region))

(defvar sch-mode-map nil
  "Keymap for sch mode.")

(defun sch-mode-keybindings (map)
  "Install sch keybindings into `MAP'."
  (define-key map (kbd "C-c C-e") 'sch-eval-paragraph)
  (define-key map (kbd "C-c C-a") 'sch-play-paragraph)
  (define-key map (kbd "C-c C-g") 'sch-draw-paragraph))

(if sch-mode-map
    ()
  (let ((map (make-sparse-keymap "sch")))
    (rsc3-mode-keybindings map)
    (sch-mode-keybindings map) ; overwrite C-cC-a and C-cC-g
    (setq sch-mode-map map)))

(define-derived-mode
  sch-mode
  haskell-mode
  "sch"
  "Major mode for working with .sch files."
  (turn-on-font-lock))

(add-to-list 'auto-mode-alist '("\\.sch$" . sch-mode))

(provide 'sch)

;;; sch.el ends here
