# Naming

_Rsc3_ implements a rate flow model that has since been adopted by _StSc3_ and _JsSc3_ and _Spl_. [1, 2]

In addition, these projects all follow the same naming convention.

Ugen constructors are in _TitleCase_, i.e. _SinOsc_.

Ugens that are written as operators in _ScLang_ are also written in title case, i.e. _MidiCps_.

The rule is decided by the convention that in Smalltalk systems classes are in _TitleCase_ and methods are in _camelCase_.

(While in Smalltalk it would be possible write _Min(p, q)_ it is in keeping with the system to write _p.min(q)_.)

In jssc3 this works because, for instance, _abs_ is not the name of the absolute value function, rather it is _Math.abs_, and so on.

In rsc3 the names _abs_ and _sin_ and so on are reserved by Scheme, and the forms that are extended to allow operating with signals are given distinct capitalised names, i.e. _Abs_ and _Sin_.

Infix operators must be re-written for Js, where the meaning of operators cannot be extended.

A translation table is used, so that _p + q_ is translated as _add(p, q)_.

A simple expression in the three equivalent notations is:

1. _StSc3_: SinOsc(60.midiCps, 0) * 0.1
2. _JsSc3_: mul(SinOsc(midiCps(60), 0), 0.1)
3. _Rsc3_: (Mul (SinOsc (MidiCps 60) 0) 0.1)
4. _Spl_: SinOsc(60.MidiCps, 0) * 0.1

Given that the systems have the same rate conventions and naming conventions, the notations can be easily translated.

With the addition of a mechanism to translate between case forms for operators _Rsc3_ should be able to use the same notation for synthesis graphs as _StSc3_ and _Jssc3_ and _Spl_.

There is also _.sch_, a notation that borrows a subset of Haskell notation to write Scheme programs.

There is a _.sch_ to _.scm_ translator.

* * *

[1] This was initially an _alternate rate flow_ model and was implemented at _rsc3-arf_, but the models have since been switched.

[2] In many senses _JsSc3_ is simply _Rsc3_, writing _SinOsc(440, 0)_ in place of _(SinOsc 440 0)_.
