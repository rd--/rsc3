all:
	(cd mk; make all)

clean:
	(cd mk; make clean)

ix:
	sh sh/mk-graphs.sh

push-all:
	r.gitlab-push.sh rsc3
