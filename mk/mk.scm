(import (rnrs) (mk-r6rs core))

(define rsc3-core-file-seq
  (lambda (random_c sys_c)
    (list
     "constant"
     "core"
     "list"
     (string-append "random." random_c)
     "sc3"
     "rand"
     "kr"
     (string-append "sys." sys_c)
     "thread"
     ; syntax
     "core.syntax")))

(define at-src
  (lambda (x)
    (string-append "../src/" x ".scm")))

(define rsc3-core-src
  (lambda (random_c sys_c)
    (map at-src (rsc3-core-file-seq random_c sys_c))))

; core/chezscheme
(mk-r6rs
 '(rsc3 core)
 (rsc3-core-src "chezscheme" "chezscheme")
 (string-append (list-ref (command-line) 1) "/rsc3/core.chezscheme.sls")
 '((prefix (chezscheme) chezscheme:) (rnrs) (rhs core) (sosc core))
 '()
 '())

; core/ikarus
(mk-r6rs
 '(rsc3 core)
 (rsc3-core-src "srfi" "ikarus")
 (string-append (list-ref (command-line) 1) "/rsc3/core.ikarus.sls")
 '((prefix (ikarus) ikarus:) (prefix (srfi :27 random-bits) srfi:) (rnrs) (rhs core) (sosc core))
 '()
 '())

; core/guile
(mk-r6rs
 '(rsc3 core)
 (rsc3-core-src "srfi" "guile")
 (string-append (list-ref (command-line) 1) "/rsc3/core.guile.sls")
 '((prefix (guile) guile:) (prefix (srfi :27 random-bits) srfi:) (rnrs) (rhs core) (sosc core))
 '()
 '())

; ugen
(mk-r6rs
 '(rsc3 ugen)
 (map at-src '("db/ugen-core" "db/ugen-ext" "pseudo" "envelope" "event" "hw" "operator" "scs" "smalltalk")) ; "ugen" "ugen/rdu"
 (string-append (list-ref (command-line) 1) "/rsc3/ugen.sls")
 '((rnrs) (rhs core) (rsc3 core))
 '()
 '())

; server
(mk-r6rs
 '(rsc3 server)
 (map at-src '("command" "server"))
 (string-append (list-ref (command-line) 1) "/rsc3/server.sls")
 '((rnrs) (rhs core) (sosc core) (rsc3 core))
 '()
 '())

(exit)
