rsc3
----

scheme supercollider client

[rsc3](http://rohandrape.net/?t=rsc3) is a set of
[r6rs](http://www.r6rs.org/)
scheme modules for working with the
[supercollider](http://audiosynth.com/)
synthesiser.
the rsc3 interaction environment
([rsc3.el](http://rohandrape.net/?t=rsc3&e=emacs/rsc3.el))
is written for
gnu [emacs](http://gnu.org/software/emacs/).

for installation and configuration please consult the
[tutorial](http://rohandrape.net/?t=rsc3-texts&e=md/rsc3-tutorial.md)
at [rsc3-texts](http://rohandrape.net/?t=rsc3-texts).

there are a number of related projects:

- [rsc3-dot](http://rohandrape.net/?t=rsc3-dot): ugen graph drawing (1998)
- [rsc3-disassembler](http://rohandrape.net/?t=rsc3-disassembler): synth graph disassembler (1998)
- [rsc3-lang](http://rohandrape.net/?t=rsc3-lang): supercollider language variants (1998)
- [rsc3-midi](http://rohandrape.net/?t=rsc3-midi): musical instrument digital interface (2002)
- [rsc3-sdif](http://rohandrape.net/?t=rsc3-sdif): sound description interchange format (2002)
- [rsc3-arf](http://rohandrape.net/?t=rsc3-arf): alternate rate flow for ugen graphs (2005)
- [hsc3-lisp](http://rohandrape.net/?t=hsc3-lisp): a lisp interpeter, sc3 ugens are the only type (2014)

there is a small set of example [graphs](http://rohandrape.net/?t=rsc3&e=md/graphs.md).
these mostly are written to also work with `hsc3-lisp`,
& assume that `~/.rsc3` has:

~~~~
(import (rnrs) (rhs core) (sosc core) (rsc3 core) (rsc3 server) (rsc3 ugen))
~~~~

there are pre-processors to translate haskell and smalltalk-c notation to scheme notation.
many of the example graphs at _stsc3_ will also run in _rsc3_.

rsc3 is by rohan drape and others.
supercollider is by james mccartney.
chezscheme is by r kent dybvig.
ikarus is by abdulaziz ghuloum.
plt is by matthew flatt and others.
emacs is by richard stallman and others.

tested with:
[chezscheme](https://www.scheme.com/)-9.5.4,
[ikarus](http://ikarus-scheme.org/)-0.0.4-1870,
[guile](https://www.gnu.org/software/guile/)-3.0.4

© [rohan drape](http://rohandrape.net/) and others,
  1998-2024,
  [gpl](http://gnu.org/copyleft/).

see the
[history](http://rohandrape.net/?t=rsc3&q=history)
for details

initial announcement:
[2004-12-15/sc-users](http://rohandrape.net/?t=rsc3&e=md/announce.text)

<!--
[[gmane](http://article.gmane.org/gmane.comp.audio.supercollider.user/7336),
 [bham](https://www.listarc.bham.ac.uk/lists/sc-users-2004/msg07890.html)]
-->
