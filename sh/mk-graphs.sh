#!/bin/sh

g=md/graphs.md
f=$(ls help/graph/*.sch help/graph/*.scm help/graph/*.scs)

echo "## graphs" > $g
echo "" >> $g
for i in $f
do
    echo "- [$(basename $i)](?t=rsc3&e=$i)" >> $g
done
