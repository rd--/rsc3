#!/bin/sh

for i in mk-r6rs rhs sosc rsc3 rsc3-dot rsc3-disassembler rsc3-lang rsc3-midi rsc3-sdif rsc3-arf
do
    echo $i
    (cd ~/sw/$i ; make)
done
