; lfnoise2
(Mul (LfNoise2 1000) 0.05)

; LfNoise2 ; modulate frequency
(Mul (LfNoise2 (XLine 1000 10000 10 removeSynth)) 0.05)

; LfNoise2 ; as frequency control
(Mul (SinOsc (MulAdd (LfNoise2 4) 400 450) 0) 0.1)
