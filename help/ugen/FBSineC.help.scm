; FbSineC ; sclang default values
(Mul (FbSineC (Div (SampleRate) 4) 1 0.1 1.1 0.5 0.1 0.1) 0.1)

; FbSineC ; increase feedback
(let ((fb (Line 0.01 4 10 doNothing)))
  (Mul (FbSineC (SampleRate) 1 fb 1.1 0.5 0.1 0.1) 0.1))

; FbSineC ; increase phase multiplier
(let ((a (Line 1 2 10 doNothing)))
  (Mul (FbSineC (SampleRate) 1 0 a 0.5 0.1 0.1) 0.1))

; FbSineC ; randomly modulate parameters
(let* ((x (MouseX 1 12 0 0.1))
       (f (lambda (m a) (MulAdd (LfNoise2 x) m a))))
  (Mul (FbSineC
                  (f 1e4 1e4)
                  (f 32 33)
                  (f 0.5 0)
                  (f 0.05 1.05)
                  (f 0.3 0.3)
                  0.1
                  0.1)
       0.1))
