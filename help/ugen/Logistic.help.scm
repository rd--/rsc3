; Logistic ; default values
(Mul (Logistic 3 1000 0.5) 0.1)

; Logistic ; onset of chaos
(Mul (Logistic (Line 3.55 3.6 5 doNothing) 1000 0.5) 0.1)

; Logistic ; explore via mouse
(let ((x (MouseX 3 3.99 0 0.1))
      (y (MouseY 10 10000 exponential 0.1)))
  (Mul (Logistic x y 0.25) 0.1))
