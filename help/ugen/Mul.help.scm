; Mul
(Mul (SinOsc 440 0) 0.1)

; Mul ; beating (subaudio rate)
(Mul3 (FSinOsc 10 0) (PinkNoise) 0.5)

; Mul ; ring modulation
(Mul3 (SinOsc (XLine 100 1001 10 doNothing) 0)
      (SyncSaw 100 200)
      0.1)

;---- Mul of two constants is a constant
(= (Mul 3 4) 12)
