; Blip
(Mul (Blip 440 200) 0.1)

; Blip ; modulate frequency
(let ((f (XLine 20000 200 6 removeSynth)))
  (Mul (Blip f 100) 0.1))

; Blip ; modulate number of harmonics
(let ((h (Line 1 100 20 removeSynth)))
  (Mul (Blip 200 h) 0.1))

; Blip ; event control
(let ((f (lambda (g _x y z o _rx _ry p _px _py)
           (Pan2 (Blip (MidiCps p) (MulAdd y 10 1)) (MulAdd o 2 -1) (Mul g z)))))
  (Mul (Mix (Voicer 16 f)) (ctl "gain" 0.5)))
