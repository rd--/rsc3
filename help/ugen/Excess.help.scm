; Excess
(Mul (Excess (FSinOsc 1000 0) (Line 0 1 8 doNothing)) 0.1)

; Excess ; written out
(let ((a (FSinOsc 1000 0))
      (b (Line 0 1 8 doNothing)))
  (Mul (Sub a (Clip2 a b)) 0.1))

