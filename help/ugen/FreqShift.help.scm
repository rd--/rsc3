; FreqShift ; shifting a 100Hz tone by 1 Hz rising to 500Hz
(let ((i (SinOsc 100 0))
      (s (XLine 1 500 5 removeSynth)))
  (Mul (FreqShift i s 0) 0.1))

; FreqShift ; shifting a complex tone by 1 Hz rising to 500Hz
(let ((i (SinOscBank (list 101 303 606 808) 1 1))
      (s (XLine 1 500 5 removeSynth)))
  (Mul (FreqShift i s 0) 0.1))

; FreqShift ; modulating shift and phase
(let ((i (SinOsc 10 0))
      (s (Mul (LfNoise2 0.3) 1500))
      (p (LinLin (SinOsc 500 0) -1 1 0 (* 2 pi))))
  (Mul (FreqShift i s p) 0.1))

; FreqShift ; shifting bandpassed noise
(let ((i (Bpf (WhiteNoise) 1000 0.001))
      (s (Mul (LfNoise0 5.5) 1000)))
  (Mul (FreqShift i s 0) 32))
