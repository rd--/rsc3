; Pluck ; excitation signal is white-noise, triggered twice a second with varying OnePole coef
(let ((n (Mul (WhiteNoise) 0.1))
      (t (Impulse 2 0))
      (x (MouseX -0.999 0.999 0 0.1))
      (dl 1/440))
  (Pluck n t dl dl 10 x))

; Pluck
(let* ((n 25)
       (f (RandN n 0.05 0.2))
       (p (RandN n 0 1))
       (x (MouseX 60 1000 1 0.1))
       (o (LinLin (SinOsc f p) -1 1 x 3000))
       (w (Mul (WhiteNoiseN n) 0.1))
       (i (Impulse (RandN n 10 12) 0))
       (ks (Pluck w i 0.01 (Div 1 o) 2 (Rand 0.01 0.2)))
       (l (RandN n -1 1)))
  (LeakDC (Mix (Pan2 ks l 1)) 0.995))
