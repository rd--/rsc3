; Gendy1 ; sclang defaults
(Pan2 (Gendy1 1 1 1 1 440 660 0.5 0.5 12 12) 0 0.1)

; Gendy1 ; wandering bass
(Pan2 (Gendy1 1 1 1.0 1.0 30 100 0.3 0.05 5 5) 0 0.1)

; Gendy1 ; play me
(let* ((x (MouseX 100 1000 1 0.1))
       (g (Gendy1 2 3 1 1 20 x 0.5 0.0 40 40)))
  (Pan2 (Mul (RLPF g 500 0.3) 0.2) 0.0 0.1))

; Gendy1 ; scream!
(let ((x (MouseX 220 440 1 0.1))
      (y (MouseY 0.0 1.0 0 0.1)))
 (Pan2 (Gendy1 2 3 1 1 x (Mul 8 x) y y 7 7) 0.0 0.1))

; Gendy1 ; 1 cp = random noise
(Pan2 (Gendy1 1 1 1 1 440 660 0.5 0.5 1 1) 0 0.1)

; Gendy1 ; 2 cps = an oscillator
(Pan2 (Gendy1 1 1 1 1 440 660 0.5 0.5 2 2) 0 0.1)

; Gendy1 ; used as an lfo
(let* ((ad (MulAdd (SinOsc 0.1 0) 0.49 0.51))
       (dd (MulAdd (SinOsc 0.13 0) 0.49 0.51))
       (as (MulAdd (SinOsc 0.17 0) 0.49 0.51))
       (ds (MulAdd (SinOsc 0.19 0) 0.49 0.51))
       (g  (Gendy1 2 4 ad dd 3.4 3.5 as ds 10 10)))
  (Pan2 (SinOsc (MulAdd g 50 350) 0) 0.0 0.1))

; Gendy1 ; wasp
(let ((ad (MulAdd (SinOsc 0.1 0) 0.1 0.9)))
  (Pan2 (Gendy1 0 0 ad 1.0 50 1000 1 0.005 12 12) 0.0 0.1))

; Gendy1 ; modulate distributions ; change of Pitch as distributions change the duration structure and spectrum
(let* ((x (MouseX 0 7 0 0.1))
       (y (MouseY 0 7 0 0.1))
       (g (Gendy1 x y 1 1 440 660 0.5 0.5 12 12)))
  (Pan2 g 0 0.1))

; Gendy1 ; modulate number of cps
(let* ((x (MouseX 1 13 0 0.1))
       (g (Gendy1 1 1 1 1 440 660 0.5 0.5 12 x)))
  (Pan2 g 0 0.1))

; Gendy1 ; self modulation
(let* ((x  (MouseX 1 13 0 0.1))
       (y  (MouseY 0.1 10 0 0.1))
       (g0 (Gendy1 5 4 0.3 0.7 0.1 y 1.0 1.0 5 5))
       (g1 (Gendy1 1 1 1 1 440 (MulAdd g0 500 600) 0.5 0.5 12 x)))
  (Pan2 g1 0.0 0.1))

; Gendy1 ; Use sinus to track any oscillator and take CP positions from it ; use adparam and ddparam as the inputs to sample
(let* ((p (LfPulse 100 0 0.4))
       (s (Mul (SinOsc 30 0) 0.5))
       (g (Gendy1 6 6 p s 440 660 0.5 0.5 12 12)))
  (Pan2 g 0.0 0.1))

; Gendy1 ; near the cornerse interesting
(let* ((x (MouseX 0 200 0 0.1))
       (y (MouseY 0 200 0 0.1))
       (p (LfPulse x 0 0.4))
       (s (Mul (SinOsc y 0) 0.5))
       (g (Gendy1 6 6 p s 440 660 0.5 0.5 12 12)))
  (Pan2 g 0.0 0.1))

; Gendy1 ; texture
(let ((o (lambda ()
           (let* ((f (Rand 130.0 160.3))
                  (ad (MulAdd (SinOsc 0.1 0) 0.49 0.51))
                  (dd (MulAdd (SinOsc 0.13 0) 0.49 0.51))
                  (as (MulAdd (SinOsc 0.17 0) 0.49 0.51))
                  (ds (MulAdd (SinOsc 0.19 0) 0.49 0.51))
                  (g (Gendy1 (Rand 0 6) (Rand 0 6) ad dd f f as ds 12 12)))
             (Pan2 (SinOsc (MulAdd g 200 400) 0) (Rand -1 1) 0.1)))))
  (!+ o 10))

; Gendy1 ; try durscale 10.0 and 0.0 too
(let* ((x (MouseX 10 700 0 0.1))
       (y (MouseY 50 1000 0 0.1))
       (g (Gendy1 2 3 1 1 1 x 0.5 0.1 10 10)))
  (Pan2 (CombN (Resonz g y 0.1) 0.1 0.1 5) 0.0 0.1))

; Gendy1 ; another traffic moment
(let* ((i 10)
       (x (MouseX 100 2000 0 0.1))
       (y (MouseY 0.01 1.0 0 0.1))
       (b (MouseButton 0 1 0.1))
       (z (!+
           (lambda ()
             (let* ((f (Rand 50 560.3))
                    (n (Rand 2 20))
                    (k (MulAdd (SinOsc (ExpRand 0.02 0.2) 0)
                                (Div n 2)
                                (Div n 2)))
                    (g (Gendy1
                               (Rand 0 6) (Rand 0 6) (Rand 0 1) (Rand 0 1) f f
                               (Rand 0 1) (Rand 0 1) n k)))
               (Pan2 g (Rand -1 1) (Div 0.1 (sqrt i)))))
	   i)))
  (Add (Mul b z) (Mul (Sub 1 b) (Resonz z x y))))
