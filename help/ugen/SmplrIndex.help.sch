-- SamplerIndex ; event control ; stereo ; requires loaded bosendorfer (or equivalent) sample data
let f g x y z _o _rx _ry _p _px _py =
      let smnn =
            [21, 23, 24, 26, 28, 29, 31, 33, 35, 36, 38, 40, 41, 43, 45, 47, 48, 50, 52, 53, 55, 57, 59
            ,60, 62, 64, 65, 67, 69, 71, 72, 74, 76, 77, 79, 81, 83, 84, 86, 88, 89, 91, 93, 95, 96, 98
            ,100, 101, 103, 105, 107, 108]
          tbl = asLocalBuf smnn
          mnn = x * 88 + 21
          mnn0 = Latch mnn g
          rt = MidiRatio ((mnn - mnn0) * y)
          ix = cr (SamplerIndex tbl (BufFrames tbl) mnn0)
          buf = mceChannel ix 0
          rt0 = mceChannel ix 1
          b0 = 100
          sig = PlayBuf 2 (b0 + buf) (BufRateScale (b0 + buf) * rt0 * rt) g 0 noLoop doNothing
      in sig * z * LagUD g 0 4
in Mix (Voicer 16 f)
