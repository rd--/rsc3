; Select ; audio rate ; all inputs must be at audio rate
(let* ((a (Mce3 (SinOsc 440 0) (Saw 440) (Pulse 440 0.1)))
       (cycle 3/2)
       (w (MulAdd (LfSaw 1 0) cycle cycle)))
  (Mul (Select w a) 0.05))

; Select ; control rate ; if any input is not at audio rate select must be at control rate ; requires=kr:
(let* ((n 32)
       (a (MidiCps (Floor (RandN n 30 80))))
       (cycle (/ n 2))
       (w (MulAdd (LfSaw 1/2 0) cycle cycle)))
  (Mul (Saw (kr: (Select w a))) 0.05))
