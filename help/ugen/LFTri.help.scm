; LfTri
(Mul (LfTri 500 1) 0.1)

; LfTri ; used as both oscillator and lfo
(let ((f (MulAdd (LfTri 4 0) 400 400)))
  (Mul (LfTri f 0) 0.1))

; LfTri l c.f. LfCub
(let ((f (MulAdd (LfTri (MulAdd (LfTri 0.2 0) 8 10) 0) 400 800)))
  (Mul (LfTri f 0) 0.1))

; LfTri
(Mul (LfTri (MulAdd (LfTri 0.2 0) 400 800) 0) 0.1)

; LfTri
(Mul (LfTri 800 0) 0.1)

; LfTri
(Mul (LfTri (XLn 100 8000 30) 0) 0.1)
