; GbmanL ; sclang default initial parameters
(Mul (GbmanL (MouseX 20 (SampleRate) 0 0.1) 1.2 2.1) 0.05)

; GbmanL ; different initial parameters
(Mul (GbmanL (MouseX 20 (SampleRate) 0 0.1) -0.7 -2.7) 0.05)

; GbmanL ; slow shift
(Mul (GbmanL (MouseX 20 (SampleRate) 0 0.1) 1.2 2.0002) 0.05)

; GbmanL ; frequency control
(Mul (SinOsc (MulAdd (GbmanL 40 1.2 2.1) 400 500) 0) 0.1)
