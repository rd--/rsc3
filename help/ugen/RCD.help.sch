-- Rcd ; rotating clock divider
let freqs = map (\i -> MidiCps (i * 5 + 50)) [0 .. 8]
    amps = [1,  0.5,  0.3,  0.3,  0.3,  0.2,  0.2,  0.2]
    tr = LfPulse 7 0 0.01
    rot = -2
    spread = TiRand 0 1 (Impulse 0.13 0)
    dv = TiRand 0 3 (Impulse 0.1 0)
    pulses = Rcd tr rot 0 dv spread 0 0 0 1
    oscs = SinOsc freqs 0 * pulses * amps
    sig = Splay (mceRotate 3 oscs) 1 0.25 0 True
in sig + CombN sig 2 [2,  1] 3 * 0.3
