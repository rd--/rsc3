; LfCub
(let ((f (MulAdd (LfCub (MulAdd (LfCub 0.2 0) 8 10) 0) 400 800)))
  (Mul (LfCub f 0) 0.1))

; LfCub
(Mul (LfCub (MulAdd (LfCub 0.2 0) 400 800) 0) 0.1)

; LfCub
(Mul (LfCub 800 0) 0.1)

; LfCub
(Mul (LfCub (XLn 100 8000 30) 0) 0.1)
