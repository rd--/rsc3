-- Bezier ; bezier approximation of sin function
Bezier 100 0.0001 440 0 [0, 0, 0.2, -1.3, 0.3, -1.3, 0.5, 0, 0.7, 1.3, 0.8, 1.3, 1, 0] * 0.1

-- Bezier ; dynamic shifting of control points,  oscillator ; texture=overlap, 2, 5, 12, inf
let w l r = LinLin (LfdNoise3 2) (-1) 1 l r
    s = Bezier 100 0.0001 (w 200 205) 0 [0, 0, w 0.05 0.25, -1.3, w 0.25 0.45, -1.3
                                        , 0.5, 0, w 0.55 0.75, 1.3, w 0.75 0.95, 1.3
                                        , 1, 0]
in Pan2 s (IRand (-1) 1) 0.1

-- Bezier ; dynamic shifting of control points,  amplitude modulator
let w l r = LinLin (LfdNoise3 0.5) (-1) 1 l r
    s = Bezier 100 0.0001 (w 0.15 16) 0 [ 0, 0, w 0.05 0.25, -1.3, w 0.25 0.45, -1.3
                                        , 0.5, 0, w 0.55 0.75, 1.3, w 0.75 0.95, 1.3
                                        , 1, 0]
in SoundIn 0 * LinLin s (-1) 1 0.25 1

-- Bezier ; event control
let f g _x y z o rx ry p _px _py =
      let w i l r = LinLin i 0 1 l r
          freq = UnitCps p
          s = Bezier 20 0.004 freq 0 [0, 0, w y 0.05 0.25, -1.3, w rx 0.25 0.45, -1.3
                                     , 0.5, 0, w ry 0.55 0.75, 1.3, w o 0.75 0.95, 1.3
                                     , 1, 0]
      in Pan2 s (o * 2 - 1) (z * LagUD g 0 (1 + y * 0.65))
in Mix (Voicer 1 f) * 0.5
