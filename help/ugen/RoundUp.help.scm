; RoundUp
(let* ((x (MouseX 60 4000 0 0.1))
       (f (RoundUp x 100)))
  (Mul (SinOsc f 0) 0.05))

; RoundUp
(let ((n (Line 24 108 6 removeSynth)))
  (Mul (Saw (MidiCps (RoundUp n 1))) 0.05))
