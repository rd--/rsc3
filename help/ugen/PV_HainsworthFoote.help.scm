; PV_HainsworthFoote ; just hainsworth metric with low threshold
(let* ((source (SoundIn 0))
       (detect (PV_HainsworthFoote
                (FftDefaults (LocalBuf 1 2048) source)
                1.0
                0.0
                (MouseX 0.5 1.25 1 0.1)
                0.04)))
  (Mul3
   (SinOsc (list 440 445) 0)
   (Decay (Mul 0.1 detect) 0.1)
   0.1))

; PV_HainsworthFoote ; just hainsworth metric, spot note transitions
(let* ((src (Mul (LfSaw (MulAdd (LfNoise0 1) 90 400) 0) 0.5))
       (dtc (PV_HainsworthFoote
             (FftDefaults (LocalBuf 1 2048) src)
             1.0
             0.0
             0.9
             0.5))
       (cmp (Mul
             (SinOsc 440 0)
             (Decay (Mul 0.1 dtc) 0.1))))
  (Mul (list src cmp) 0.1))

; just foote metric, foote never triggers with threshold over 1.0, mouse control of threshold
(let* ((src (SoundIn 0))
       (dtc (PV_HainsworthFoote
             (FftDefaults (LocalBuf 1 2048) src)
             0.0
             1.0
             (MouseX 0.0 1.1 0 0.1)
             0.02))
       (cmp (Mul
             (SinOsc 440 0)
             (Decay (Mul 0.1 dtc) 0.1))))
  (Mul (list src cmp) 0.1))
