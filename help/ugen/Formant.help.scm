; Formant ; modulate fundamental frequency, Formant frequency stays constant
(let ((f (XLine 400 1000 8 removeSynth)))
  (Mul (Formant f 2000 800) 0.1))

; Formant ; modulate Formant frequency, fundamental frequency stays constant
(let ((f (XLine 400 4000 8 removeSynth)))
  (Mul (Formant (Mce2 200 300) f 200) 0.1))

; Formant ; modulate width frequency, other frequencies stay constant
(let ((w (XLine 800 8000 8 removeSynth)))
  (Mul (Formant 400 2000 w) 0.1))

; Formant ; event control
(let ((f (lambda (g x y z _o _rx _ry _p _px _py)
           (let ((f0 (Mul (list 200 300) x))
                 (ff (LinExp y 0 1 400 1200)))
             (Mul3 (Formant f0 ff 200) g z)))))
  (Mul (Mix (Voicer 16 f)) (ctl "gain" 0.5)))
