; Abs
(Abs (Mul (SyncSaw 100 440) 0.05))

;---- Abs of a constant is a constant
(= (Abs -1) 1)
