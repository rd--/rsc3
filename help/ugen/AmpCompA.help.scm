; AmpCompA
(let* ((x (MouseX 300 15000 1 0.1))
       (y (MouseY 0 1 0 0.1))
       (o (Mul (SinOsc x 0) 0.1))
       (c (AmpCompA x 300 (DbAmp -10) 1)))
  (Mce2 (Mul o y) (Mul3 o (Sub 1 y) c)))

; AmpCompA ; adjust minimum and root (flatten Out the curve for higher amplitudes)
(let* ((x (MouseX 300 18000 1 0.1))
       (y (MouseY 0 1 0 0.1))
       (o (Mul (Formant 300 x 20) 0.1))
       (c (AmpCompA o 300 0.6 0.3)))
  (Mce2 (Mul o y) (Mul3 o (Sub 1 y) c)))

; AmpCompA ; Amplitude compensation in frequency modulation (Fletscher-Munson curve)
(let* ((x (MouseX 300 15000 1 0.1))
       (y (MouseY (Mce2 3 17) (Mce2 200 900) 1 0.1))
       (m (Mul x (MulAdd (SinOsc y 0) 0.5 1)))
       (a (AmpCompA m 300 (DbAmp -10) 1)))
  (Mul3 (SinOsc m 0) 0.1 a))
