; LfdClipNoise ; c.f. lfclipnoise
(let ((f (MulAdd (LfdClipNoise (MouseX 0.1 1000 1 0.1)) 200 500)))
  (Mul (SinOsc f 0) 0.1))

; LfdClipNoise ; LfClipNoise quantizes time steps at high freqs, LfdClipNoise does not
(let ((f (XLine 1000 20000 10 removeSynth)))
  (Mul (LfdClipNoise f) 0.05))
