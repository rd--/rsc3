; LfClipNoise
(Mul (LfClipNoise 1000) 0.025)

; LfClipNoise ; modulate frequency
(let ((f (XLine 1000 10000 10 removeSynth)))
  (Mul (LfClipNoise f) 0.025))

; LfClipNoise ; as frequency control
(let ((f (MulAdd (LfClipNoise 4) 200 600)))
  (Mul (SinOsc f 0) 0.1))

; LfClipNoise
(let ((f (MulAdd (LfClipNoise (MouseX 0.1 1000 1 0.1)) 200 500)))
  (Mul (SinOsc f 0) 0.1))

; LfClipNoise ; not quantization at high frequencies, c.f. LfdClipNoise
(let ((f (XLine 1000 20000 10 removeSynth)))
  (Mul (LfClipNoise f) 0.05))
