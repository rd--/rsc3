; Rand
(let* ((a (Line 0.1 0 0.1 removeSynth))
       (p (Rand -1 1))
       (s (Mul (FSinOsc (Rand 200 1200) 0) a)))
  (Pan2 s p 1))

; Rand
(Mul (SinOsc (MulAdd (LfNoise1 (Add 6 (! (lambda () (Rand -4 4)) 2))) 100 200) 0) 0.1)

; Rand ; kr
(Mul (SinOsc (kr: (MulAdd (LfNoise1 (Add 6 (! (lambda () (Rand -4 4)) 2))) 100 200)) 0) 0.1)
