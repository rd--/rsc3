; Latch
(Mul (Blip (MulAdd (Latch (WhiteNoise) (Impulse 9 0)) 400 500) 4) 0.1)

; Latch ; the above is only an example, LfNoise0 is a faster way to generate random steps
(Mul (Blip (MulAdd (LfNoise0 9) 400 500) 4) 0.1)

; Latch ; <http://create.ucsb.edu/pipermail/sc-users/2006-December/029991.html>
(let* ((n0 (MulAdd (LfNoise2 8) 200 300))
       (n1 (MulAdd (LfNoise2 3) 10 20))
       (s (Mul (Blip n0 n1) 0.1))
       (x (MouseX 1000 (Mul (SampleRate) 0.1) 1 0.1)))
  (Latch s (Impulse x 0)))
