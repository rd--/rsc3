; DemandEnvGen ; frequency envelope with random times
(let* ((l (Dseq inf (list 204 400 201 502 300 200)))
       (t (Drand inf (list 1.01 0.2 0.1 2.0)))
       (y (MouseY 0.01 3 1 0.1))
       (f (DemandEnvGen l (Mul t y) 7 0 1 1 1 0 1 doNothing)))
  (Mul (SinOsc (Mul f (Mce2 1 1.01)) 0) 0.1))

; DemandEnvGen ; frequency modulation
(let* ((x (MouseX -0.01 -4 0 0.1))
       (y (MouseY 1 3000 1 0.1))
       (l (lambda () (Dseq inf (! (lambda () (ExpRand 200 1000)) 32))))
       (t (Mul (SampleDur) y))
       (f (DemandEnvGen (Mce2 (l) (l)) t 5 x 1 1 1 0 1 doNothing)))
  (Mul (SinOsc f 0) 0.1))

; DemandEnvGen ; gate, MouseX on right side of screen toggles gate
(let* ((x (MouseX 0 1 0 0.1))
       (l (RoundTo (Dwhite inf 300 1000) 100))
       (f (DemandEnvGen l 0.1 5 0.3 (Gt x 0.5) 1 1 0 1 doNothing)))
  (Mul (SinOsc (Mul f (Mce2 1 1.21)) 0) 0.1))
