; LfNoise0 ; c.f. lfd-noise0
(Mul (LfNoise0 (MouseX 0.1 1000 1 0.1)) 0.1)

; LfNoise0 ; silent for 2 secs before going up In freq
(Mul (LfNoise0 (XLine 0.5 10000 3 removeSynth)) 0.1)

; LfNoise0 ; quantization at high frequencies
(Mul (LfNoise0 (XLine 1000 20000 10 removeSynth)) 0.1)

; lfnoise0
(Mul (LfNoise0 1000) 0.05)

; LfNoise0 ; modulate frequency
(Mul (LfNoise0 (XLine 1000 10000 10 removeSynth)) 0.05)

; LfNoise0 ; as frequency control
(Mul (SinOsc (MulAdd (LfNoise0 4) 400 450) 0) 0.1)
