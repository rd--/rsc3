; MidiCps
(Mul (Saw (MidiCps (Ln 24 108 10))) 0.1)

; MidiCps
(Mul (SinOsc (MidiCps '(69 69.25)) 0) 0.05)

; MidiCps ; nested
(Mix (Mul (SinOsc (MidiCps '((60 60.5) (69 69.25))) 0) 0.05))

; MidiCps ; demand
(let* ((t (Impulse '(7 10) 0))
       (s (Diwhite inf 60 72))
       (f (Demand t 0 s))
       (o (SinOsc (MidiCps (list f (Add f 0.75))) 0)))
  (Mix (Mul (Cubed (Cubed o)) 0.1)))

;---- MidiCps of a constants is a constant
(= (MidiCps 69) 440)
