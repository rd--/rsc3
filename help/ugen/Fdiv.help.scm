; Fdiv ; usually written as Div
(Div (Mul (PinkNoise) 0.05) (Mul (FSinOsc 10 0.5) 0.75))

; Fdiv
(Fdiv (SinOsc 440 0) 10)

;---- Fdiv of two constants is a constant
(= (Div 12 4) 3)
