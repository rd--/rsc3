; Formlet
(let ((i (Impulse 20 0.5)))
  (Formlet i 1000 0.01 0.1))

; Formlet
(let* ((f (XLine 10 400 8 removeSynth))
       (i (Mul (Blip f 1000) 0.1)))
  (Formlet i 1000 0.01 0.1))

; Formlet ; modulate Formant frequency
(let ((i (Mul (Blip (MulAdd (SinOsc 5 0) 20 300) 1000) 0.1))
      (f (XLine 1500 700 8 removeSynth)))
  (Formlet i f 0.005 0.04))
