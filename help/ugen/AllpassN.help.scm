; AllpassN ; the allpass delay has no audible effect as a Resonator on steady state sound
(AllpassC (Mul (WhiteNoise) 0.05) 0.01 (XLine 0.0001 0.01 20 doNothing) 0.2)

; AllpassN ; an echo, similar to comb, output is inverted and has lower amplitude
(AllpassN (Mul (Decay (Dust 1) 0.2) (Mul (WhiteNoise) 0.05)) 0.2 0.2 3)

; AllpassN ; c.f. AllpassL and allpass-c
(let ((z (Mul (WhiteNoise) 0.05)))
  (Add z (AllpassN z 0.01 (XLine 0.0001 0.01 20 doNothing) 0.2)))
