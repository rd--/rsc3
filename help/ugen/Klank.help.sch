-- klank
let i = Decay (Impulse 4 0) 0.03 * ClipNoise () * 0.01
    d = klankDataMce (RandN 12 800 4000) (1 ! 12) (RandN 12 0.1 2)
in Klank i 1 0 1 d
