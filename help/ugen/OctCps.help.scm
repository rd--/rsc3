; OctCps
(let ((f (OctCps (Line 2 9 6 removeSynth))))
  (Mul (Saw f) 0.05))

; OctCps
(let ((f (OctCps (RoundTo (Line 2 9 6 removeSynth) 1/12))))
  (Mul (Saw f) 0.05))
