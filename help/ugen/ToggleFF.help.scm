; ToggleFf
(let* ((t (Dust (XLn 1 1000 60)))
       (s (SinOsc (MulAdd (ToggleFf t) 400 800) 0)))
  (Mul s 0.05))

; ToggleFf
(let* ((t (Impulse 15 0))
       (g (ToggleFf t)))
  (Pan2
   (SinOsc (TRand 1440 1490 t) 0)
   (TRand -0.15 0.15 t)
   (Mul (Lag g 0.01) (TRand 0.25 0.55 t))))

; ToggleFf
(let* ((t (Impulse (LinLin (LfNoise0 4) -1 1 16 24) 0))
       (g (ToggleFf t)))
  (Pan2
   (Saw (MidiCps (TRand 92 132 t)))
   (TRand -0.15 0.15 t)
   (Mul g (TRand 0.05 0.75 t))))

; ToggleFf
(let* ((t (Impulse (MulAdd (LfNoise0 4) 16 24) 0))
       (g (ToggleFf t)))
  (Pan2
   (Saw (MidiCps (TRand 72 122 t)))
   (TRand -0.1 0.1 t)
   (Mul g (TRand 0.15 0.55 t))))

; ToggleFf
(let* ((t (Impulse (MulAdd (LfNoise2 4) 7 23) 0))
       (g (ToggleFf t)))
  (Pan2
   (Blip (MidiCps (TRand 23 135 t)) (TRand 3 23 t))
   (TRand -0.5 0.5 t)
   (Mul g (TRand 0.25 0.75 t))))

; ToggleFf
(let* ((t (Impulse (LinLin (LfNoise0 4) -1 1 16 48) 0))
       (g (Lag (ToggleFf t) 0.075)))
  (Pan2
   (Blip (MidiCps (TRand 72 79 t)) (TRand 2 9 t))
   (TRand -0.5 0.5 t)
   (Mul g (TRand 0.25 0.75 t))))

; ToggleFf
(let* ((t (Impulse (MulAdd (LfNoise0 4) 9 21) 0))
       (g (ToggleFf t)))
  (Pan2
   (Blip (MidiCps (TRand 22 54 t)) (TRand 2 5 t))
   (TRand -0.5 0.5 t)
   (Mul g (TRand 0.05 0.25 t))))
