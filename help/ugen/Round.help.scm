; RoundTo
(let* ((x (MouseX 60 4000 0 0.1))
       (f (RoundTo x 100)))
  (Mul (SinOsc f 0) 0.05))

; RoundTo
(let ((n (Line 24 108 6 removeSynth)))
  (Mul (Saw (MidiCps (RoundTo n 1))) 0.05))
