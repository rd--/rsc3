; PMOsc
(let ((f (Line 600 900 5 removeSynth)))
  (Mul (PMOsc f 600 3 0) 0.1))

; PMOsc
(let ((mf (Line 600 900 5 removeSynth)))
  (Mul (PMOsc 300 mf 3 0) 0.1))

; PMOsc
(let ((i (Line 0 20 8 removeSynth)))
  (Mul (PMOsc 300 550 i 0) 0.1))

; PMOsc ; mce
(let ((i (Line 0 20 8 removeSynth)))
  (Mul (PMOsc (Mce2 300 307) (Mce2 550 553) i 0) 0.1))

; PMOsc ; nested mce
(let ((i (Line 0 20 8 removeSynth)))
  (Mix (Mul (PMOsc (Mce2 (Mce2 300 411) (Mce2 307 415)) (Mce2 550 553) i 0) 0.1)))
