; !+ ; Mix . Dup
(let ((n 6)
      (o (lambda () (Mul (FSinOsc (Rand 200 700) 0) 0.05))))
  (!+ o n))

; !+
(let ((n 18)
      (o (lambda () (Pan2 (FSinOsc (Rand 200 700) 0) (Rand -1 1) (Rand 0.01 0.05)))))
  (!+ o n))

; mixFill
(let ((f (lambda (k) (Mul (SinOsc (MulAdd 110 k 110) 0) (DbAmp (Sub 0 (Mul k 3)))))))
  (Mul (mixFill 8 f) 0.05))
