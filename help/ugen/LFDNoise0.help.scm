; lfdnoise0 ; c.f. lf-noise0
(Mul (LfdNoise0 (MouseX 0.1 1000 1 0.1)) 0.1)

; lfdnoise0
(Mul (LfdNoise0 (XLine 0.5 10000 3 removeSynth)) 0.1)

; lfdnoise ; unlike LfNoise0 does not quantize time steps at high frequencies
(Mul (LfdNoise0 (XLine 1000 20000 10 removeSynth)) 0.1)
