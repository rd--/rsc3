; Add
(Add (Mul (FSinOsc 800 0) 0.1) (Mul (PinkNoise) 0.1))

; Add ; DC offset
(Add (Mul (FSinOsc 440 0) 0.1) 0.1)

;---- Add of two constants is a constant
(= (Add 3 4) 7)
