; vibrato ; at 1 Hz, note the use of DC.ar
(let ((v (Vibrato (DC 400) 1 0.02 0 0 0.04 0.1 0 0)))
  (Mul (SinOsc v 0) 0.1))

; vibrato ; compare, k-rate freq input can be a constant
(let ((v (Vibrato 400 1 0.02 0 0 0.04 0.1 0 0)))
  (Mul (SinOsc v 0) 0.1))

; vibrato ; control rate and rateVariation
(let* ((x (MouseX 2 100 linear 0.2))
       (y (MouseY 0 1 linear 0.2))
       (v (Vibrato (DC 400) x 0.1 1 1 y 0.1 0 0)))
  (Mul (SinOsc v 0) 0.1))

; vibrato ; control depth and depthVariation
(let* ((n (MulAdd (LfNoise1 1) 3 7))
       (x (MouseX 0 1 linear 0.2))
       (y (MouseY 0 1 linear 0.2))
       (v (Vibrato (DC 400) n x 1 1 y 0.1 0 0)))
  (Mul (SinOsc v 0) 0.1))

; vibrato ; mce
(let ((v (Vibrato (DC (Mce2 400 440.5)) 1 0.02 0 0 0.04 0.1 0 0)))
  (Mul (SinOsc v 0) 0.1))

; vibrato ; mce
(let ((v (Vibrato (DC (Mce2 (Mce2 411 440) (Mce2 419 440.5))) 1 0.02 0 0 0.04 0.1 0 0)))
  (Mix (Mul (SinOsc v 0) 0.1)))
