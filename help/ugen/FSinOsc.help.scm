; FSinOsc ; Sc2, note extra iphase argument
(Mul (FSinOsc (Mce2 440 550) 0) 0.05)

; FSinOsc
(let ((f (XLine 200 4000 1 removeSynth)))
  (Mul (FSinOsc f 0) 0.25))

; FSinOsc ; loses Amplitude towards the end
(let ((f (MulAdd (FSinOsc (XLine 4 401 8 removeSynth) 0) 200 800)))
  (Mul (FSinOsc f 0) 0.15))
