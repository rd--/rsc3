; Dust ; density=1
(Dust 1)

; DustRange ; mean-iot=1
(DustRange 0.0001 2)

; DustRange ; impulse (uniform iot)
(DustRange 1 1)

; impulse ; random gain
(Mul (Impulse 1 0) (Abs (WhiteNoise)))

; Dust ; density=40
(Dust (/ 1 0.025))

; rdustr
(DustRange 0.0001 0.05)

; ---- Courtesy Julian Rohrhuber, sc-dev/2006-January/009747.html
;      This seems a little flat, see related sc-dev post.
(define (dustr lo hi)
  (let ((d (Dseq inf (list (Dwhite 1 lo hi)))))
    (TDuty d 0 0 (Abs (WhiteNoise)) 0)))

(dustr 0.001 (MouseX 0.001 1 1 0.1))
(DustRange 0.001 (MouseX 0.001 1 1 0.1))

(Mul (DustRange 0.001 0.001) (Abs (WhiteNoise)))
(Mul (dustr 0.001 0.001) (Abs (WhiteNoise)))
(Mul (Impulse (/ 1 0.001) 0) (Abs (WhiteNoise)))
