; TGrains
(let* ((b (ctl "buf" 0))
       (t-rate (MouseY 2 200 1 0.1))
       (dur (Div 4 t-rate))
       (t (Impulse t-rate 0))
       (i (MouseX 0 (BufDur b) 0 0.1)))
  (TGrains 2 t b 1 i dur 0 0.1 2))

; TGrains
(let* ((b (ctl "buf" 0))
       (trate (MouseY 8 120 1 0.1))
       (dur (Div 12 trate))
       (clk (Impulse trate 0))
       (x (MouseX 0 (BufDur b) 0 0.1))
       (pos (Add x (TRand 0 0.01 clk)))
       (pan (Mul (WhiteNoise) 0.6)))
  (TGrains 2 clk b 1 pos dur pan 0.1 2))

; TGrains
(let* ((b (ctl "buf" 0))
       (trate (MouseY 8 120 1 0.1))
       (dur (Div 4 trate))
       (clk (Dust trate))
       (x (MouseX 0 (BufDur b) 0 0.1))
       (pos (Add x (TRand 0 0.01 clk)))
       (pan (Mul (WhiteNoise) 0.6)))
  (TGrains 2 clk b 1 pos dur pan 0.1 2))

; TGrains
(let* ((b (ctl "buf" 0))
       (trate (MouseY 2 120 1 0.1))
       (dur (Div 1.2 trate))
       (clk (Impulse trate 0))
       (pos (MouseX 0 (BufDur b) 0 0.1))
       (pan (Mul (WhiteNoise) 0.6))
       (rate (ShiftLeft 1.2 (RoundTo (Mul (WhiteNoise) 3) 1))))
  (TGrains 2 clk b rate pos dur pan 0.1 2))
