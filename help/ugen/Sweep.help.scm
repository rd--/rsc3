; Sweep ; modulate sine frequency
(let* ((t (Impulse (MouseX 0.5 20 1 0.1) 0))
       (f (Add (Sweep t 700) 500)))
  (Mul (SinOsc f 0) 0.1))

; Sweep ; Index into a buffer ; requires=buf
(let* ((b (ctl "buf" 0))
       (t (Impulse (MouseX 0.5 20 1 0.1) 0))
       (i (Sweep t (BufSampleRate b))))
  (Mul (BufRd 1 b i 0 2) 0.25))

; Sweep ; backwards with variable offset ; requires=buf
(let* ((b (ctl "buf" 0))
       (t (Impulse (MouseX 0.5 10 1 0.1) 0))
       (r (BufSampleRate b))
       (i (Add (Sweep t (Neg r)) (Mul (BufFrames b) (LfNoise0 15)))))
  (Mul (BufRd 1 b i 0 2) 0.5))

; Sweep ; raising rate ; requires=buf
(let* ((b (ctl "buf" 0))
       (t (Impulse (MouseX 0.5 10 1 0.1) 0))
       (r (Add (Sweep t 2) 0.5))
       (i (Sweep t (Mul (BufSampleRate b) r))))
  (Mul (BufRd 1 b i 0 2) 0.25))
