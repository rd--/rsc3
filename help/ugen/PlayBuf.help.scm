; PlayBuf ; play once
(let ((b (ctl "buf" 0))) (PlayBuf 1 b (BufRateScale b) 1 0 noLoop doNothing))

; PlayBuf ; infinite loop
(let ((b (ctl "buf" 0))) (PlayBuf 1 b (BufRateScale b) 1 0 loop doNothing))

; PlayBuf ; trigger playback at each pulse
(let ((b (ctl "buf" 0)))
  (PlayBuf 1 b (BufRateScale b) (Impulse 2 0) 0 noLoop doNothing))

; PlayBuf ; trigger playback at each Pulse (diminishing intervals)
(let ((b (ctl "buf" 0))
      (t (Impulse (XLine 0.1 100 10 removeSynth) 0)))
  (PlayBuf 1 b (BufRateScale b) t 0 0 0))

; PlayBuf ; loop playback, accelerating pitch
(let ((b (ctl "buf" 0))
      (rate (XLine 0.1 100 60 removeSynth)))
 (PlayBuf 1 b rate 1 0 1 0))

; PlayBuf ; sine wave control of playback rate, negative rate plays backwards
(let ((b (ctl "buf" 0))
      (r (MulAdd (FSinOsc (XLine 0.2 8 30 removeSynth) 0) 3 0.6)))
 (PlayBuf 1 b (Mul (BufRateScale b) r) 1 0 1 0))
