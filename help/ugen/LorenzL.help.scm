; LorenzL ; vary frequency
(let ((x (MouseX 20 (SampleRate) 0 0.1)))
  (Mul (LorenzL x 10 28 2.667 0.05 0.1 0 0) 0.10))

; LorenzL ; randomly modulate parameters
(Mul (LorenzL
     
      (SampleRate)
      (MulAdd (LfNoise0 1) 2 10)
      (MulAdd (LfNoise0 1) 20 38)
      (MulAdd (LfNoise0 1) 1.5 2)
      0.05
      0.1 0.0 0.0)
     0.1)

; LorenzL ; frequency control
(let* ((x (MouseX 1 200 0 0.1))
       (c (LorenzL x 10 28 2.667 0.05 0.1 0 0)))
  (Mul (SinOsc (MulAdd (Lag c 0.003) 800 900) 0) 0.1))
