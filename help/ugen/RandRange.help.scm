; RandRange ; RandRange is a binary-operator
(Mul (SinOsc (RandRange 440 660) 0) 0.05)

; RandRange ; c.f. rand
(Mul (SinOsc (Rand 440 660) 0) 0.05)

; RandRange ; RandRange is a binary-operator, Rand is an ordinary ugen
(Mul (SinOsc (Mce2 (Rand 440 660) (RandRange 440 660)) 0) 0.05)
