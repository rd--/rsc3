; Crackle
(Mul (Crackle 1.95) 0.1)

; Crackle ; modulate chaos parameter
(Mul (Crackle (Line 1.0 2.0 3 removeSynth)) 0.1)
