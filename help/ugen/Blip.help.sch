-- Blip ; event control
let f g x y z o _rx _ry _p _px _py =
      Pan2 (Blip (MidiCps (x * 13 + 48)) (y * 10 + 1)) (o * 2 - 1) (g * z)
in Mix (Voicer 16 f) * 0.5

-- Blip ; event control (p)
let f g _x y z o _rx _ry p px _py =
      let f0 = UnitCps p
          nh = y * 10 + 1
      in Pan2 (Blip f0 nh) (o * 2 - 1) (g * z)
in Mix (Voicer 16 f) * 0.5
