; VOsc3 ; requires=buf:0-7
(let ((p (MouseX 0 7 0 0.1))
      (f1 (list 240 241))
      (f2 (list 240.27 241.1))
      (f3 (list 240.43 239.71)))
  (Mul (VOsc3 p f1 f2 f3) 0.1))

;---- allocate and fill tables 0 to 7 with a generated list of harmonic amplitudes
(withSc3
 (lambda (fd)
   (let ((gen-buf
	  (lambda (i)
	    (async fd (b_alloc i 1024 1))
	    (let* ((n (expt (+ i 1) 2))
		   (a (map
		       (lambda (j)
			 (squared (/ (- n j) n)))
		       (enumFromTo 0 (- n 1)))))
	      (async fd (b_gen1 i "sine1" (cons 7 a)))))))
     (for-each gen-buf (enumFromTo 0 7)))))

;---- reallocate buffers while oscillator is running
(withSc3
 (lambda (fd)
   (for-each
    (lambda (i)
      (async fd (b_gen1  i "sine1" (cons 7 (replicateM 16 (lambda () (rand 1.0)))))))
    (enumFromTo 0 7))))
