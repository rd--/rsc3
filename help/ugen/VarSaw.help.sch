-- VarSaw
let x = MouseX 0 1 linear 0.2 in VarSaw 220 0 x * 0.1

-- VarSaw
let f = LfPulse [3,  3.03] 0 0.3 * 200 + 200
    w = LinLin (LfTri 1 0) (-1) 1 0 1
in VarSaw f 0 w * 0.1
