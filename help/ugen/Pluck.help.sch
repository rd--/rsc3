-- Pluck ; event control
let f g x y z o _rx _ry _p _px _py =
      let n = WhiteNoise () * z
          dlmax = 1 / 220
          dl = dlmax * (1 - x * 0.9)
          sig = Pluck n g dlmax dl 10 (y / 3)
      in Pan2 sig (o * 2 - 1) 1
in Mix (Voicer 16 f) * 2
