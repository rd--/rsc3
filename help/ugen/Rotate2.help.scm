; Rotate2 ; rotation of stereo sound, via lfo
(let ((x (Mul (PinkNoise) 0.1))
      (y (Mul (LfTri 800 0) (Mul (LfPulse 3 0 0.3) 0.05))))
  (Rotate2 x y (LfSaw 0.1 0)))

; Rotate2 ; rotation of stereo sound, via mouse
(let ((x (! (lambda () (Mul (LfSaw (Rand 198 202) 0) 0.025)) 4))
      (y (Mul (SinOsc 900 0) (Mul (LfPulse 3 0 0.3) 0.05))))
  (Rotate2 x y (MouseX 0 2 0 0.1)))

; Rotate2 ; rotation of B2 encoded signal
(let* ((p (Mul (WhiteNoise) 0.02))
       (q (Mul (Mix (LfSaw (list 200 200.37 201) 0)) 0.03))
       (e (Add (PanB2 p -0.5 1) (PanB2 q -0.5 1)))
       (r (Rotate2 (mceChannel e 1) (mceChannel e 2) (MouseX -1 1 0 0.1))))
  (DecodeB2 4 (mceChannel e 0) (mceChannel r 0) (mceChannel r 1) 0.5))
