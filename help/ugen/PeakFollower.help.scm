; PeakFollower ; no decay
(let* ((s (Mul (Dust 20) (Line 0 1 4 doNothing)))
       (f (MulAdd (PeakFollower s 1.0) 1500 200)))
  (Mul (SinOsc f 0) 0.1))

; PeakFollower ; a little decay
(let* ((s (Mul (Dust 20) (Line 0 1 4 doNothing)))
       (f (MulAdd (PeakFollower s 0.999) 1500 200)))
  (Mul (SinOsc f 0) 0.1))

; PeakFollower ; MouseX controls decay
(let* ((x (MouseX 0.99 1.0001 1 0.1))
       (s (Mul (Dust 20) (Line 0 1 4 doNothing)))
       (f (MulAdd (PeakFollower s (Min x 1.0)) 1500 200)))
  (Mul (SinOsc f 0) 0.1))

; PeakFollower ; follow sine lfo
(let* ((x (MouseX 0.99 1.0001 1 0.1))
       (s (SinOsc 0.2 0))
       (f (MulAdd (PeakFollower s (Min x 1.0)) 200 500)))
  (Mul (SinOsc f 0) 0.1))
