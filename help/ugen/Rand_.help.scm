; unary operator ; rand_
(Mul (SinOsc (Add 110 (Rand_ 110)) 0) 0.1)

; c.f. rand (ordinary ugen)
(Mul (SinOsc (Add 110 (Rand 0 110)) 0) 0.1)
