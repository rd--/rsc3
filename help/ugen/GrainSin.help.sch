-- GrainSin
let overlap = MouseY 0 2 0 0.2
    f = MouseX 1 220 0 0.2
in GrainSin 2 (Impulse f 0) (overlap / f) 440 0 (-1) 512 * 0.1
