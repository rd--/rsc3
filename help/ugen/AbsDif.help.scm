; AbsDif
(Mul
 (FSinOsc 440 0)
 (AbsDif 0.2 (Mul (FSinOsc 2 0) 0.5)))

;---- AbsDif of constants is a constant
(= (AbsDif 1 2) 1)
