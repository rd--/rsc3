; whitenoise
(Mul (WhiteNoise) 0.05)

; WhiteNoise ; noise generator constructorse unique
(Mul (Sub (WhiteNoise) (WhiteNoise)) 0.05)

; WhiteNoise ; silence
(let ((n (WhiteNoise))) (Sub n n))
