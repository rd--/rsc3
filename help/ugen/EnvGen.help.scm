; EnvGen
(let* ((d (EnvPerc 0.01 1 1 (list -4 -4)))
       (t (Impulse (LinLin (LfNoise2 3) -1 1 1 9) 0))
       (e (EnvGen t 0.1 0 1 doNothing d)))
 (Mul e (SinOsc (TRand 220 440 t) 0)))

; EnvGen ; Mce at trigger input
(let* ((d (EnvPerc 0.01 1 1 (list -4 -4)))
       (t (Impulse (Mce2 3 5) 0))
       (e (EnvGen t 0.1 0 1 doNothing d)))
 (Mul e (SinOsc (TRand 220 440 t) 0)))

; Charles Dodge and Thomas Jerse.
; "Computer Music: Synthesis, Composition, and Performance."
; Schirmer, New York, 1997.
; Second Edition
; Figure 4.5, pg. 81 (Figure 3.5, pg. 71)
(let ((riseTime 0.05)
      (amp 0.1)
      (dur 2.0)
      (decayTime 0.7)
      (freq 220.0))
  (let* ((sustain (- dur riseTime decayTime))
         (envData (EnvLinen riseTime sustain decayTime amp '(1 1 1)))
         (env (EnvGen 1 1 0 1 removeSynth envData)))
    (Mul (SinOsc freq 0) env)))

; EnvGen ; asEnvGen
(Mul (SinOsc 440 0) (asEnvGen (EnvPerc 0.01 1 0.1 -4) (Impulse 0.75 0)))
