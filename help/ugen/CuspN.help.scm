; CuspN ; vary frequency
(let ((x (MouseX 20 (SampleRate) 0 0.1)))
  (Mul (CuspN x 1.0 1.99 0) 0.05))

; CuspN ; mouse controlled params
(let ((x (MouseX 0.9 1.1 1 0.1))
      (y (MouseY 1.8 2 1 0.1)))
  (Mul (CuspN (Div (SampleRate) 4) x y 0) 0.1))

; CuspN ; frequency control
(let* ((x (MouseX 0.9 1.1 1 0.1))
       (y (MouseY 1.8 2 1 0.1))
       (f (MulAdd (CuspN 40 x y 0) 800 900)))
  (Mul (SinOsc f 0.0) 0.1))
