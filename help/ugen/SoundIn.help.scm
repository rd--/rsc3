; SoundIn ; mono
(SoundIn 0)

; SoundIn ; stereo
(SoundIn (Mce2 0 1))

; SoundIn ; quad with re-ordering
(SoundIn (Mce4 0 2 1 3))
