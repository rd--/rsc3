; mousex
(Mul (SinOsc (MouseX 40 10000 1 0.1) 0) 0.1)

; mousex
(Mce2 (Mul (SinOsc (MouseX 20 2000 1 0.1) 0)
           (MouseY 0.01 0.1 0 0.1))
      (Mul (SinOsc (MouseY 20 2000 1 0.1) 0)
           (MouseX 0.01 0.1 0 0.1)))

; MouseXRand ; auto-pilot variant of mousex
(Mul (SinOsc (MouseXRand 40 10000 1 0.1) 0) 0.1)
