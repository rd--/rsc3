; CombC
(CombC (Mul (WhiteNoise) 0.01) 0.01 (XLine 0.0001 0.01 20 removeSynth) 0.2)

; CombC ; with negative feedback
(CombC (Mul (WhiteNoise) 0.01) 0.01 (XLine 0.0001 0.01 20 removeSynth) -0.2)

; CombC ; as an echo
(CombC (Mul (Decay (Mul (Dust 1) 0.1) 0.2) (WhiteNoise)) 0.2 0.2 3)
