; Ringz
(Ringz (Mul (Dust 3) 0.1) 2000 2)

; Ringz
(Ringz (Mul (WhiteNoise) 0.005) 2000 0.5)

; Ringz ; modulate frequency
(Ringz (Mul (WhiteNoise) 0.005)
       (XLine 100 3000 10 doNothing)
       0.5)

; Ringz ; modulate frequency
(Ringz (Mul (Impulse 6 0) 0.1)
       (XLine 100 3000 10 doNothing)
       0.5)

; Ringz ; modulate ring time
(Ringz (Mul (Impulse 6 0) 0.1)
       2000
       (XLine 4 0.04 8 doNothing))

; Ringz ; modulate ring time opposite direction
(Ringz (Mul (Impulse 6 0) 0.1)
       2000
       (XLine 0.04 4 8 doNothing))

; Ringz
(let ((n (Mul (WhiteNoise) 0.001)))
  (!+ (lambda ()
	(let ((f (XLn (Rand 100 5000) (Rand 100 5000) 20)))
	  (Ringz n f 0.5)))
      10))
