; Decay ; Dust triggers Decay to create an exponential envelope
(Mul (Decay (Dust 1) 0.05) (WhiteNoise))

; Decay ; impulse triggers Decay to create an exponential decay envelope
(Mul (Decay (Impulse (XLine 1 50 20 removeSynth) 0.25) 0.1) (PinkNoise))
