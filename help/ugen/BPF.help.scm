; Bpf
(let* ((f1 (XLine 0.7 300 20 removeSynth))
       (f2 (MulAdd (FSinOsc f1 0) 3600 4000)))
  (Bpf (Mul (Saw 200) 0.25) f2 0.3))

; Bpf
(let* ((f1 (MouseX 220 440 0 0.1))
       (f2 (Mce2 f1 (Sub 550 f1)))
       (rq (MouseY 0 0.01 0 0.1)))
  (Bpf (WhiteNoise) f2 rq))
