; LocalBuf ; allocate a buffer local to the synthesis graph, 1-channel, 2048-places
(let* ((nc 1)
       (nf 2048)
       (buf (LocalBuf nc nf))
       (c1 (FftDefaults (LocalBuf 1 2048) (Mul (WhiteNoise) 0.1)))
       (c2 (PV_BrickWall c1 (Mul (SinOsc (ctl "lfo_freq" 0.1) 0) 0.75))))
  (IfftDefaults c2))

; local-buf; not clearing the buffer accesses old data, slowly overwrite data with noise
(let* ((b (LocalBuf 2 2048))
       (nf (BufFrames b))
       (x (MouseX 1 2 linear 0.2))
       (rd (Mul (PlayBuf 2 b x 1 0 loop doNothing) 0.1))
       (wr (lambda (p i) (BufWr b (LinLin p -1 1 0 nf) loop i)))
       (n (! (lambda () (WhiteNoise)) 2))
       (ph (LfNoise0 530)))
  (Mrg2 rd (wr ph n)))
