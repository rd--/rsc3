; Linen ; kr only ; requires kr gate
(let ((e (Linen (kr: (Impulse 2 0)) 0.01 0.1 0.4 doNothing)))
  (Mul (SinOsc 440 0) e))

; Linen ; MouseX is gate
(let* ((y (MouseY 0.01 0.1 0 0.1))
       (x (MouseX -1 1 0 0.1)))
  (Mul (SinOsc 440 0) (Linen x 1 y 1.0 doNothing)))
