; Median ; signal with ImPulse noise
(Median 3 (Add (Mul (Saw 500) 0.05) (Mul (Dust2 100) 0.9)))

; Median ; length can be increased for longer duration noise
(Median 5 (Add (Mul (Saw 500) 0.05) (LPZ1 (Mul (Dust2 100) 0.9))))

; Median ; long filters begIn chopping off the peaks of the waveform
(let ((x (Mul (SinOsc 1000 0) 0.1))) (Mce2 x (Median 31 x)))

; Median ; filter for high frequency noise & leakdc for low frequency noise
(let* ((s0 (MulAdd (WhiteNoise) 0.05 (Mul (SinOsc 800 0) 0.1)))
       (s1 (Median 31 s0)))
  (LeakDC s1 0.9))
