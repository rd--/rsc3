; asLocalBuf ; combines LocalBuf and SetBuf
(let* ((b (asLocalBuf (Mce8 60 62 63 65 67 69 70 72)))
       (m (Index b (MulAdd (LfSaw 0.5 -1) 4 4))))
  (Mul (SinOsc (Lag2 (MidiCps m) 0.1) 0) 0.05))

; asLocalBufInterleaved ; BufRd
(let* ((nc 4)
       (b (asLocalBufInterleaved nc (mceFill nc (lambda (x) (* (+ x 1) 110)))))
       (f (BufRd nc b (Dc 0) 0 1)))
  (Splay2 (Mul (SinOsc f 0) 0.05)))

; asLocalBufMultiChannel ; BufRd ; three channels, four frames
(let* ((b (asLocalBufMultiChannel '((110 220 440 880) (220 440 880 1760) (112 224 448 896))))
       (f (BufRd 3 b (MouseX 0 (BufFrames b) 0 0.2) 0 1)))
  (Splay2 (Mul (SinOsc f 0) 0.1)))
