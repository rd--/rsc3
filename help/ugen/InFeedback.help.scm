; InFeedback
(let ((f (MulAdd (InFeedback 1 0) 1300 300)))
 (Mul (SinOsc f 0) 0.1))

; InFeedback ; evaluate these In either order and hear both tones
(let ((b (Add (NumInputBuses) (NumOutputBuses))))
  (InFeedback 1 b))

; InFeedback ; evaluate these In either order and hear both tones
(let ((b (Add (NumInputBuses) (NumOutputBuses))))
  (Mrg2
   (Mul (SinOsc 660 0) 0.1)
   (Out b (Mul (SinOsc 440 0) 0.1))))

; InFeedback ; doubters consult
(let ((b (Add (NumInputBuses) (NumOutputBuses))))
  (In 1 b))

; InFeedback ; resonator, see LocalOut for variant
(let* ((b (Add (NumInputBuses) (NumOutputBuses)))
       (p (InFeedback 1 b))
       (i (Mul (Impulse 1 0) 0.5))
       (d (DelayC (Add i (Mul p 0.995)) 1 (Sub (Recip 440) (Recip (ControlRate))))))
  (Mrg2 p (OffsetOut b d)))

; InFeedback ; compare with oscillator
(Mul (SinOsc 440 0) 0.05)
