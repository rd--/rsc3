; LfPar ; c.f. lfcub
(let ((f (MulAdd (LfPar (MulAdd (LfPar 0.2 0) 8 10) 0) 400 800)))
  (Mul (LfPar f 0) 0.1))

; LfPar
(Mul (LfPar (MulAdd (LfPar 0.2 0) 400 800) 0) 0.1)

; LfPar
(Mul (LfPar 800 0) 0.1)

; LfPar
(Mul (LfPar (XLn 100 8000 30) 0) 0.1)
