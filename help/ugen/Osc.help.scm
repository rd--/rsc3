; Osc ; requires=wavetable (see b-gen1)
(let ((b (ctl "tbl" 10)))
  (Mul (Osc b 220 0) 0.1))

; Osc ; modulate freq
(let ((b (ctl "tbl" 10))
      (f (XLine 2000 200 1 removeSynth)))
  (Mul (Osc b f 0) 0.1))

; Osc ; modulate freq
(let* ((b (ctl "tbl" 10))
       (f1 (XLine 1 1000 9 removeSynth))
       (f2 (MulAdd (Osc b f1 0) 200 800)))
  (Mul (Osc b f2 0) 0.1))

; Osc ; modulate phase
(let* ((b (ctl "tbl" 10))
       (f (XLine 20 8000 10 removeSynth))
       (p (Mul (Osc b f 0) (* 2 pi))))
  (Mul (Osc b 800 p) 0.1))

; Osc ; change wavetable buffer while Osc is playing (see b-gen1)
(let ((b (ctl "tbl" 10)))
  (Mul (Osc b 220 0) 0.1))
