; SinOsc ; modulate freq
(let ((f (XLn 2000 200 1)))
  (Mul (SinOsc f 0) 0.1))

; SinOsc ; modulate freq
(let* ((f1 (XLn 1 1000 9))
       (f2 (MulAdd (SinOsc f1 0) 200 800)))
  (Mul (SinOsc f2 0) 0.1))

; SinOsc ; modulate phase
(let* ((f (XLn 20 8000 10))
       (p (Mul (SinOsc f 0) (* pi 2))))
  (Mul (SinOsc 800 p) 0.1))

; SinOsc ; c.f. LfCub
(let ((f (MulAdd (SinOsc (MulAdd (SinOsc 0.2 0) 8 10) 0) 400 800)))
  (Mul (SinOsc f 0) 0.1))

; SinOsc
(Mul (SinOsc (MulAdd (SinOsc 0.2 0) 400 800) 0) 0.1)

; SinOsc
(Mul (SinOsc 800 0) 0.1)

; SinOsc
(Mul (SinOsc (XLn 100 8000 30) 0) 0.1)

; SinOsc
(Mul
 (SinOsc
  (Add
   (Mul
    (LfPulse
     (MulAdd (LfPulse 0.5 0.5 0.8) 1.5 0.5)
     0.0
     (range (SinOsc 0.15 0) 0.10 0.45))
    (range (SinOsc 0.2 0) 330 660))
   (Mul
    (LfPulse
     (MulAdd (LfPulse 0.25 0.0 0.2) 2.5 1)
     0.0
     (range (SinOsc 0.25 0) 0.15 0.35))
    (range (SinOsc 0.1 0) 110 770)))
  0)
 0.2)

; SinOsc
(Mul (SinOsc '(660 770) 0) 0.1)

; SinOsc
(Mul (Mix (SinOsc (map midiCps '(60 64 67 71 78 81 85 87.5)) 0)) 0.05)
