; HenonN ; default initial parameters
(Mul (HenonN (MouseX 20 (SampleRate) 0 0.1) 1.4 0.3 0 0) 0.05)

; HenonN ; mouse control of parameters
(Mul (HenonN
              (Div (SampleRate) 4)
              (MouseX 1 1.4 0 0.1)
              (MouseY 0 0.3 0 0.1)
              0
              0)
     0.05)

; HenonN ; randomly modulate parameters
(Mul (HenonN
              (Div (SampleRate) 8)
              (MulAdd (LfNoise2 1) 0.20 1.20)
              (MulAdd (LfNoise2 1) 0.15 0.15)
              0
              0)
     0.05)

; HenonN ; frequency control
(let ((x (MouseX 1 1.4 0 0.1))
      (y (MouseY 0 0.3 0 0.1))
      (f 40))
  (Mul (SinOsc (MulAdd (HenonN f x y 0 0) 800 900) 0) 0.05))
