; COsc ; requires=wavetable
(let ((b (ctl "tbl" 10))) (Mul (COsc b 200 0.7) 0.1))

; COsc
(let ((b (ctl "tbl" 10))) (Mul (COsc b 200 (MouseXRand 0 4 0 0.1)) 0.1))

; COsc ; c.f. osc
(let ((b (ctl "tbl" 10))) (Mul (Osc b 200 0.0) 0.1))
