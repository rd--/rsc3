-- Formant ; event control
let f g x y z _o _rx _ry _p _px _py =
      let f0 = x * [200,  300]
          ff = LinExp y 0 1 400 1200
      in Formant f0 ff 200 * g * z
in Mix (Voicer 16 f) * 0.5
