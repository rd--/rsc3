; lfnoise1
(Mul (LfNoise1 1000) 0.05)

; LfNoise1 ; modulate frequency
(Mul (LfNoise1 (XLine 1000 10000 10 removeSynth)) 0.05)

; LfNoise1 ; as frequency control
(Mul (SinOsc (MulAdd (LfNoise1 4) 400 450) 0) 0.1)

