; Mix ; multiple channel expansion and sum signals
(let ((f (list 600.2 622.0 641.3 677.7)))
  (Mul (Mix (FSinOsc f 0)) 0.02))

; Mix ; expansion nests ; L=100,5000 R=500,501
(let ((l (FSinOsc (Mce2 100  500) 0))
      (r (FSinOsc (Mce2 5000 501) 0)))
  (Mul 0.01 (Mix (Mce2 l r))))
