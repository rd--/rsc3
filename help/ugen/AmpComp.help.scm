; AmpComp ; compare sine with and without compensation
(let* ((x (MouseX 300 15000 1 0.1))
       (y (MouseY 0 1 0 0.1))
       (o (Mul (SinOsc x 0) 0.1))
       (c (AmpComp o 300 0.333)))
  (Mce2 (Mul o y) (Mul3 o y c)))

; AmpComp ; modulate exponent
(let* ((x (MouseX 300 15000 1 0.1))
       (o (Mul (Pulse x 0.5) 0.05)))
  (Mul o (AmpComp x 300 1.3)))

; AmpComp ; Amplitude compensation In frequency modulation
(let* ((x (MouseX 300 15000 1 0.1))
       (y (MouseY 3 200 1 0.1))
       (m (Mul x (MulAdd (SinOsc y 0) 0.5 1)))
       (a (AmpComp m 300 0.333)))
  (Mul3 (SinOsc m 0) 0.1 a))
