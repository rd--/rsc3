; Sin
(let* ((a (Ln (- pi) pi 1))
       (b (Sub (Div (Sin a) (/ pi 2)) 1))
       (f (MulAdd b 900 1600)))
  (Mul (SinOsc f 0) 0.1))

;---- Sin of a constant is a constant (also Abs and Lt)
(= (Lt (Abs (Sin pi)) 0.00001) 1)
