; K2A
(K2A (Mul (WhiteNoise) 0.05))

; K2A
(Mce2
 (K2A (Mul (WhiteNoise) 0.05))
 (Mul (WhiteNoise) 0.01))

; K2A
(let* ((freq (Mul (Div (MouseX 0.1 40 1 0.1) (BlockSize)) (SampleRate))))
  (Mul
   (Mce2
    (K2A (LfNoise0 freq))
    (LfNoise0 freq))
   0.1))

; K2A ; nested mce
(let ((g (Mce2 (LfNoise0 (Mce2 1 2)) (LfNoise1 (Mce2 2 3)))))
  (Mix (Mul3 (SinOsc (Mce2 440 441) 0) (K2A g) 0.1)))
