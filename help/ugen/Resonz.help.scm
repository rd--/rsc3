; resonz
(Resonz (Mul (WhiteNoise) 0.1) 2000 0.1)

; Resonz ; modulate frequency
(let ((f (XLine 1000 8000 10 removeSynth)))
  (Resonz (Mul (WhiteNoise) 0.1) f 0.05))

; Resonz ; modulate bandwidth
(let ((rq (XLine 1 0.001 8 removeSynth)))
  (Resonz (Mul (WhiteNoise) 0.1) 2000 rq))

; Resonz ; modulate bandwidth opposite direction
(let ((rq (XLine 0.001 1 8 removeSynth)))
  (Resonz (Mul (WhiteNoise) 0.1) 2000 rq))

; Resonz ; random Resonator at a random location
(let ((freq (Mul 120 (IRand 1 16)))
      (bw 1/4)
      (gain 8))
  (Pan2 (Resonz (WhiteNoise) freq (Div bw freq))
        (Rand -1 1)
        gain))
