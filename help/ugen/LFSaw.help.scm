; LfSaw
(Mul (LfSaw 500 1) 0.1)

; LfSaw ; used as both oscillator and lfo
(let ((f (MulAdd (LfSaw 4 0) 400 400)))
  (Mul (LfSaw f 0) 0.1))
