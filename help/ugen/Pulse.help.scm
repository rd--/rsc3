; Pulse ; modulate frequency
(let ((f (XLine 40 4000 6 removeSynth)))
  (Mul (Pulse f 0.1) 0.05))

; Pulse ; modulate Pulse width
(let ((w (Line 0.01 0.99 8 removeSynth)))
  (Mul (Pulse 200 w) 0.05))

; Pulse ; two band limited square waves Thru a resonant low pass filter
(RLPF (Mul (Pulse (Mce2 100 250) 0.5) 0.05)
      (XLine 8000 40 5 removeSynth)
      0.05)
