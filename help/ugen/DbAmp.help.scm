; DbAmp
(Mul (FSinOsc 800 0.0)
     (DbAmp (Line -15 -40 10 removeSynth)))

; DbAmp
(let* ((x (MouseX -60 0 0 0.1))
       (f (MulAdd (DbAmp x) 200 700)))
  (Mul (SinOsc f 0) 0.1))

;---- DbAmp of a constant is a constant (also Lt and Sub)
(= (Lt (Sub (DbAmp -6) 0.5) 0.01) 1)
