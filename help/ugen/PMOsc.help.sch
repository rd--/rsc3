-- PmOsc ; event control
let f g x y z o _rx _ry _p _px _py =
      let cps = MidiCps (x * 24 + 42)
          sig = PmOsc
                (cps + SinOsc o 0 * y * 5)
                (LinLin (LfPulse (1/8) 0 0.5) 0 1 1.01 2.01 * cps)
                (EnvGen g 1 0 1 doNothing (Env [3, 3, 0] [0, 0.2] [-4, -4] 0 0))
                0
      in Mix (sig * z * g)
    rvb s = XFade2 s (GVerb (Bpf s (MidiCps 90) 1) 50 8 0.5 0.5 15 0 0.7 0.5 300) 0.2 1
in rvb (Mix (Voicer 16 f))
