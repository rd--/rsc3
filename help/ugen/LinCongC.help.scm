; LinCongC ; default initial parameters
(Mul (LinCongC (MouseX 20 (SampleRate) 0 0.1) 1.1 0.13 1 0) 0.1)

; LinCongC ; randomly modulate parameters
(Mul (LinCongC
     
      (MulAdd (LfNoise2 1.0) 1e4 1e4)
      (MulAdd (LfNoise2 0.1) 0.5 1.4)
      (MulAdd (LfNoise2 0.1) 0.1 0.1)
      (LfNoise2 0.1)
      0)
     0.1)

; LinCongC ; frequency control
(Mul (SinOsc (MulAdd (LinCongC
                          
                           40
                           (MulAdd (LfNoise2 0.1) 0.1 1.0)
                           (MulAdd (LfNoise2 0.1) 0.1 0.1)
                           (LfNoise2 0.1)
                           0)
                          500 600) 0)
     0.1)
