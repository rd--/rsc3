; Mce2 ; channel layout is L=440 and R=441
(Mul (SinOsc (Mce2 440 441) 0) 0.1)

; Mce2 ; channel layout is L=440,441 and R=660,661
(let ((f (Mce2 (Mce2 440 660) (Mce2 441 661))))
  (Mul (Mix (SinOsc f 0)) 0.1))

; Mce ; list inputs at ugen constructorse
(Splay (SinOsc (iterate 5 (lambda (x) (+ x 55)) 55) 0) 1 0.1 0 #t)

; Mce ; nested lists
(let ((f (list (list 440 660) (list 441 661))))
  (Mul (Mix (SinOsc f 0)) 0.1))
