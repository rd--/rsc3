; In ; patching input to output
(Mul (In 2 (NumOutputBuses)) 0.1)

; In ; patching input to output, with summed delay
(let ((i (Mul (In 2 (NumInputBuses)) 0.1)))
  (Add i (DelayN i 0.5 0.5)))

; In ; write noise to audio bus 10, then read it out, mrg is ordered
(Mrg2
 (In 1 10)
 (Out 10 (Mul (PinkNoise) 0.1)))

; In ; write and also read random values to control-bus
(let* ((b (ctl "bus" 0))
       (t (Dust 2))
       (f (TRand 200 500 t)))
  (Mrg2 (Mul (SinOsc (In 1 b) 0) 0.1)
        (Out b f)))
