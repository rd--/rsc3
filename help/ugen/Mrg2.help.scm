; Mrg2 ; Mrg2 defines a node indicating a multiple root graph ; there is a leftmost rule, so that mrg nodes need not be terminal
(let ((l (Mul (SinOsc 300 0) 0.1))
      (r (Out 1 (Mul (SinOsc 900 0) 0.1))))
  (Mrg2 l r))

; Mrg2 ; the leftmost node may be an mce node
(let ((l (Mul (SinOsc (Mce2 300 400) 0) 0.1))
      (r (Out 1 (Mul (SinOsc 900 0) 0.1))))
  (Mrg2 l r))

; Mrg2 ; the rightmost node may be an mce node ; here k-rate sinosce not connected to anything
(let ((l (Mul (SinOsc (Mce2 300 301) 0) 0.1))
      (r (SinOsc (Mce2 1 (Mce2 2 3)) 0)))
  (Mrg2 l r))
