; Pan2
(let ((p (FSinOsc 2 0)))
  (Pan2 (PinkNoise) p 0.1))

; Pan2
(let ((x (MouseX -1 1 0 0.1))
      (y (MouseY 0 0.1 0 0.1)))
  (Pan2 (PinkNoise) x y))

; Pan2
(Pan2
 (Ringz
  (Mul (WhiteNoise) 0.1)
  (range (LfPulse 2 0.0 0.1) 300 1300)
  0.5)
 (LfTri 0.05 3.0)
 0.1)
