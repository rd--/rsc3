; LatoocarfianC ; default initial parameters
(Mul (LatoocarfianC (MouseX 20 (SampleRate) 0 0.1) 1 3 0.5 0.5 0.5 0.5) 0.02)

; LatoocarfianC ; randomly modulate all parameters
(Mul (LatoocarfianC
      (Div (SampleRate) 4)
      (MulAdd (LfNoise2 2) 1.5 1.5)
      (MulAdd (LfNoise2 2) 1.5 1.5)
      (MulAdd (LfNoise2 2) 0.5 1.5)
      (MulAdd (LfNoise2 2) 0.5 1.5)
      0.5
      0.5)
     0.05)
