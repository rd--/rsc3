; SinOscBank ; c.f. Klang
(SinOscBank
 (list 440 550 660 770 880 990 1000)
 (list 0.05 0.02 0.07 0.04 0.05 0.02 0.03)
 (list 0))

; SinOscBank ; random
(SinOscBank
 (rrandn 7 220.0 440.0)
 (rrandn 7 0.02 0.10)
 (rrandn 7 0.0 1.0))
