; Klank ; c.f. Klang
(let ((i (Mul (Impulse 2 0) 0.1))
      (d (klankData
	  (list 800 1071 1153 1723)
	  (replicate 5 1)
	  (replicate 5 1))))
  (Klank i 1 0 1 d))

; Klank
(let ((i (Mul (Dust 8) 0.1))
      (d (klankData
	  (list 800 1071 1353 1723)
	  (replicate 4 1)
	  (replicate 4 1))))
  (Klank i 1 0 1 d))

; Klank
(let ((i (Mul (PinkNoise) 0.007))
      (d (klankData
	  (list 800 1071 1353 1723)
	  (replicate 4 1)
	  (replicate 4 1))))
  (Klank i 1 0 1 d))

; Klank
(let ((i (Mul (PinkNoise) (Mce2 0.002 0.002)))
      (d (klankData
	  (list 200 671 1153 1723)
	  (replicate 4 1)
	  (replicate 4 1))))
  (Klank i 1 0 1 d))

; Klank
(let ((i (Mul (Decay (Impulse 4 0) 0.03) (Mul (ClipNoise) 0.01)))
      (d (klankDataMce
	  (RandN 12 800 4000)
          (replicate 12 1)
          (RandN 12 0.1 2))))
  (Klank i 1 0 1 d))
