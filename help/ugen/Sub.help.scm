; Sub ; silence
(let ((z (FSinOsc 800 0))) (Sub z z))

; Sub ; non-silence
(Mul (Sub (WhiteNoise) (WhiteNoise)) 0.01)

;---- Sub of constants is a constant
(= (Sub 1 2) -1)
