-- http://www.listarc.bham.ac.uk/lists/sc-users/msg08240.html (jrhb)
let xn = 64
    yn = 65
    x = let ph = InFeedback 2 yn * [LfdNoise3 1, LfdNoise3 1]
        in Out xn (reverse (SinOsc (60 * 2) ph))
    y = let ph = InFeedback 2 xn * [LfdNoise3 0.3, LfdNoise3 0.3] * 4
        in Out yn (SinOsc (50 * 2) ph)
in Mrg3 (InFeedback 2 xn) y x * 0.1

-- http://www.listarc.bham.ac.uk/lists/sc-users/msg08240.html (jrhb)
let xn = 64
    yn = 65
    x = let ph = InFeedback 2 yn * Dup (\() -> LfdNoise3 (sqrt 2)) 2
        in Out xn (reverse (SinOsc 89 ph))
    y = let ph = InFeedback 2 xn * Dup (\() -> LfdNoise3 (sqrt pi)) 2 * pi
        in Out yn (Tanh (SinOsc 19 ph))
in Mrg3 (InFeedback 2 xn) y x * 0.1

-- http://www.listarc.bham.ac.uk/lists/sc-users/msg08240.html (jrhb)
let xn = 64
    yn = 65
    zn = 66
    x = let ph = InFeedback 2 yn * map (\i -> LfdNoise3 (sqrt i)) [1, 2]
        in Out xn (reverse (SinOsc 89 ph))
    y = let ph = InFeedback 2 xn * Dup (\() -> LfdNoise1 (sqrt pi)) 2 * pi
        in Out yn (Tanh (SinOsc 19 ph))
    z = let ph = InFeedback 2 yn * Dup (\() -> LfdNoise0 (sqrt (sqrt pi))) 2 * 42 * pi
        in Out zn (Tanh (SinOsc 43 ph) * exp (-0.5 * pi))
in Mrg4 (InFeedback 2 xn) y x z * 0.1

-- http://www.listarc.bham.ac.uk/lists/sc-users/msg08240.html (jrhb)
let xn = 64
    yn = 65
    zn = 66
    x = let j = LinLin (LfNoise0 (log 2 / 2)) (-1) 1 1 30
            f = Select (cr j) [3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127]
            ph = InFeedback 2 zn * map (cr . LfdNoise3 . Sqrt) [1, 2]
        in Out xn (reverse (SinOsc (cr f) ph))
    y = let ph = InFeedback 2 xn * Dup (\() -> LfdNoise1 (Sqrt pi)) 2 * pi
        in Out yn (Tanh (SinOsc 19 ph) * exp 1)
    z = let ph = InFeedback 2 yn * Dup (\() -> LfdNoise0 (Sqrt (Sqrt pi))) 2 * 42 * pi
        in Out zn (Tanh (SinOsc 43 ph) * exp (-0.5 * pi))
in Mrg4 (InFeedback 2 xn) y x z * 0.1

-- http://www.listarc.bham.ac.uk/lists/sc-users/msg08240.html (jrhb)
let xn = 64
    yn = 65
    zn = 66
    x = let j = LinLin (LfNoise2 (log 2 / 2)) (-1) 1 1 30
            ph = InFeedback 2 zn * map (LfdNoise3 . sqrt) [1, 2]
            f = [281, 107, 263, 151, 271, 19, 229, 193, 293, 131, 71, 101, 181, 7, 13, 29, 223, 251, 199, 79, 61, 163, 173, 37, 139, 43, 3, 89, 113, 53, 239]
        in Out xn (SelectX j (SinOsc f ph))
    y = let ph = InFeedback 2 xn * Dup (\() -> LfdNoise1 (sqrt pi)) 2 * pi
        in Out yn (Tanh (SinOsc 19 ph) * exp 1)
    z = let ph = InFeedback 2 yn * Dup (\() -> LfdNoise0 (sqrt (sqrt pi))) 2 * 42 * pi
        in Out zn (Tanh (SinOsc 43 ph) * exp (-0.5 * pi))
in Mrg4 (InFeedback 2 xn) y x z * 0.1
