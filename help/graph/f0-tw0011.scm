; ---- tw 0011 (f0) ; https://twitter.com/redFrik/status/23182604046 ; packet size
(letrec ((s (lambda (o i) (Mul (SinOsc (Mul (Pow (Mce2 i (Add i 0.0001)) 2) (f o (Sub i 1))) (Mul (f o (Sub i 1)) 0.0001)) (f o (Sub i 1)))))
         (f (lambda (o i) (if (> i 0) (s o i) o))))
  (Div (f 60 6) 60))
