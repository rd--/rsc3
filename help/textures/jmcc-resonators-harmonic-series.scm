; resonators harmonic series (jmcc) #2
(withSc3
 (xfadeTextureUgen
  (list 1 7 inf)
  (let* ((p 2)
         (noise (Mul (BrownNoise) 0.001))
         (rat (list 1.0 1.125 1.25 1.333 1.5 1.667 1.875 2.0 2.25 2.5 2.667 3.0 3.333 3.75 4.0))
         (freq (Mul (choose rat) 120))
         (resFreqs (zipWith Add ((series-with Add) p freq freq) (! (lambda () (Rand2 0.5)) p)))
         (spec (klankData
                resFreqs
                (map (lambda (i) (Div 1 (Add i 1))) (to 0 (- p 1)))
                (! (lambda () (Rand 0.5 4.5)) p))))
    (! (lambda () (Klank noise 1 0 1 spec)) 2))))
