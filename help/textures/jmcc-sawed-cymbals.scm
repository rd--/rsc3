; sawed cymbals - jmcc
(withSc3
 (overlapTextureUgen
  (list 4 4 6 inf)
  (let* ((mk-y (lambda ()
                 (let* ((f1 (Rand 500 2500))
                        (f2 (Rand 0 8000))
                        (f (RandN 15 f1 (Add f1 f2)))
                        (rt (RandN 15 2 6)))
                   (klankDataMce f (replicate 15 1) rt))))
         (z (! mk-y 2))
         (f-s (XLn (Rand 0 600) (Rand 0 600) 12)))
    (Klank (Mul (LfSaw f-s 0) 0.0005) 1 0 1 (Transpose z)))))
