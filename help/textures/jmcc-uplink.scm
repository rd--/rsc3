; uplink (jmcc) #2
(withSc3
 (overlapTextureUgen
  (list 4 1 5 inf)
  (let ((f (! (lambda ()
                (let* ((p0 (LfPulse (Rand 0 20) 0 (Rand 0 1)))
                       (p1 (MulAdd (LfPulse (Rand 0 4) 0 (Rand 0 1))
				   (Rand 0 8000)
				   (Rand 0 2000))))
                  (Mul p0 p1)))
	      2)))
    (Pan2 (Mul (LfPulse f 0 0.5) 0.04) (Rand -0.8 0.8) 1))))
