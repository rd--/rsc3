; lots-o-sines (jmcc) #2
(withSc3
 (xfadeTextureUgen
  (list 4 4 inf)
  (let* ((n 60)
         (z (klangData
	     (! (lambda () (LinRand 40 10040 0)) n)
             (! 1 n)
             (! 0 n)))
         (k (! (lambda () (Klang 1 0 z)) 2)))
    (Mul k (Div 0.1 n)))))
