; seqr (rd)
(withSc3
 (overlapTexture
  (list 6 6 3 inf)
  (lambda ()
    (let* ((seqr-f
            (lambda (f e)
              (let ((n (/ (length e) 2)))
                (kr: (Select (MulAdd (LfSaw f 0) n n) (map midi-cps e))))))
           (n (rrand 6 18))
           (f (/ (rrand 9 18) n)))
      (Mul (Blip
                 (Mce2
		  (seqr-f f (rrandn n 72 96))
                  (seqr-f f (rrandn n 72 84)))
                 (Mce2
		  (seqr-f f (rrandn n 1 3))
                  (seqr-f f (rrandn n 3 6))))
           (Mce2
	    (seqr-f f (rrandn n 0.05 0.10))
            (seqr-f f (rrandn n 0.05 0.15))))))))
