; rails (jmcc) #2
(withSc3
 (overlapTextureUgen
 (list 3 2 4 inf)
  (let* ((n 20) ; resonant modes
         (e (Mul (Dust 100) 0.04)) ; excitation
         (f (XLn 3000 300 8)) ; Sweep filter down
         (l (Ln (Rand2 1) (Rand2 1) 8)) ; Sweep pan
         (r (! (lambda () (Add 200 (LinRand 0 3000 0))) n)) ; resonant frequencies
         (a (! 1 n))
         (t (Add 0.2 (RandN n 0 1))) ; ring times
         (k (Klank (Resonz e f 0.2) 1 0 1 (klankDataMce r a t))))
    (Pan2 k l 1))))

