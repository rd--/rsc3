; clustered sines (jmcc) #2
(withSc3
 (xfadeTextureUgen
  (list 4 4 inf)
  (let* ((n 80)
         (f1 (Rand 100 1100))
         (f2 (Mul 4 f1))
         (y (Add f1 (RandN n 0 f2)))
         (z (klangData y (map (lambda (e) (Div f1 e)) y) (! 0 n)))
         (k (! (lambda () (Klang 1 0 z)) 2)))
    (Mul k (Div 0.3 n)))))
