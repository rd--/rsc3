(withSc3
 (lambda (fd)
   (letc ((f 440)
          (a 0.1))
     (sendSynth fd "sin" (Out 0 (Mul (SinOsc f 0) a))))
   (sendMessage fd (s_new0 "sin" 1001 addToTail 1))))

(withSc3
 (lambda (fd)
   (sendMessage fd (n_set1 1001 "f" 1280))))

(withSc3
 (lambda (fd)
   (sendMessage
    fd
    (n_set
     1001
     (list
      (cons "f" (rrand 60 900))
      (cons "a" (rrand 0.05 0.25)))))))
