; notify
(withSc3
 (lambda (fd)
   (async fd (notify 1))
   (thread-sleep 2.0)
   (let ((r (waitMessage fd "/tr")))
     (sendMessage fd (notify 0)) ; async here will recv a '/tr' not a '/done' message
     r)))
