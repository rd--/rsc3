; b_free ; release buffer
(withSc3
 (lambda (fd)
   (async fd (b_free 0))))
