; b_gen1 ; allocate sine wave-table at buffer=10
(withSc3
 (lambda (fd)
   (async fd (b_alloc 10 512 1))
   (async fd (b_gen1 10 "sine1" (list (+ 1 2 4) 1 1/2 1/3 1/4 1/5 1/6 1/7 1/8 1/9 1/10)))))

; b_gen1 ; allocate sine wave-table at buffer=10
(withSc3
 (lambda (fd)
   (async fd (b_alloc 10 512 1))
   (async fd (b_gen1 10 "sine1" (list (+ 1 2 4) 1 1/2 1/3 1/4 1/5)))))

; b_gen1 ; change wavetable buffer while osc is playing
(withSc3
 (lambda (fd)
   (async fd (b_gen1 10 "sine1" (list (+ 1 2 4) 1 (random-float-uniform 0 1) 1/4)))))

; b_gen1 ; allocate chebyshev wave-table at buffer=10
(withSc3
 (lambda (fd)
   (async fd (b_alloc 10 512 1))
   (async fd (b_gen1 10 "cheby" (list 0 1 0 1 1 0 1)))))
