; 20060926 ; pattern buffer ; rd

(define tseq
  (lambda (l)
    (let ((n (/ (length l) 2)))
      (kr: (Select (MulAdd (LfSaw 0.5 0) n n) l)))))

(define rs
  (lambda (nf fd)
    (let ((r0 (rrand 0.0 nf))
          (r1 (rand 1.0)))
      (sendMessage fd (b_set1 10 r0 r1)))))

(define pattern-buffer
  (lambda (nf c)
    (let* ((p (Phasor 0 (BufRateScale 10) 0 (BufFrames 10) 0))
           (t (BufRd 1 10 p 1 4))
           (r1 (replicateM c (lambda () (rrand 36.0 96.0))))
           (r2 (replicateM c (lambda () (rand2 1.0))))
           (r3 (rrand 0 2))
           (n1 (TRand 0.02 0.08 t))
           (e (Decay2 t 0.01 n1))
           (f (MidiCps (tseq r1)))
           (l (tseq r2))
           (o (list-ref (list (SinOsc f 0) (Saw f)) r3)))
      (Pan2 o l e))))

(withSc3
 (lambda (fd)
   (let ((nf (* 2 48000))
         (c 24))
     (begin
       (async fd (b_alloc 10 (* nf 2) 1))
       (replicateM c (lambda () (rs nf fd)))
       (play fd (Out 0 (pattern-buffer nf c)))))))

; (async fd (b_alloc 10 (* (* 2 48000) 2) 1))
; (replicateM 24 (lambda () (rs (* 2 48000) fd)))
