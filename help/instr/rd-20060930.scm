; 20060930 ; voscil ; rd

(define voscil
  (lambda (b)
    (let* ((hb (/ (- b 1) 2))
           (r 6)
           (f 600))
      (Add
       (Pan2
        (Add
         (VOsc (MulAdd (LfNoise0 r) hb hb) (* f 2) 0)
         (Mul
          (Blip
           (MulAdd (LfNoise0 r) 40 600)
           (MulAdd (LfNoise0 r) 16 24))
          (MulAdd (LfNoise0 r) 0.1 0.1)))
        (LfNoise0 r)
        (MulAdd (LfNoise0 r) 0.5 0.5))
       (Pan2
        (VOsc (MulAdd (LfSaw (/ 1 r) 0) hb hb) f 0)
        (LfNoise0 r)
        (MulAdd (LfNoise0 r) 0.5 0.5))))))

(let ((n (* 8192 4))
      (b 32))
  (withSc3
   (lambda (fd)
     (begin
       (for-each
        (lambda (i)
          (begin
            (async fd (b_alloc i n 1))
            (replicateM
             (rrand 2 512)
             (lambda () (sendMessage fd (b_set1 i (rrand 0 n) (rand2 1.0)))))))
        (enumFromTo 0 (- b 1)))
       (play fd (Out 0 (voscil b)))))))
