; r6rs
(import (rnrs) (rhs core) (sosc core) (rsc3 core) (rsc3 server) (rsc3 ugen))

(equal? (SinOsc 440 0) (SinOsc 440 0)) ; #f

(== (map rate->abbrev (map make-rate (list 0 1 2 3))) '(ir kr ar dr)) ; #t

(== (map rate-from-enum (map make-rate (list 0 1 2 3))) '(0 1 2 3)) ; #t

(== (differentiate '(1 2 4 7 11)) '(1 1 2 3 4)) ; #t

(== (d->dx '(1 2 4 7 11)) '(1 2 3 4)) ; #t

(== (integrate '(3 4 1 1)) '(3 7 8 9)) ; #t

(== (integrate '(1 1 2 3 4)) '(1 2 4 7 11)) ; #t

(! (lambda () (choose (list 1 3 5 7 9))) 7) ; [?]
(! (lambda () (choose (vector 1 3 5 7 9))) 7) ; [?]

(!+ (lambda () (Mul (SinOsc (Rand 20 2000) 0) 0.01)) 10)

(eq? (coin 0) #f) ; #t
(eq? (coin 1) #t) ; #t
(coin 0.5) ; equally #t or #f

(randn 7 1.0) ; floats in (0, 1)
(rrandn 7 23 31) ; integers in (23, 31)

(Add 2 3) ; 5
(Sub 2 3) ; -1
(Mul 2 3) ; 6
(Fdiv 2 3) ; 2/3
(Idiv 2 3)
(Div 2 3) ; 2/3
(Sin pi) ; ~0
(Cos 0) ; 1
(Neg 1) ; -1
(Abs -1) ; 1
(MulAdd 2 3 4) ; 10
(TanH 1) ; ugen
(RoundTo 2 3) ; ugen

; Smalltalk

(equal? (to 1 8) (list 1 2 3 4 5 6 7 8))

(equal? (at (list 1 2 3 4 5 6 7 8) 5) 5)
(equal? (at (vector 1 2 3 4 5 6 7 8) 5) 5)

(equal? (asArray (list 1 2 3)) (list 1 2 3))
(equal? (asArray (vector 1 2 3)) (vector 1 2 3))
(equal? (asArray 1) (list 1))

(klangData (list 110 220 440) (list 0.1) (list 0))
(klangData (list 110 220 440) (list) (list))

(Mul (list 1 2 3 4) 5)
