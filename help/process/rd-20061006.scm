; 20061006 ; strtchd-scrmbld ; rd

(define dustR
  (lambda (r lo hi)
    (let ((d (Dseq inf (Dwhite 1 lo hi))))
      (TDuty r d 0 0 (Abs (WhiteNoise r)) 1))))

(define rpr
  (lambda (n t)
    (let ((i (ControlIn 2 n)))
      (TRand (mceChannel i 0) (mceChannel i 1) t))))

(define strtchd
  (lambda (b z)
    (let* ((clk (DustRange (ControlIn 1 0) (ControlIn 1 1)))
           (rat (rpr 2 clk))
           (dur (rpr 4 clk))
           (bdr (BufDur b))
           (hbd (Mul bdr 0.5))
           (pos (Add (Mul (rpr 8 clk) bdr) (MulAdd (LfSaw z 0) hbd hbd)))
           (pan (rpr 10 clk))
           (amp (rpr 6 clk)))
      (TGrains 2 clk b rat pos dur pan amp 2))))

(define scrmbld
  (lambda (u b t)
    (let* ((f (FftDefaults b u))
           (g (PV_BinScramble
	       f
               (MouseX 0.5 1.0 0 0.1)
               (MouseY 0.5 1.0 0 0.1)
               t)))
      (IfftDefaults g))))

(define strtchd-scrmbld
  (let ((t0 (Dust 0.01))
        (t1 (Dust 0.02))
        (u (Add (strtchd 10 0.015) (strtchd 10 0.0175))))
    (list
     (scrmbld (mceChannel u 0) 20 t0)
     (scrmbld (mceChannel u 1) 30 t1))))

; (mk-r-set)
(define mk-r-set
  (lambda ()
    (list (rrand 0.005 0.001)   (rrand 0.0075 0.0125)
          (rrand 0.90 0.975)    (rrand 1.025 1.10)
          (rrand 0.005 0.075)   (rrand 0.075 0.125)
          (rrand 0.005 0.01)    (rrand 0.15 0.25)
          (rrand -0.015 -0.005) (rrand 0.005 0.015)
          (rrand -1.0 0.0)          (rrand 0.0 1.0))))

(define pattern
  (lambda (fd)
    (begin
      (sendMessage fd (c_setn1 0 (mk-r-set)))
      (thread-sleep (choose (list 0.05 0.15 0.25 0.5 0.75 1.25)))
      (pattern fd))))

(withSc3
 (lambda (fd)
   (begin
     (async fd (b_allocRead 10 "/home/rohan/data/audio/metal.wav" 0 0))
     (async fd (b_alloc 20 2048 1))
     (async fd (b_alloc 30 2048 1))
     (play fd (Out 0 strtchd-scrmbld))
     (pattern fd))))
