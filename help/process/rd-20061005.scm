; 20061005 ; tgr-rpr ; rd

(define dustR
  (lambda (r lo hi)
    (let ((d (Dseq inf (Dwhite 1 lo hi))))
      (TDuty r d 0 0 (Abs (WhiteNoise r)) 1))))

(define rpr
  (lambda (n t)
    (let ((i (ControlIn 2 n)))
      (TRand (mceChannel i 0) (mceChannel i 1) t))))

(define rSet
  (lambda (r)
    (if (> r 0.5)
        (list (rrand 0.005 0.025) (rrand 0.05 0.25)
              (rrand 0.75 0.95)   (rrand 1.05 1.25)
              (rrand 0.001 0.01)  (rrand 0.02 0.04)
              (rrand 0.1 0.2)     (rrand 0.2 0.4)
              (rrand 0.0 0.45)    (rrand 0.55 1.0)
              (rrand -1.0 0.0)    (rrand 0.0 1.0))
        (list (rrand 0.005 0.025) (rrand 0.05 0.25)
              (rrand -1.25 -1.05) (rrand -0.95 -0.75)
              (rrand 0.001 0.01)  (rrand 0.02 0.04)
              (rrand 0.1 0.2)     (rrand 0.2 0.4)
              (rrand 0.0 0.45)    (rrand 0.55 1.0)
              (rrand -1.0 0.0)    (rrand 0.0 1.0)))))

; (tgrRpr 10)
(define tgrRpr
  (lambda (b)
    (let* ((clk (DustRange (ControlIn 1 0) (ControlIn 1 1)))
           (rat (rpr 2 clk))
           (dur (rpr 4 clk))
           (pos (Mul (rpr 8 clk) (BufDur b)))
           (pan (rpr 10 clk))
           (amp (rpr 6 clk)))
      (TGrains 2 (kr: clk) b rat pos dur pan amp 2)))) ; clk must be kr

(define pattern
  (lambda (fd)
    (begin
      (sendMessage fd (c_setn1 0 (rSet (rrand 0 1))))
      (thread-sleep (choose (list 0.25 0.75 1.5)))
      (pattern fd))))

(withSc3
 (lambda (fd)
   (begin
     (async fd (b_allocRead 10 "/home/rohan/data/audio/metal.wav" 0 0))
     (play fd (Out 0 (tgrRpr 10)))
     (pattern fd))))
