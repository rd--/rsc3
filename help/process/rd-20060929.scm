; 20060929 ; e-lamell ; rd

(define e-lamell
  (letc ((f 440) (n 12) (d 0.1) (p 0) (a 1.0))
    (let* ((h (Line n (TChoose 1 (list 1 32)) d doNothing))
           (s (Blip f h))
           (e (EnvGen 1 a 0 1 removeSynth (EnvPerc 0.005 d 1 -4))))
      (Pan2 s p e))))

(define r-note
  (lambda (o p)
    (+ (* (choose o) 12) (choose p))))

(define l-sel
  (lambda ()
    (r-note (list 2 3) (list 0))))

(define h-sel
  (lambda ()
    (r-note (list 2 3 4) (list 0))))

(define pattern
  (lambda (fd)
    (sendBundle
     fd
     (bundle
      -1
      (list (s_new
	     "blip" -1 addToTail 1
             (list
	      "f" (midi-cps (l-sel))
              "n" (rrand 2 36)
              "d" (exp-rand 0.01 0.4)
              "a" (rand 0.75)
              "p" (rand2 1.0)))
            (s_new
	     "blip" -1 addToTail 1
             (list
	      "f" (midi-cps (h-sel))
              "n" (rrand 2 36)
              "d" (exp-rand 0.01 0.4)
              "a" (choose (list 0 0.25 0.5 1.0))
              "p" (rand2 1.0))))))
    (thread-sleep 0.1)
    (pattern fd)))

(withSc3
 (lambda (fd)
   (sendSynth fd "blip" (Out 0 e-lamell))
   (pattern fd)))
