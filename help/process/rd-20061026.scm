; 20061026 ; nharm ; rd

(define nharm
  (lambda (n f)
    (if
     (<= n 0)
     (list)
     (cons f (nharm (- n 1) (Add f f))))))

(define klg
  (lambda (m u)
    (let* ((n (rrand 4 u))
           (d (rrand 9 12))
           (a 0.5)
           (e (EnvGen 1 0.9 0 1 removeSynth (EnvSine d a)))
           (s (klangData
	       (nharm n (MidiCps (rrand m (+ m 2))))
               (replicateM n (lambda () (rrand 0.01 0.02)))
               (replicate n 0))))
      (Pan2 (Klang 1 0 s) (rand2 1.0) e))))

(define pattern
  (lambda (fd)
    (begin
      (play fd (Out 0 (klg (rrand 32.0 92.0) (rrand 9 24))))
      (thread-sleep (rrand 0.25 0.75))
      (pattern fd))))

(withSc3 pattern)
