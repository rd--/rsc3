; 20061021 ; jones ; rd

(define mkls
  (lambda (bp t)
    (EnvGen 1 1 0 1 removeSynth (EnvBreakPointLinear bp t 1))))

(define mkrmp
  (lambda (l r t)
    (mkls (list l 1 r) t)))

(define fd (udpOpen "127.0.0.1" 57110))

#|
(sendMessage
 fd
 (s_new
  "grn" -1 1 1
  (list
   "start-freq" (rrand 440 450)
   "end-freq"   (rrand 12440 12450)
   "start-pan"  (rrand -1 1)
   "end-pan"    (rrand -1 1)
   "ampl"       (rrand 0.05 0.55)
   "dur"        (rrand 0.01 0.05))))
|#
(sendSynth
 fd "grn"
 (letc ((start-freq 440)
        (end-freq 440)
        (ampl 0.1)
        (dur 0.05)
        (start-pan 0)
        (end-pan 0)
        (num-harm 1))
   (Out
    0
    (Pan2
     (Blip (mkrmp start-freq end-freq dur) num-harm)
     (mkrmp start-pan end-pan dur)
     (EnvGen 1 1 0 1 removeSynth (EnvSine dur ampl))))))

(define forever
  (lambda (f)
    (f)
    (forever f)))

(forever
 (lambda ()
   (sendMessage
    fd
    (s_new
     "grn" -1 1 1
     (list
      "start-freq" (rrandf 440 12450)
      "end-freq"   (if (> (rrandf 0 1) 0.1)
                       (rrandf 12440 12450)
                       (rrandf 440 445))
      "start-pan"  (rrandf -1 0)
      "end-pan"    (rrandf 0 1)
      "ampl"       (rrand 0.05 0.95)
      "dur"        (rrand 0.01 0.075)
      "num-harm"   (list-choose (list 1 1 1 2 2 3 16)))))
   (wait
    (pchoose
     (list 0.05 0.075 0.1 0.125 0.15 0.5)
     (list 2    2     4   4     2    1  )))))

(forever
 (lambda ()
   (sendMessage
    fd
    (s_new
     "grn" -1 1 1
     (list
      "start-freq" (rrandf 1350 1550)
      "end-freq"   (rrandf 1430 1450)
      "start-pan"  (rrand -1.0 -0.5)
      "end-pan"    (rrand 0.5 1.0)
      "ampl"       (rrand 0.15 0.25)
      "dur"        (rrand 0.025 0.075)
      "num-harm"   (rrand 1 32))))
   (wait 0.025)))

(forever
 (lambda ()
   (sendMessage
    fd
    (s_new
     "grn" -1 1 1
     (list
      "start-freq" (rrand 350.0 550.0)
      "end-freq"   (rrand 430.0 450.0)
      "start-pan"  (rrand -1.0 -0.5)
      "end-pan"    (rrand 0.5 1.0)
      "ampl"       (rrand 0.15 0.25)
      "dur"        (rrand 0.035 0.085)
      "num-harm"   (rrand 1 32))))
   (wait (rrand 0.020 0.025))))
